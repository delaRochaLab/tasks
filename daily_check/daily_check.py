"""
Play sounds, light leds, open valves, check photogates.
required vars (all strings, transformed whithin the task script)
VAR_BOX: str, box name (paired with bpod), eg 'BPOD01'
"""

from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import user_settings as conf
from toolsR import SoundR, VideoR, UtilsR
import numpy as np
import os
import timeit
import time
import datetime
import csv
from scipy.signal import firwin, lfilter


# TASK
task_version = '1.0'
task_name = 'daily_check'
# stage_number = 1
try:
    tasks_commit, md5sum = UtilsR.get_git_revision_hash('~/projects/tasks/', conf.PYBPOD_PROTOCOL)
except:
    tasks_commit, md5sum = ['coud not find git commit or md5checksum']*2


# PORTS
try:
    behavport_idx = (np.where(np.array(conf.BPOD_BEHAVIOR_PORTS_ENABLED) == True)[0] + 1).tolist()
    LEDL, LEDC, LEDR = [getattr(OutputChannel, f"PWM{num}") for num in behavport_idx]
    WATERL, WATERC, WATERR = [(getattr(OutputChannel, "Valve"), num) for num in behavport_idx]
    PGLin, PGCin, PGRin = [getattr(EventName, f"Port{num}In") for num in behavport_idx]
    PGLout, PGCout, PGRout = [getattr(EventName, f"Port{num}Out") for num in behavport_idx]
except:
    UtilsR.stop('THIS PROTOCOL REQUIRES 3 AND ONLY 3 ACTIVE PORTS, ACTIVATE THEM MANUALLY')


# GET WATER CALIBRATION VALUES
try:
    ValveLtime, ValveCtime, ValveRtime = UtilsR.getWaterCalib(conf.VAR_BOX, behavport_idx)
except:
    UtilsR.stop('COULD NOT FIND WATER CALIBRATION OF BOX: ' + str(conf.VAR_BOX))
if ValveLtime >= 0.3 or (ValveLtime / ValveRtime) > 2 or ValveRtime >= 0.3 or (ValveRtime / ValveLtime) > 2:
    continueTask = UtilsR.dropdownmenu('WATER CALIBRATION ERROR. L: ' + str(ValveLtime) + ', R: ' + str(ValveRtime),
                                       'Continue with these values?', ['Yes', 'No'], 'No')
    if continueTask == 'No':
        UtilsR.stop('')


# GET SOUND CALIBRATION VALUES (BROADBAND)
try:
    LAmp, RAmp = UtilsR.getBroadbandCalib(conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT FIND BROADBAND CALIBRATION OF SOUNDDEVICE: ' + str(conf.VAR_BOX))


# INITIALIZE SOUND STREAM
try:
    sampleRate = 192000
    soundStream = SoundR(sampleRate=sampleRate, deviceOut=conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT INITIALIZE SOUND STREAM, CHECK SOUNDCARD INDEX')


# INITIALIZE VIDEO, PLAY AND RECORD
try:
    currSessionVid = os.path.expanduser('~/VIDEO_pybpod/' + (conf.PYBPOD_SUBJECTS[0]).split("'")[1] + '/')
    if not os.path.exists(os.path.expanduser(currSessionVid)):
        os.makedirs(os.path.expanduser(currSessionVid))
    cam = VideoR(indx_or_path=conf.VAR_BOX, name_video=conf.PYBPOD_SESSION + '.avi', path=currSessionVid,
                 title=conf.VAR_BOX, fps=120, codec_cam='X264', codec_video='X264')
    cam.play()
    # here usually cam.record() but in this task we do not want to record
except:
    UtilsR.stop('COULD NOT INITIALIZE VIDEO, CHECK VIDEO INDEX')


# SOFTCODE HANDLER AND FUNCTIONS
LOAD_L = (OutputChannel.SoftCode, 1)
PLAY = (OutputChannel.SoftCode, 2)
STOP_AND_LOAD_R = (OutputChannel.SoftCode, 3)
STOP = (OutputChannel.SoftCode, 4)
MESSAGEL = (OutputChannel.SoftCode, 5)
MESSAGEC = (OutputChannel.SoftCode, 6)
MESSAGER = (OutputChannel.SoftCode, 7)
message = 'PHOTOGATES CHECKED: '

def my_softcode_handler(data):
    global message
    global SL1, SL2, SR1, SR2
    if data == 1:
        soundStream.load(SL1, SR1)
    if data == 2:
        soundStream.playSound()
    elif data == 3:
        soundStream.stopSound()
        soundStream.load(SL2, SR2)
    elif data == 4:
        soundStream.stopSound()
    elif data == 5:
        message += ' left,'
    elif data == 6:
        message += ' center,'
    elif data == 7:
        message += 'right'


# RELEVANT VARIABLES FOR THE TASK
led_intensity = 127 # from 0 to 255
port_responses = {PGLin: 'left', PGCin: 'center', PGRin: 'right', EventName.Tup: 'exit'}


# CREATING STIMULI
amplitude = 1.0
duration = 2  # stim len (secs). Does not imply playing whole stim each time
band_fs = [2000, 20000]  # boundaries for the white noise (Hz)
filter_len = 10000  # filter len
soundVec = UtilsR.whiteNoiseGen(amplitude, band_fs[0], band_fs[1], duration, FsOut=sampleRate, Fn=filter_len)
SL1, SR1, EL1, ER1 = UtilsR.envelope(0, soundVec, 1, 20, samplingR=sampleRate,
                                     variance=0.015, randomized=False, paired=False, LAmp=LAmp, RAmp=RAmp)  # left
SL2, SR2, EL2, ER2 = UtilsR.envelope(1, soundVec, 1, 20, samplingR=sampleRate,
                                     variance=0.015, randomized=False, paired=False, LAmp=LAmp, RAmp=RAmp)  # right


# INIT BPOD
my_bpod = Bpod()
my_bpod.softcode_handler_function = my_softcode_handler


# CREATING STATE MACHINE, ADDING STATES, SENDING AND RUNNING
sma = StateMachine(my_bpod)

sma.add_state(
    state_name='start',
    state_timer=1,
    state_change_conditions={EventName.Tup: 'startSoundL'},
    output_actions=[LOAD_L])

sma.add_state(
    state_name='startSoundL',
    state_timer=1,
    state_change_conditions={EventName.Tup: 'stopSoundL'},
    output_actions=[PLAY])

sma.add_state(
    state_name='stopSoundL',
    state_timer=1,
    state_change_conditions={EventName.Tup: 'startSoundR'},
    output_actions=[STOP_AND_LOAD_R])

sma.add_state(
    state_name='startSoundR',
    state_timer=1,
    state_change_conditions={EventName.Tup: 'stopSoundR'},
    output_actions=[PLAY])

sma.add_state(
    state_name='stopSoundR',
    state_timer=1,
    state_change_conditions={EventName.Tup: 'ledl'},
    output_actions=[STOP])

sma.add_state(
    state_name='ledl',
    state_timer=2,
    state_change_conditions={EventName.Tup: 'ledc'},
    output_actions=[(LEDL, led_intensity)])

sma.add_state(
    state_name='ledc',
    state_timer=2,
    state_change_conditions={EventName.Tup: 'ledr'},
    output_actions=[(LEDC, led_intensity)])

sma.add_state(
    state_name='ledr',
    state_timer=2,
    state_change_conditions={EventName.Tup: 'waterl'},
    output_actions=[(LEDR, led_intensity)])

sma.add_state(
    state_name='waterl',
    state_timer=3,
    state_change_conditions={EventName.Tup: 'waterr'},
    output_actions=[WATERL])

sma.add_state(
    state_name='waterr',
    state_timer=3,
    state_change_conditions={EventName.Tup: 'touch'},
    output_actions=[WATERR])

sma.add_state(
    state_name='touch',
    state_timer=10,
    state_change_conditions=port_responses,
    output_actions=[])

sma.add_state(
    state_name='left',
    state_timer=10,
    state_change_conditions=port_responses,
    output_actions=[(LEDL, led_intensity), MESSAGEL])

sma.add_state(
    state_name='center',
    state_timer=10,
    state_change_conditions=port_responses,
    output_actions=[(LEDC, led_intensity), MESSAGEC])

sma.add_state(
    state_name='right',
    state_timer=10,
    state_change_conditions=port_responses,
    output_actions=[(LEDR, led_intensity), MESSAGER])

my_bpod.send_state_machine(sma)
my_bpod.run_state_machine(sma)


# CLOSING CAMERA AND BPOD
cam.stop()
my_bpod.close()


# PRESENTING MESSAGEBOX
UtilsR.stop(message)
