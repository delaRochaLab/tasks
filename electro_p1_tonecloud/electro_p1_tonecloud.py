"""
required vars = [
    VAR_BOX: str, box name (paired with bpod), eg 'BPOD01'; 
    VAR_REC: int, 0=do not record; 1=record; 
    VAR_BLEN: int (even), block len; 50
    VAR_DATA: int, 0= do not save using rach util; 1= save data using R util,
    VAR_BNUM: int, block number]

Update_status var = []

# known issues:
"""

from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import user_settings as conf
from toolsR import SoundR, VideoR, UtilsR
import numpy as np
import os
import timeit
import time
import datetime
import csv
from scipy.signal import firwin, lfilter

# TASK
task_version = '0.1'
task_name = p1
stage_number = 1
try:
    tasks_commit, md5sum = UtilsR.get_git_revision_hash('~/projects/tasks/', conf.PYBPOD_PROTOCOL)
except:
    tasks_commit, md5sum = ['coud not find git commit or md5checksum']*2

# PORTS
try:
    behavport_idx = (np.where(np.array(conf.BPOD_BEHAVIOR_PORTS_ENABLED) == True)[0] + 1).tolist()
    LEDL, LEDC, LEDR = [getattr(OutputChannel, f"PWM{num}") for num in behavport_idx]
    WATERL, WATERC, WATERR = [(getattr(OutputChannel, "Valve"), num) for num in behavport_idx]
    PGLin, PGCin, PGRin = [getattr(EventName, f"Port{num}In") for num in behavport_idx]
    PGLout, PGCout, PGRout = [getattr(EventName, f"Port{num}Out") for num in behavport_idx]
except:
    UtilsR.stop('THIS PROTOCOL REQUIRES 3 AND ONLY 3 ACTIVE PORTS, ACTIVATE THEM MANUALLY')

# GET WATER CALIBRATION VALUES
try:
    ValveLtime, ValveCtime, ValveRtime = UtilsR.getWaterCalib(conf.VAR_BOX, behavport_idx)
except:
    UtilsR.stop('COULD NOT FIND WATER CALIBRATION OF BOX: ' + str(conf.VAR_BOX))
if ValveLtime >= 0.3 or (ValveLtime / ValveRtime) > 2 or ValveRtime >= 0.3 or (ValveRtime / ValveLtime) > 2:
    continueTask = UtilsR.dropdownmenu('WATER CALIBRATION ERROR. L: ' + str(ValveLtime) + ', R: ' + str(ValveRtime),
                                       'Continue with these values?', ['Yes', 'No'], 'No')
    if continueTask == 'No':
        UtilsR.stop('')

# GET SOUND CALIBRATION VALUES (BROADBAND)
try:
    LAmp, RAmp = UtilsR.getBroadbandCalib(conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT FIND BROADBAND CALIBRATION OF SOUNDDEVICE: ' + str(conf.VAR_BOX))

# INITIALIZE SOUND STREAM
try:
    sampleRate = 192000
    soundStream = SoundR(sampleRate=sampleRate, deviceOut=conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT INITIALIZE SOUND STREAM, CHECK SOUNDCARD INDEX')

# INITIALIZE VIDEO, PLAY AND RECORD
try:
    currSessionVid = os.path.expanduser('~/VIDEO_pybpod/' + (conf.PYBPOD_SUBJECTS[0]).split("'")[1] + '/')
    if not os.path.exists(os.path.expanduser(currSessionVid)):
        os.makedirs(os.path.expanduser(currSessionVid))
    cam = VideoR(indx_or_path=conf.VAR_BOX, name_video=conf.PYBPOD_SESSION + '.avi', path=currSessionVid,
                 title=conf.VAR_BOX, fps=120, codec_cam='X264', codec_video='X264')
    cam.play()
    if int(conf.VAR_REC) > 0:
        cam.record()
except:
    UtilsR.stop('COULD NOT INITIALIZE VIDEO, CHECK VIDEO INDEX')


########################### relevant variables for the task ##############################

# session-trials
nTrials = 2000
sessionTimeLength = 14400 # secs
sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart+sessionTimeLength

# stimuli (white-noise)
duration = 33 # stim len (secs). Does not imply playing whole stim each time
band_fs = [2000, 20000] # boundaries for the white noise (Hz)
filter_len = 10000 # filter len
# Next we assign sound vector. Returns np.array. How does this work? -> help(whiteNoiseGen)
# soundVec = whiteNoiseGen(1.0, band_fs[0], band_fs[1], duration, FsOut=XonarSamplingR, Fn=filter_len) 
# soundVec = ToneCloudGen(0.5, 1.0, samplingR=XonarSamplingR)
# using amplitude=1.0, adapt to calib values later.

counter_idle_stop_sound = 0


# with calibrations, get stim with envelopes of each side
Lstim,tonefreqsL = UtilsR.ToneCloudGen(0.1, LAmp, samplingR=sampleRate)
Rstim,tonefreqsR = UtilsR.ToneCloudGen(0.1, RAmp, samplingR=sampleRate) # just assigning first 2 items which are the sound vectors

print(Lstim)
print(len(Lstim))

##### trial list #######  trialTypes = [0, 1] 0=(rewarded left) or  1=(rewarded right)
coin = str(np.random.choice(['heads', 'tails'])) # from which side should be the first block

trial_list = []
starting_block_length = int(conf.VAR_BLEN) # 50 i guess is a good value to start with
for i in range(int(conf.VAR_BLEN)):
    blocklen= int(starting_block_length/2**i)
    if blocklen<10:
        break
    if coin == 'heads':
        trial_list += [0]*blocklen+[1]*blocklen
    else:
        trial_list += [1]*blocklen+[0]*blocklen

if coin=='heads':
    shortblock = [0]*10+[1]*10
else:
    shortblock = [1]*10+[0]*10

amount_missing = nTrials-len(trial_list)
pending_trials = shortblock*int(amount_missing/len(shortblock))
trial_list = trial_list + pending_trials

trial_list = [0,1]*50


def remainingTime():
    return sessionTimeEnding-time.time()


def my_softcode_handler(data):
    global start, timeit, soundStream
    print(data)
    if data == 68:
        print("Play")
        soundStream.playSound()
        start = timeit.default_timer()
    elif data == 66:
        soundStream.stopSound()
        print(':::::::::Time to stop:::::::::', timeit.default_timer() - start)
 
WaitForPoke_timer=29.5

######################## BPOD and States #########################
START_APP = timeit.default_timer()
my_bpod = Bpod()
my_bpod.register_value('STAGE_NUMBER', stage_number)
my_bpod.register_value('task_version', task_version)
my_bpod.register_value('REWARD_SIDE', trial_list) # required for trend plugin [0-left, 1-right]
my_bpod.register_value('left_amp', LAmp)
my_bpod.register_value('right_amp', RAmp)
my_bpod.register_value('left_valve', ValveLtime)
my_bpod.register_value('right_valve', ValveRtime)
my_bpod.softcode_handler_function = my_softcode_handler

for i in range(len(trial_list)):  # Main loop
    # go idle or not
    if counter_idle_stop_sound < 100:
        goIdle = 'Punish'
    else:
        goIdle = 'Idle'
    # define variables according to Left/Right trial

    if trial_list[i] == 0:
        valvetime = ValveLtime
        rewardValve = WATERL
        soundStream.load(Lstim[0], Lstim[1])
        pokechange = {PGLin: 'Reward', EventName.Tup: goIdle}
        rewardLED = (LEDL, 8)
    elif trial_list[i] == 1:
        valvetime = ValveRtime
        rewardValve = WATERR
        soundStream.load(Rstim[0], Rstim[1])
        pokechange = {PGRin: 'Reward', EventName.Tup: goIdle}
        rewardLED = (LEDR, 8)

    sma = StateMachine(my_bpod)
    sma.set_global_timer_legacy(timer_id=1, timer_duration=remainingTime())
    
    sma.add_state(
        state_name='StartSound',
        state_timer=0.25,
        state_change_conditions={Bpod.Events.Tup: 'stopsound'},
        output_actions=[(OutputChannel.SoftCode, 68), (OutputChannel.BNC1, 3)])
    sma.add_state(
        state_name='stopsound',
        state_timer=0.25,
        state_change_conditions={Bpod.Events.Tup: 'exit'},
        output_actions=[(OutputChannel.SoftCode, 66), (OutputChannel.BNC1, 0)])

    my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device
    my_bpod.run_state_machine(sma)  # Run state machine

    if time.time() > sessionTimeEnding:
        break

my_bpod.close()  # Disconnect Bpod and perform post-run actions
cam.stop()

print('EXECUTION TIME', timeit.default_timer() - START_APP)

# new reports:
os.system('datahandler -l ' + conf.PYBPOD_SESSION[:-7])


