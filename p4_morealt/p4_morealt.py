"""
required vars (all strings, transformed whithin the task script)= [
    VAR_BOX: str, box name (paired with bpod), eg 'BPOD01';
    VAR_REC: int, 0=do not record; 1=record;
    VAR_BLEN: int (even), block len;
    VAR_DATA: int, 0= do not save using rach util; 1= save data using R util,
    VAR_BNUM: int, block number]
    VAR_SSIDE: 'r'-random, '0'-left, '1'-right
    VAR_SBLOCK: 'r'-random, True - repetitive, 'False'- alternating
    VAR_VARIATION: 'standard', 'no_stimulus', 'high_volatility', 'low_volatility_alt', 'low_volatility_rep' just logging purposes
Update_status var = []

# known issues:
    - todo: set seed if random trials.

# Adapted by DDD @2020/07/03 from p4 to generate uneven blocks in the p4 RepAlt task with
longer Alt blocks than Rep blocks (ratio around 5:1, 100:20)
To do so, you need to:
1) Define the transition matrix "tm":
tm = [
  [0.2,0.2], # blocktype0 = Alt block p(rep=80 independent de si L o R)
  [0.8,0.8]  # blocktype1 = Rep block
]
2) Define the different block lengths in "block_lens" NO in "blen"
block_lens = [110,30] #starting in Alt block
3) Use UtilsR.newBlock instead of UtilsR.block
newBlock(tm, sblock=1, block_lens=[110,30]*17) # [110,30]*17 = around 2000 trials
See lines 154-163

"""

from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import user_settings as conf
from toolsR import SoundR, VideoR, UtilsR
import numpy as np
import os
import timeit
import time
import datetime
import csv
from scipy.signal import firwin, lfilter

# TASK
task_version = '0.5'  # changelog. new stuff: dropdown menu. REQUIRES update-variables checked
task_name = 'p4_morealt'
stage_number = 4
try:
    tasks_commit, md5sum = UtilsR.get_git_revision_hash('~/projects/tasks/', conf.PYBPOD_PROTOCOL)
except:
    tasks_commit, md5sum = ['coud not find git commit or md5checksum']*2

# PORTS
try:
    behavport_idx = (np.where(np.array(conf.BPOD_BEHAVIOR_PORTS_ENABLED) == True)[0] + 1).tolist()
    LEDL, LEDC, LEDR = [getattr(OutputChannel, f"PWM{num}") for num in behavport_idx]
    WATERL, WATERC, WATERR = [(getattr(OutputChannel, "Valve"), num) for num in behavport_idx]
    PGLin, PGCin, PGRin = [getattr(EventName, f"Port{num}In") for num in behavport_idx]
    PGLout, PGCout, PGRout = [getattr(EventName, f"Port{num}Out") for num in behavport_idx]
except:
    UtilsR.stop('THIS PROTOCOL REQUIRES 3 AND ONLY 3 ACTIVE PORTS, ACTIVATE THEM MANUALLY')

# GET WATER CALIBRATION VALUES
try:
    ValveLtime, ValveCtime, ValveRtime = UtilsR.getWaterCalib(conf.VAR_BOX, behavport_idx)
except:
    UtilsR.stop('COULD NOT FIND WATER CALIBRATION OF BOX: ' + str(conf.VAR_BOX))
if ValveLtime >= 0.3 or (ValveLtime / ValveRtime) > 2 or ValveRtime >= 0.3 or (ValveRtime / ValveLtime) > 2:
    continueTask = UtilsR.dropdownmenu('WATER CALIBRATION ERROR. L: ' + str(ValveLtime) + ', R: ' + str(ValveRtime),
                                       'Continue with these values?', ['Yes', 'No'], 'No')
    if continueTask == 'No':
        UtilsR.stop('')

# GET SOUND CALIBRATION VALUES (BROADBAND)
try:
    LAmp, RAmp = UtilsR.getBroadbandCalib(conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT FIND BROADBAND CALIBRATION OF SOUNDDEVICE: ' + str(conf.VAR_BOX))

# INITIALIZE SOUND STREAM
try:
    sampleRate = 192000
    soundStream = SoundR(sampleRate=sampleRate, deviceOut=conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT INITIALIZE SOUND STREAM, CHECK SOUNDCARD INDEX')

# INITIALIZE VIDEO, PLAY AND RECORD
try:
    currSessionVid = os.path.expanduser('~/VIDEO_pybpod/' + (conf.PYBPOD_SUBJECTS[0]).split("'")[1] + '/')
    if not os.path.exists(os.path.expanduser(currSessionVid)):
        os.makedirs(os.path.expanduser(currSessionVid))
    cam = VideoR(indx_or_path=conf.VAR_BOX, name_video=conf.PYBPOD_SESSION + '.avi', path=currSessionVid,
                 title=conf.VAR_BOX, fps=120, codec_cam='X264', codec_video='X264')
    cam.play()
    if int(conf.VAR_REC) > 0:
        cam.record()
except:
    UtilsR.stop('COULD NOT INITIALIZE VIDEO, CHECK VIDEO INDEX')

# VARIATION AND POSTOP
if 'VAR_VARIATION' in conf.PYBPOD_VARSNAMES:
    targetdic = ['standard', 'other...']
    targetdefault = "standard"
    targettitle = "Chose VARIATION value"
    targetlabel = "variation"

    conf.VAR_VARIATION = UtilsR.dropdownmenu(targettitle, targetlabel, targetdic, targetdefault)
    # assign blen and bnum + sblock according to variation
    if conf.VAR_VARIATION in ['standard', 'no_stimulus']:
        conf.VAR_BLEN, conf.VAR_BNUM, conf.VAR_SBLOCK = '80', '26', 'r'
    elif conf.VAR_VARIATION == 'high_volatility':
        conf.VAR_BLEN, conf.VAR_BNUM, conf.VAR_SBLOCK = '20', '100', 'r'
    elif conf.VAR_VARIATION.startswith('low_volatility'):
        conf.VAR_BLEN, conf.VAR_BNUM = '2000', '2'
        if conf.VAR_VARIATION.endswith('rep'):
            conf.VAR_SBLOCK = '1'
        elif conf.VAR_VARIATION.endswith('alt'):
            conf.VAR_SBLOCK = '0'

    # this will load vars from gui but log what user desires
    if conf.VAR_VARIATION == 'other...':
        question = 'type in variation'
        conf.VAR_VARIATION = UtilsR.inputmenu(targettitle, question)

if 'VAR_POSTOP' in conf.PYBPOD_VARSNAMES:
    targetdic = ['none', 'no_injection', 'saline_ppc', 'saline_mpfc', 'muscimol_ppc', 'muscimol_mpfc', 'electro',
                 'opto', 'lesion_mpfc', 'lesion_dms', 'other...']
    targetdefault = "none"
    targettitle = "Chose POSTOP value"
    targetlabel = "postop"

    conf.VAR_POSTOP = UtilsR.dropdownmenu(targettitle, targetlabel, targetdic, targetdefault)
    if conf.VAR_POSTOP == 'other...':
        question = 'type in postop'
        conf.VAR_POSTOP = UtilsR.inputmenu(targettitle, question)



########################### relevant variables for the task ##############################

### complete Trial list
if conf.VAR_SSIDE == 'r':
    startingSide = int(np.random.choice([0, 1]))  # which side to start
else:
    startingSide = int(conf.VAR_SSIDE)  # needs to be either 0 (left) or 1(right)

if conf.VAR_SBLOCK == 'r':
    startingBlock = bool(np.random.choice([True, False]))  # type of block
else:
    startingBlock = bool(int(conf.VAR_SBLOCK))

# NEW BLOCK DEFINES UNBALANCED BLOCKS WITH LARGER ALT THAN REP BLOCKS
# Define transition matrix
tm = [
  [0.2,0.2], # blocktype0 = Rep block p(rep=80 independent de si L o R)
  [0.8,0.8]  # blocktype1 = Alt block
]
# Define the different block lengths (larger for Alt: blocktype1)
unbalanced_len = list([110,30]) 
# Use UtilsR.newBlock instead of UtilsR.block (starting block = 1, Alt)
trial_list, prob_repeat = UtilsR.newBlock(tm, sblock=0, block_seq=[0,1]*17, block_lens=unbalanced_len*17) # [20,100]*17 = around 2000 trials

#trial_list, prob_repeat = UtilsR.block(startingSide, repetitive=startingBlock, bsize=int(conf.VAR_BLEN),
#                                       bnum=int(conf.VAR_BNUM))
trial_list = list(trial_list)

sessionTimeLength = 3000  # secs
sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart + sessionTimeLength

evidences = {
    'f1': (1, 0.5, 0.25, 0, -0.25, -0.5, -1)
}

# stimuli (white-noise)
duration = 1  # stim len (secs). Does not imply playing whole stim each time
band_fs = [2000, 20000]  # boundaries for the white noise (Hz)
filter_len = 10000  # filter len
# Next we assign sound vector. Returns np.array. How does this work? -> help(whiteNoiseGen)
soundVec = UtilsR.whiteNoiseGen(1.0, band_fs[0], band_fs[1], duration, FsOut=sampleRate,
                                Fn=filter_len)  # using amplitude=1.0, adapt to calib values later.


def my_softcode_handler(data):
    global start, timeit, soundStream, cam
    if data == 68:
        soundStream.playSound()
    elif data == 66:
        soundStream.stopSound()

def remainingTime():
    return sessionTimeEnding - time.time()

######################### BPOD and States ##################################
START_APP = timeit.default_timer()
my_bpod = Bpod()

my_bpod.register_value('TASK', [task_name, task_version, tasks_commit, md5sum])
my_bpod.register_value('STAGE_NUMBER', stage_number)

### WE NEED TO KEEP THIS, since it ccan be cchanged on the go, it first log wont be accurate (probably previous session one)
try:
    my_bpod.register_value('Variation', conf.VAR_VARIATION)
except:
    my_bpod.register_value('Variation', 'no variation of the task stated')
try:
    my_bpod.register_value('Postop', conf.VAR_POSTOP)  # dunno what will happen if it gets a None type
except:
    my_bpod.register_value('Postop', 'no postop stated')

my_bpod.register_value('REWARD_SIDE', trial_list)  # required for trend plugin [0-left, 1-right]
my_bpod.register_value('left_amp', LAmp)
my_bpod.register_value('right_amp', RAmp)
my_bpod.register_value('left_valve', ValveLtime)
my_bpod.register_value('right_valve', ValveRtime)

my_bpod.softcode_handler_function = my_softcode_handler

for i in range(len(trial_list)):  # Main loop
    timeleft = remainingTime()
    coh_to_chose_from = np.array(evidences['f1'])
    curr_coh = UtilsR.select_evidence(trial_list[i], coh_to_chose_from)

    if trial_list[i] == 0:
        valvetime = ValveLtime
        rewardValve = WATERL
        resp = {PGLin: 'Reward', PGRin: 'Punish', EventName.Tup: 'Invalid'}
        rewardLED = (LEDL, 8)
        centralLED = (LEDC, 8)
    elif trial_list[i] == 1:
        valvetime = ValveRtime
        rewardValve = WATERR
        resp = {PGRin: 'Reward', PGLin: 'Punish', EventName.Tup: 'Invalid'}
        rewardLED = (LEDR, 8)
        centralLED = (LEDC, 8)

    SL, SR, EL, ER = UtilsR.envelope(curr_coh, soundVec, 1, 20, samplingR=sampleRate, variance=0.015, randomized=False,
                                     paired=False, LAmp=LAmp, RAmp=RAmp)
    soundStream.load(SL, SR)
    my_bpod.register_value('coherence01', curr_coh)
    my_bpod.register_value('left_envelope', EL.tolist())
    my_bpod.register_value('right_envelope', ER.tolist())
    my_bpod.register_value('prob_repeat', prob_repeat[i])

    sma = StateMachine(my_bpod)
    sma.set_global_timer_legacy(timer_id=1, timer_duration=timeleft)

    sma.add_state(  # WaitCPoke
        state_name='WaitCPoke',
        state_timer=timeleft,
        state_change_conditions={PGCin: 'Fixation', Bpod.Events.GlobalTimer1_End: 'exit', EventName.Tup: 'exit'},
        output_actions=[centralLED, (Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(  # Fixation
        state_name='Fixation',
        state_timer=0.3,
        state_change_conditions={PGCout: 'WaitCPoke', EventName.Tup: 'StartSound'},
        output_actions=[centralLED])
    sma.add_state(  # StartSound
        state_name='StartSound',
        state_timer=1,
        state_change_conditions={PGCout: 'WaitResponse', EventName.Tup: 'WaitResponse'},
        # because it seems to get stuck here sometimes
        output_actions=[(OutputChannel.SoftCode, 68)])  # ,(Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(
        state_name='WaitResponse',
        state_timer=8,
        state_change_conditions=resp,
        output_actions=[(OutputChannel.SoftCode, 66)])
    sma.add_state(  # Reward
        state_name='Reward',
        state_timer=valvetime,
        state_change_conditions={EventName.Tup: 'keep-led-on'},
        output_actions=[rewardValve, rewardLED])
    sma.add_state(  # keep-led-on
        state_name='keep-led-on',
        state_timer=0.3 - valvetime,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[rewardLED])
    sma.add_state(  # Punish
        state_name='Punish',
        state_timer=2,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[])
    sma.add_state(
        state_name='Invalid',
        state_timer=0.001,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[])

    my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device
    my_bpod.run_state_machine(sma)  # Run state machine

    if time.time() > sessionTimeEnding:
        break

my_bpod.close()  # Disconnect Bpod and perform post-run actions
cam.stop()

print('EXECUTION TIME', timeit.default_timer() - START_APP)

# new reports:
os.system('datahandler -l ' + conf.PYBPOD_SESSION[:-7])
