from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import user_settings as conf
from toolsR import SoundR, VideoR, UtilsR
import numpy as np
import os
import time


# TASK
task_version = '0.1'
stage_number = 1
task_name = 'p2_freq'
try:
    tasks_commit, md5sum = UtilsR.get_git_revision_hash('~/projects/tasks/', conf.PYBPOD_PROTOCOL)
except:
    tasks_commit, md5sum = ['coud not find git commit or md5checksum']*2


# PORTS
try:
    behavport_idx = (np.where(np.array(conf.BPOD_BEHAVIOR_PORTS_ENABLED) == True)[0] + 1).tolist()
    LEDL, LEDC, LEDR = [getattr(OutputChannel, f"PWM{num}") for num in behavport_idx]
    WATERL, WATERC, WATERR = [(getattr(OutputChannel, "Valve"), num) for num in behavport_idx]
    PGLin, PGCin, PGRin = [getattr(EventName, f"Port{num}In") for num in behavport_idx]
    PGLout, PGCout, PGRout = [getattr(EventName, f"Port{num}Out") for num in behavport_idx]
except:
    UtilsR.stop('THIS PROTOCOL REQUIRES 3 AND ONLY 3 ACTIVE PORTS, ACTIVATE THEM MANUALLY')


# GET WATER CALIBRATION VALUES
try:
    ValveLtime, ValveCtime, ValveRtime = UtilsR.getWaterCalib(conf.VAR_BOX, behavport_idx)
except:
    UtilsR.stop('COULD NOT FIND WATER CALIBRATION OF BOX: ' + str(conf.VAR_BOX))
if ValveLtime >= 0.3 or (ValveLtime / ValveRtime) > 2 or ValveRtime >= 0.3 or (ValveRtime / ValveLtime) > 2:
    continueTask = UtilsR.dropdownmenu('WATER CALIBRATION ERROR. L: ' + str(ValveLtime) + ', R: ' + str(ValveRtime),
                                       'Continue with these values?', ['Yes', 'No'], 'No')
    if continueTask == 'No':
        UtilsR.stop('')


# GET SOUND CALIBRATION VALUES
try:
    sound1_freq = 6500
    sound2_freq = 31000
    sound1_left_amp, sound1_right_amp, sound2_left_amp, sound2_right_amp = UtilsR.getFrequencyCalib(conf.VAR_BOX, sound1_freq, sound2_freq)
except:
    UtilsR.stop('COULD NOT FIND BROADBAND CALIBRATION OF SOUNDDEVICE: ' + str(conf.VAR_BOX))


# INITIALIZE SOUND STREAM
try:
    sampleRate = 192000
    soundStream = SoundR(sampleRate=sampleRate, deviceOut=conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT INITIALIZE SOUND STREAM, CHECK SOUNDCARD INDEX')


# INITIALIZE VIDEO, PLAY AND RECORD
try:
    currSessionVid = os.path.expanduser('~/VIDEO_pybpod/' + (conf.PYBPOD_SUBJECTS[0]).split("'")[1] + '/')
    if not os.path.exists(os.path.expanduser(currSessionVid)):
        os.makedirs(os.path.expanduser(currSessionVid))
    cam = VideoR(indx_or_path=conf.VAR_BOX, name_video=conf.PYBPOD_SESSION + '.avi', path=currSessionVid,
                 title=conf.VAR_BOX, fps=120, codec_cam='X264', codec_video='X264')
    cam.play()
    if int(conf.VAR_REC) > 0:
        cam.record()
except:
    UtilsR.stop('COULD NOT INITIALIZE VIDEO, CHECK VIDEO INDEX')


# RELEVANT VARIABLES
sessionTimeLength = 5400  # secs
sessionTimeStart = time.time()
sessionTimeEnd = sessionTimeStart + sessionTimeLength

Lstim = UtilsR.generate_sound_freq(stim_evidence=0, sound_duration=120, modulator_freq=20,
                                   variance=0.015, sound1_freq=sound1_freq, sound2_freq=sound2_freq,
                                   sound1_left_amp=sound1_left_amp, sound1_right_amp=sound1_right_amp,
                                   sound2_left_amp=sound2_left_amp, sound2_right_amp=sound2_right_amp,
                                   out_freq=sampleRate)

Rstim = UtilsR.generate_sound_freq(stim_evidence=1, sound_duration=120, modulator_freq=20,
                                   variance=0.015, sound1_freq=sound1_freq, sound2_freq=sound2_freq,
                                   sound1_left_amp=sound1_left_amp, sound1_right_amp=sound1_right_amp,
                                   sound2_left_amp=sound2_left_amp, sound2_right_amp=sound2_right_amp,
                                   out_freq=sampleRate)

trial_list = []

if int(conf.VAR_EARLY_BLOCKS) == 1:
    coin = np.random.randint(2)
    for _ in range(2):
        if coin == 0:
            trial_list += [0] * 40 + [1] * 40
        else:
            trial_list += [1] * 40 + [0] * 40

extra = np.random.randint(2, size=1000)
trial_list += list(extra)

accwindow = [0] * 40


# BPOD AND SOFTCODE HANDLER
my_bpod = Bpod()

def my_softcode_handler(data):
    global soundStream
    if data == 68:
        soundStream.playSound()
    elif data == 66:
        soundStream.stopSound()

my_bpod.softcode_handler_function = my_softcode_handler


# REGISTER VALUES
my_bpod.register_value('STAGE_NUMBER', stage_number)
my_bpod.register_value('TASK', [task_name, task_version, tasks_commit, md5sum])
my_bpod.register_value('REWARD_SIDE', trial_list)  # required for trend plugin [0-left, 1-right]
my_bpod.register_value('sound1_freq', sound1_freq)
my_bpod.register_value('sound2_freq', sound2_freq)
my_bpod.register_value('sound1_left_amp', sound1_left_amp)
my_bpod.register_value('sound1_right_amp', sound1_right_amp)
my_bpod.register_value('sound2_left_amp', sound2_left_amp)
my_bpod.register_value('sound2_right_amp', sound2_right_amp)
my_bpod.register_value('left_valve', ValveLtime)
my_bpod.register_value('right_valve', ValveRtime)


# MAIN LOOP
for i in range(len(trial_list)):

    timeleft = sessionTimeEnd - time.time()

    if timeleft <= 0:
        break

    if trial_list[i] == 0:
        valvetime = ValveLtime
        rewardValve = WATERL
        soundStream.load(Lstim[0], Lstim[1])
        resp = {PGLin: 'Reward', PGRin: 'Punish'}
        rewardLED = (LEDL, 8)
        centralLED = (LEDC, 8)
    elif trial_list[i] == 1:
        valvetime = ValveRtime
        rewardValve = WATERR
        soundStream.load(Rstim[0], Rstim[1])
        resp = {PGRin: 'Reward', PGLin: 'Punish'}
        rewardLED = (LEDR, 8)
        centralLED = (LEDC, 8)

    sma = StateMachine(my_bpod)
    sma.set_global_timer_legacy(timer_id=1, timer_duration=timeleft)

    sma.add_state(
        state_name='WaitCPoke',
        state_timer=timeleft,
        state_change_conditions={PGCin: 'Fixation', Bpod.Events.GlobalTimer1_End: 'exit', EventName.Tup: 'exit'},
        output_actions=[centralLED, (Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(
        state_name='Fixation',
        state_timer=0.3,
        state_change_conditions={PGCout: 'WaitCPoke', EventName.Tup: 'StartSound'},
        output_actions=[centralLED])
    sma.add_state(
        state_name='StartSound',
        state_timer=1,
        state_change_conditions=resp,
        output_actions=[(OutputChannel.SoftCode, 68)])
    sma.add_state(
        state_name='Reward',
        state_timer=valvetime,
        state_change_conditions={EventName.Tup: 'keep-led-on'},
        output_actions=[rewardValve, rewardLED, (OutputChannel.SoftCode, 66)])
    sma.add_state(
        state_name='keep-led-on',
        state_timer=0.3 - valvetime,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[rewardLED])
    sma.add_state(
        state_name='Punish',
        state_timer=4,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[(OutputChannel.SoftCode, 66)])
    sma.add_state(  # we never come here, it exists in order the trend plugin works
        state_name='Invalid',
        state_timer=0.001,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[])

    my_bpod.send_state_machine(sma)
    my_bpod.run_state_machine(sma)

    trialstring = ("Current trial info: {0}".format(my_bpod.session.current_trial))

    if "'Punish': [(nan, nan)]" in trialstring:  # Punish state was not triggered = rat poked in the correct port
        accwindow = accwindow[1:] + [1]
    elif "'Reward': [(nan, nan)]" in trialstring:
        accwindow = accwindow[1:] + [0]


# CLOSING AND REPORTS
my_bpod.close()
cam.stop()
print('EXECUTION TIME', time.time() - sessionTimeStart)
result = 'performance: ' + str(sum(accwindow) / len(accwindow))
print(result)
os.system('datahandler -l ' + conf.PYBPOD_SESSION[:-7])
