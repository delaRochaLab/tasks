# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
required vars = [
    VAR_CAMIDX: int, camera index; 
    VAR_XONAR: int, soundcard index; 
    VAR_BOX: str, box name (paired with bpod), eg 'BPOD01'; 
    VAR_REC: int, 0=do not record; 1=record; 
    VAR_BLEN: int (even), block len; 50
    VAR_DATA: int, 0= do not save using rach util; 1= save data using R util,
    VAR_BNUM: int, block number]

Update_status var = []

# known issues:
    - keep-led-on will malfunction if valvetime > 0.3 s
"""

task_version = '0.2'
stage_number = 1
task_name = 'p1_chirps_2.02'
import user_settings as conf # to use vars: conf.VAR_0
from toolsR import VideoR
from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import timeit, numpy as np, time, datetime
from toolsR import SoundR
import os
from scipy.signal import firwin, lfilter
import sounddevice as sd # is this shiet being used? -del-
import subprocess
from toolsJ import *

########################### relevant variables for the task ##############################

# session-trials
nTrials = 150
sessionTimeLength = 14400 # secs
sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart+sessionTimeLength

# stimuli (white-noise)
duration = 33 # stim len (secs). Does not imply playing whole stim each time
band_fs = [2000, 20000] # boundaries for the white noise (Hz)
XonarSamplingR = 192000 # soundcard sampling rate
filter_len = 10000 # filter len
# Next we assign sound vector. Returns np.array. How does this work? -> help(whiteNoiseGen)
soundVec = whiteNoiseGen(5.0, band_fs[0], band_fs[1], duration, FsOut=XonarSamplingR, Fn=filter_len) # using amplitude=1.0, adapt to calib values later.
#soundVec = pureToneGen(1.0, 5000, duration, FsOut=XonarSamplingR,ramp=True) # using amplitude=1.0, adapt to calib values later.

counter_idle_stop_sound = 0

########################## pre-start, load stuff #########################################

### Calibrations
try:
    ValveLtime, ValveCtime, ValveRtime = getWaterCalib(conf.VAR_BOX) # retrieve water calibratino
except:
    #raise NameError('Could not find water calibration of '+str(conf.VAR_BOX)+'. Aborting.')
    ValveLtime, ValveCtime, ValveRtime = 0.1,0.1,0.125
try:
    LAmp, RAmp = getBroadbandCalib(int(conf.VAR_XONAR)) # retrieve broadband calibration
except:
    #raise NameError('Could not find broadband calibration of sounddevice '+str(conf.VAR_XONAR)+'. Aborting.')
    LAmp, RAmp = 1, 1

### Video (extracted from electro_p4 by AF)
cam_sync = {68: ["StartSound", 0, (330, 30)],
	    66: ["WaitResponse", 1, (360, 30)],
	    10: ["WaitCPoke", 2, (300, 30)]}
currSessionVid = os.path.expanduser('~/VIDEO_pybpod/'+(conf.PYBPOD_SUBJECTS[0]).split("'")[1]+'/') #  new way, ensure pybpod_subjects is a string (probably a list)
if not os.path.exists(os.path.expanduser(currSessionVid)):
    os.makedirs(os.path.expanduser(currSessionVid))
cam = VideoR(indx_or_path=int(conf.VAR_CAMIDX),
    name_video=conf.PYBPOD_SESSION+'.avi',
    path=currSessionVid,
    title=conf.VAR_BOX,
    fps=30,
#    cam_sync=cam_sync,
#    states_on_sync=True,
#    isColor=False,
    codec_cam='X264',
    codec_video='X264')
camOK = False
cam.play()
if int(conf.VAR_REC)>0:
        cam.record()

# with calibrations, get stim with envelopes of each side
Lstim = envelope(0, soundVec, 30, 30*20, LAmp=LAmp, RAmp=RAmp)[:2]
Rstim = envelope(1, soundVec, 30, 30*20, LAmp=LAmp, RAmp=RAmp)[:2] # just assigning first 2 items which are the sound vectors

print(Lstim)
print(len(Lstim))

### misc
LEDINT = 0 # from 0 to 255 (zero, no led)
LEDL, LEDC, LEDR = (OutputChannel.PWM1,LEDINT), (OutputChannel.PWM2,LEDINT), (OutputChannel.PWM3,LEDINT)

##### trial list #######  trialTypes = [0, 1] 0=(rewarded left) or  1=(rewarded right)
# Create an array of nTrials
trial_list = [0]*nTrials

# we will define a function so it gets evaluated in each trial. will it? :) To set the timer to exit the trial when session timer ends
def remainingTime():
    return sessionTimeEnding-time.time()

# Create the sound server 
soundStream = SoundR(sampleRate=XonarSamplingR, deviceOut=int(conf.VAR_XONAR), channelsOut=2) #change device depending on the sound card

def my_softcode_handler(data):
    global start, timeit, soundStream
    print(data)
    if data == 68:
        print("Play")
        soundStream.playSound()
        start = timeit.default_timer()
    elif data == 66:
        soundStream.stopSound()
        print(':::::::::Time to stop:::::::::', timeit.default_timer() - start)
 
WaitForPoke_timer=29.5

######################## BPOD and States #########################
START_APP = timeit.default_timer()
my_bpod = Bpod()
my_bpod.register_value('STAGE_NUMBER', stage_number)
my_bpod.register_value('task_version', task_version)
my_bpod.register_value('REWARD_SIDE', trial_list) # required for trend plugin [0-left, 1-right]
my_bpod.register_value('left_amp', LAmp)
my_bpod.register_value('right_amp', RAmp)
my_bpod.register_value('left_valve', ValveLtime)
my_bpod.register_value('right_valve', ValveRtime)
my_bpod.softcode_handler_function = my_softcode_handler


for i in range(len(trial_list)):  # Main loop
    # go idle or not
    if counter_idle_stop_sound < 100:
        goIdle = 'Punish'
    else:
        goIdle = 'Idle'

    # define variables according to Left/Right trial
    valvetime = ValveLtime
    rewardValve = (OutputChannel.Valve, 1)
    soundStream.load(Lstim, Rstim)
    pokechange = {EventName.Port1In:'Reward', EventName.Tup:  goIdle}
    rewardLED = LEDC

    sma = StateMachine(my_bpod)
    sma.set_global_timer_legacy(timer_id=1, timer_duration=remainingTime())
    
    sma.add_state(
        state_name='StartSound',
        state_timer=0.1,
        state_change_conditions={Bpod.Events.Tup: 'stopsound'},
        output_actions=[(OutputChannel.SoftCode, 68), (OutputChannel.BNC1, 3)])
    sma.add_state(
        state_name='stopsound',
        state_timer=0.4,
        state_change_conditions={Bpod.Events.Tup: 'exit'},
        output_actions=[(OutputChannel.SoftCode, 66), (OutputChannel.BNC1, 0)])


    my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device
    print(i)
    print("Waiting for poke. Reward: ", 'left' if trial_list[i] == 0 else 'right')
    my_bpod.run_state_machine(sma)  # Run state machine

my_bpod.close()  # Disconnect Bpod and perform post-run actions

cam.stop()


