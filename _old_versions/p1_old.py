# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
required vars = [
    VAR_CAMIDX: int, camera index; 
    VAR_XONAR: int, soundcard index; 
    VAR_BOX: str, box name (paired with bpod), eg 'BPOD01'; 
    VAR_REC: int, 0=do not record; 1=record; 
    VAR_BLEN: int (even), block len; 50
    VAR_DATA: int, 0= do not save using rach util; 1= save data using R util,
    VAR_BNUM: int, block number]

Update_status var = []

# known issues:
    - keep-led-on will malfunction if valvetime > 0.3 s
"""
task_version = '0.2'
stage_number = 1
task_name = 'p2'
from toolsR import SoundR
from toolsR import VideoR
import user_settings as conf # to use vars: conf.VAR_0
from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import timeit, numpy as np, time, datetime
import os, csv
from scipy.signal import firwin, lfilter
import sounddevice as sd
from toolsJ import *
from modular_report.main import main

def water_calib(board, ports):
    """
    Obtain the water calibration values saved in the log. ONLY FOR BOXES > 6, OTHERWISE USE 'getWaterCalib' INSIDE TOOLSJ.
    If it's the first time using a box, a calibration has to be performed first.
    """
    # Location of the CSV calibration datafile that contains past calibration results:
    log_name = os.path.expanduser("~/pluginsr-for-pybpod/water-calibration-plugin/DATA/water_calibration.csv")
    with open(log_name, 'r') as log:
        calibration_data = csv.DictReader(log, delimiter=';')
        latest_row = None
        for row in calibration_data:
            if row['board'] == board:
                latest_row = dict(row)
        if latest_row is None:
            raise Exception("Water calibration data not found.")
        else:
            results = latest_row['pulse_duration'].split("-")
            return [float(results[i-1]) for i in ports]

sessionDir= os.getcwd()

def get_git_revision_hash(dirpath):
    os.chdir(os.path.expanduser(dirpath))
    return subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()

try:
    tasks_commit = get_git_revision_hash('~/projects/tasks/')
except:
    tasks_commit = 'coud not find git commit'
########################### relevant variables for the task ##############################

# session-trials
nTrials = 2000
sessionTimeLength = 14400 # secs
sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart+sessionTimeLength

# stimuli (white-noise)
duration = 33 # stim len (secs). Does not imply playing whole stim each time
band_fs = [2000, 20000] # boundaries for the white noise (Hz)
XonarSamplingR = 192000 # soundcard sampling rate
filter_len = 10000 # filter len
# Next we assign sound vector. Returns np.array. How does this work? -> help(whiteNoiseGen)
soundVec = whiteNoiseGen(1.0, band_fs[0], band_fs[1], duration, FsOut=XonarSamplingR, Fn=filter_len) # using amplitude=1.0, adapt to calib values later.

counter_idle_stop_sound = 0

########################## pre-start, load stuff #########################################

### Calibrations
try:
    ValveLtime, ValveCtime, ValveRtime = water_calib(conf.VAR_BOX, [1,2,3]) # retrieve water calibratino
except:
    raise NameError('Could not find water calibration of '+str(conf.VAR_BOX)+'. Aborting.')
try:
    LAmp, RAmp = getBroadbandCalib(int(conf.VAR_XONAR)) # retrieve broadband calibration
except:
    raise NameError('Could not find broadband calibration of sounddevice '+str(conf.VAR_XONAR)+'. Aborting.')

### Video
#currSessionVid = os.path.expanduser('~/VIDEO_pybpod/'+conf.VAR_BOX+'/')
#currSessionName = 'test_'+datetime.datetime.now().strftime('%Y%m%d_%H%M') 
#if not os.path.exists(os.path.expanduser(currSessionVid)):
#    os.makedirs(os.path.expanduser(currSessionVid))
#cam = VideoR(int(conf.VAR_CAMIDX), path= currSessionVid, title=conf.VAR_BOX) #width='default', height='default',#  fps=60)#, fourcc_mjpg='default', showWindows=True)
#camOK = False                      
#cam.play()
#if int(conf.VAR_REC)>0:
#        cam.record()

### Video
#currSessionVid = os.path.expanduser('~/VIDEO_pybpod/'+conf.VAR_BOX+'/') # old way
currSessionVid = os.path.expanduser('~/VIDEO_pybpod/'+(conf.PYBPOD_SUBJECTS[0]).split("'")[1]+'/') #  new way, ensure pybpod_subjects is a string (probably a list)
#currSessionName = 'test_'+datetime.datetime.now().strftime('%Y%m%d_%H%M') # not used anymore
if not os.path.exists(os.path.expanduser(currSessionVid)):
    os.makedirs(os.path.expanduser(currSessionVid))
cam = VideoR(indx_or_path=int(conf.VAR_CAMIDX), name_video=conf.PYBPOD_SESSION+'.avi', path=currSessionVid, title=conf.VAR_BOX, fps=30, codec_cam='X264', codec_video='X264')
camOK = False
cam.play()
if int(conf.VAR_REC)>0:
        cam.record()

### Data
if int(conf.VAR_DATA) == 1:
    from toolsR import DataR
    currSessionPath = os.path.expanduser('~/DATA_pybpod/'+conf.VAR_BOX+'/') # VAR_BOX should look like 'BPOD01'
    if not os.path.exists(os.path.expanduser(currSessionPath)):
        os.makedirs(os.path.expanduser(currSessionPath))
    mydata = DataR(path=currSessionPath, name=currSessionName)

# with calibrations, get stim with envelopes of each side
Lstim = envelope(0, soundVec, 30, 30*20, LAmp=LAmp, RAmp=RAmp)[:2]
Rstim = envelope(1, soundVec, 30, 30*20, LAmp=LAmp, RAmp=RAmp)[:2] # just assigning first 2 items which are the sound vectors

### misc
LEDINT = 8 # from 0 to 255
LEDL, LEDC, LEDR = (OutputChannel.PWM1,LEDINT), (OutputChannel.PWM2,LEDINT), (OutputChannel.PWM3,LEDINT)


##### trial list #######  trialTypes = [0, 1] 0=(rewarded left) or  1=(rewarded right)
coin = str(np.random.choice(['heads', 'tails'])) # from which side should be the first block

trial_list = []
starting_block_length = int(conf.VAR_BLEN) # 50 i guess is a good value to start with
for i in range(int(conf.VAR_BLEN)):
    blocklen= int(starting_block_length/2**i)
    if blocklen<10:
        break
    if coin == 'heads':
        trial_list += [0]*blocklen+[1]*blocklen
    else:
        trial_list += [1]*blocklen+[0]*blocklen

if coin=='heads':
    shortblock = [0]*10+[1]*10
else:
    shortblock = [1]*10+[0]*10

amount_missing = nTrials-len(trial_list)
pending_trials = shortblock*int(amount_missing/len(shortblock))
trial_list = trial_list + pending_trials


# we will define a function so it gets evaluated in each trial. will it? :) To set the timer to exit the trial when session timer ends
def remainingTime():
    return sessionTimeEnding-time.time()

# Create the sound server 
soundStream = SoundR(sampleRate=XonarSamplingR, deviceOut=int(conf.VAR_XONAR), channelsOut=2) #change device depending on the sound card

# 
def my_softcode_handler(data):
    global start, timeit, soundStream
    print(data)
    if data == 68:
        print("Play")
        soundStream.playSound()
        start = timeit.default_timer()
    elif data == 66:
        soundStream.stopSound()
        print(':::::::::Time to stop:::::::::', timeit.default_timer() - start)
 
WaitForPoke_timer = 9.5

######################## BPOD and States #########################
START_APP = timeit.default_timer()
my_bpod = Bpod()
my_bpod.register_value('STAGE_NUMBER', stage_number)
my_bpod.register_value('TASK', [task_name, task_version, tasks_commit])
my_bpod.register_value('REWARD_SIDE', trial_list) # required for trend plugin [0-left, 1-right]
my_bpod.register_value('left_amp', LAmp)
my_bpod.register_value('right_amp', RAmp)
my_bpod.register_value('left_valve', ValveLtime)
my_bpod.register_value('right_valve', ValveRtime)
my_bpod.softcode_handler_function = my_softcode_handler


for i in range(len(trial_list)):  # Main loop
    # go idle or not
    if counter_idle_stop_sound < 5:
        goIdle = 'Punish'
    else:
        goIdle = 'Idle'
    # define variables according to Left/Right trial
    if trial_list[i] == 0:
        valvetime = ValveLtime
        rewardValve = (OutputChannel.Valve, 1)
        soundStream.load(Lstim[0], Lstim[1])
        pokechange = {EventName.Port1In:'Reward', EventName.Tup:  goIdle}
        rewardLED = LEDL
    elif trial_list[i] == 1:
        valvetime = ValveRtime
        rewardValve = (OutputChannel.Valve, 3)
        soundStream.load(Rstim[0], Rstim[1])
        pokechange = {EventName.Port3In:'Reward', EventName.Tup: goIdle}
        rewardLED = LEDR

    sma = StateMachine(my_bpod)
    sma.set_global_timer_legacy(timer_id=1, timer_duration=remainingTime())
    
    sma.add_state(
        state_name='StartSound',
        state_timer=0.2,
        state_change_conditions={Bpod.Events.Tup: 'waterDelivery'},
        output_actions=[(OutputChannel.SoftCode, 68),(Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(
        state_name='waterDelivery',
        state_timer=valvetime,
        state_change_conditions={EventName.Tup: 'keep-led-on'},
        output_actions=[rewardValve, rewardLED])
    sma.add_state(
        state_name='keep-led-on',
        state_timer=0.3-valvetime,
        state_change_conditions={EventName.Tup: 'WaitForPoke'},
        output_actions=[rewardLED])
    sma.add_state(
        state_name='WaitForPoke', 
        state_timer=WaitForPoke_timer, 
        state_change_conditions= pokechange,
        output_actions=[])
    sma.add_state(
        state_name='Reward', # original poke_stop_sound: need to rename it to make trend plugin work
        state_timer=3,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[(OutputChannel.SoftCode, 66)])
    sma.add_state(
        state_name='Punish',# original idle_stop_sound: need to replace it in order the trend plugin works
        state_timer=3,
        state_change_conditions= {EventName.Tup: 'exit'},
        output_actions=[(OutputChannel.SoftCode, 66)]) # change this shioot
    sma.add_state(
        state_name='Idle',
        state_timer=1,
        state_change_conditions={EventName.Port1In:'Punish', EventName.Port2In:'Punish', EventName.Port3In:'Punish', Bpod.Events.GlobalTimer1_End: 'exit'},
        output_actions=[(OutputChannel.SoftCode, 66)])


    my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device
    print("Waiting for poke. Reward: ", 'left' if trial_list[i] == 0 else 'right')
    my_bpod.run_state_machine(sma)  # Run state machine

    trialstring = ("Current trial info: {0}".format(my_bpod.session.current_trial))
    if i>=200: # just can inter in iddle state after 200 trials
        if "'Punish': [(nan, nan)]" in trialstring:# Punish state was not triggered = aka rat poked in the correct port
            counter_idle_stop_sound = 0 # therfore reset counter
        elif "'Reward': [(nan, nan)]" in trialstring: # rat did not poke to correct, so it's a 'punish' trial
            counter_idle_stop_sound += 1 
            if "'Port1In':" in trialstring or "'Port2In':" in trialstring or "'Port3In':" in trialstring: # however the rat may have poked in a wrong port (punish but rat is not sleeping) or leaving idle state
                counter_idle_stop_sound = 0 # reset counter

    print("Current idle-counter: {0}".format(counter_idle_stop_sound))
    ''' removing this, wasnt adding anything good
    if WaitForPoke_timer>3:
        WaitForPoke_timer -= 0.14
    '''
    if int(conf.VAR_DATA)==1:
        tt = my_bpod.session.current_trial
        mydata.add(tt.export())
    if time.time()>sessionTimeEnding:
        break
my_bpod.close()  # Disconnect Bpod and perform post-run actions

cam.stop()

if int(conf.VAR_DATA)==1:
    mydata.save()

print('EXECUTION TIME', timeit.default_timer() - START_APP)
os.chdir(sessionDir)
#new reports:
main(os.path.join(conf.PYBPOD_SESSION_PATH, conf.PYBPOD_SESSION + ".csv"))

