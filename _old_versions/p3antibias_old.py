# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
required vars = [
    VAR_CAMIDX: int, camera index; 
    VAR_XONAR: int, soundcard index; 
    VAR_BOX: str, box name (paired with bpod), eg 'BPOD01'; 
    VAR_REC: int, 0=do not record; 1=record; 
    VAR_DATA: int, 0= do not save using rach util; 1= save data using R util,
 

Update_status var = [
    #if setup==animal hack,
    update VAR_KSO at the end of the session  (+40 ms?)

]

# known issues:
    - keep-led-on will malfunction if valvetime > 0.3 s
    - todo: set seed if random trials. 

 - Cube matrix pending, is it really worth?
"""
task_version = 'v0.1'
task_name = 'p3antibias'
stage_number = 3

import user_settings as conf # to use vars: conf.VAR_0
from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import timeit, numpy as np, time, datetime
from toolsR import SoundR, VideoR
import os, csv
from scipy.signal import firwin, lfilter
import sounddevice as sd
from toolsJ import *

def water_calib(board, ports):
    """
    Obtain the water calibration values saved in the log. ONLY FOR BOXES > 6, OTHERWISE USE 'getWaterCalib' INSIDE TOOLSJ.
    If it's the first time using a box, a calibration has to be performed first.
    """
    # Location of the CSV calibration datafile that contains past calibration results:
    log_name = os.path.expanduser("~/pluginsr-for-pybpod/water-calibration-plugin/DATA/water_calibration.csv")
    with open(log_name, 'r') as log:
        calibration_data = csv.DictReader(log, delimiter=';')
        latest_row = None
        for row in calibration_data:
            if row['board'] == board:
                latest_row = dict(row)
        if latest_row is None:
            raise Exception("Water calibration data not found.")
        else:
            results = latest_row['pulse_duration'].split("-")
            return [float(results[i-1]) for i in ports]

## check whether xonar idx is among possible ones. (particularly usefull for the one that's usually switching positions)
scheck = [x.lstrip().split(' Xonar DX')[0] for x in str(SoundR.getDevices()).split('\n') if 'Multichannel' in x]
if conf.VAR_XONAR not in scheck:
    raise ValueError('wrong VAR_XONAR, could not find respective soundcard')
del scheck

#cam
camcheck = os.popen("ls /dev/video*").read().split('\n')
if ('/dev/video'+conf.VAR_CAMIDX) not in camcheck:
    raise ValueError('wrong cam idx')
del camcheck

########################### relevant variables for the task ##############################

# session-trials
nTrials = 2000
sessionTimeLength = 4500 # secs
nSubstages = 10
#substageWindow = 5 # not used anymore

sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart+sessionTimeLength
trial_list = np.random.choice([0,1], nTrials).tolist() # random reward side

accwindow = [0]*25
left_accwindow = [0]*25
right_accwindow = [0]*25
invalidcounter = 0
validcounter = 0

evidences = {}
KSO = {}
ez, mid, hard  = np.array([1]*nSubstages), np.around(np.linspace(0.866, 0.5, num=nSubstages),3), np.around(np.linspace(0.732, 0.25, num=nSubstages),3)
for i in range(nSubstages):
    evidences[i]= (ez[i], mid[i], hard[i], -1*hard[i], -1*mid[i], -1*ez[i])
    KSO[i]= np.around(np.linspace(400, 0, num=10),1)[i]

# this become almost useless because now we are using integers as keys for the dicts
# then with a single int (substage = availableSubstages[currentSubstage]) we can call values for both dics (evidences['e'+str(availableSubstages[currentSubstage])]) and same with KSO but KSO['KSO'+...]
# keep things easy. by now use linearly distributed evidences and KSO
def decr_steps(start, stop, numbers, factor):
    step=np.linspace(0, start-stop, num=numbers)
    shift= np.linspace(factor, 1, num=numbers)
    return np.repeat(start, numbers)-step*shift

# changed strategy to match with evidences / dict
#KSO_list = list(np.linspace(350, 0, num=10))
### think about this a bit if <70 decr diff; if >75: incr diff (from 70 to 75 keep diff)
'''
e_thr = { # probably remove this:
    'e0': (0, .95),
    'e1': (.85, .90),
    'e2': (.80, .85),
    'e3': (.75, .80),
    'e4': (.70, .75)
}
'''
#stage3sessionNumb = 0 # conf.VAR_SESSION
startingSubstage = int(conf.VAR_SUBSTAGE) ## cannot go below this (should be an int in the range [0,9])
availableSubstages_num = np.where(np.linspace(9,0,num=10) == startingSubstage)[0][0]#+1 not anymore # +1 because 0-indexed
availableSubstages_list = np.arange(startingSubstage, nSubstages, step=1).tolist()
#availableSubstages_list = ev_list[len(ev_list)-availableSubstages_num:]
currentSubstageIdx = 0 # start with 0 ## !!!! update vars or this wont work propperly

# by now ignore this stratgy
# we shift the available evidences according to accuracy and session number
#threshold_pos = 0 # means e0 = substage[0]
# threshold_pos = 5 means e0 = substage[5] 6,7,8, 9 # hardest
# todo: if reaachest ceiling, shift thresholds (substages). There's no way back

#KeepSoundOn = int(conf.VAR_KSO) #### wont use this anymore


# stimuli (white-noise)
duration = 1 # stim len (secs). Does not imply playing whole stim each time
band_fs = [2000, 20000] # boundaries for the white noise (Hz)
XonarSamplingR = 192000 # soundcard sampling rate
filter_len = 10000 # filter len
# Next we assign sound vector. Returns np.array. How does this work? -> help(whiteNoiseGen)
soundVec = whiteNoiseGen(1.0, band_fs[0], band_fs[1], duration, FsOut=XonarSamplingR, Fn=filter_len) # using amplitude=1.0, adapt to calib values later.

########################## pre-start, load stuff #########################################

### Calibrations
try:
    ValveLtime, ValveCtime, ValveRtime = water_calib(conf.VAR_BOX, [1,2,3]) # retrieve water calibratino
except:
    raise NameError('Could not find water calibration of '+str(conf.VAR_BOX)+'. Aborting.')
try:
    LAmp, RAmp = getBroadbandCalib(int(conf.VAR_XONAR)) # retrieve broadband calibration
except:
    raise NameError('Could not find broadband calibration of sounddevice '+str(conf.VAR_XONAR)+'. Aborting.')

### Video
currSessionVid = os.path.expanduser('~/VIDEO_pybpod/'+conf.VAR_BOX+'/')
currSessionName = 'test_'+datetime.datetime.now().strftime('%Y%m%d_%H%M')
if not os.path.exists(os.path.expanduser(currSessionVid)):
    os.makedirs(os.path.expanduser(currSessionVid))
cam = VideoR(indx_or_path=int(conf.VAR_CAMIDX), name_video=conf.PYBPOD_SESSION+'.avi', path=currSessionVid, title=conf.VAR_BOX, fps=30, codec_cam='X264', codec_video='X264')
camOK = False                      
cam.play()
if int(conf.VAR_REC)>0:
        cam.record()

### Data
if int(conf.VAR_DATA) == 1:
    from toolsR import DataR
    currSessionPath = os.path.expanduser('~/DATA_pybpod/'+conf.VAR_BOX+'/') # VAR_BOX should look like 'BPOD01'
    if not os.path.exists(os.path.expanduser(currSessionPath)):
        os.makedirs(os.path.expanduser(currSessionPath))
    mydata = DataR(path=currSessionPath, name=currSessionName)

### misc
LEDINT = 8 # from 0 to 255
LEDL, LEDC, LEDR = (OutputChannel.PWM1,LEDINT), (OutputChannel.PWM2,LEDINT), (OutputChannel.PWM3,LEDINT)

# Create the sound server 
soundStream = SoundR(sampleRate=XonarSamplingR, deviceOut=int(conf.VAR_XONAR), channelsOut=2) #change device depending on the sound card

def my_softcode_handler(data):
    global start, timeit, soundStream, cam
    #print(data)
    if data == 68:
        #print("Play")
        soundStream.playSound()
        #start = timeit.default_timer()
    elif data == 66:
        soundStream.stopSound()
        #print(':::::::::Time to stop:::::::::', timeit.default_timer() - start)
        
def remainingTime():
    return sessionTimeEnding-time.time()

######################### BPOD and States ##################################
START_APP = timeit.default_timer()
my_bpod = Bpod()
my_bpod.register_value('TASK', [task_name, task_version])
my_bpod.register_value('STAGE_NUMBER', stage_number)
my_bpod.register_value('REWARD_SIDE', trial_list) # required for trend plugin [0-left, 1-right] 
my_bpod.register_value('left_amp', LAmp)
my_bpod.register_value('right_amp', RAmp)
my_bpod.register_value('left_valve', ValveLtime)
my_bpod.register_value('right_valve', ValveRtime)

my_bpod.softcode_handler_function = my_softcode_handler
# testing
ValveLtime = round(ValveLtime, 3)
ValveRtime = round(ValveRtime, 3)

for i in range(0,len(trial_list),1):  # Main loop
    coh_to_chose_from = np.array(evidences[availableSubstages_list[currentSubstageIdx]]) # fixed evidences
    KeepSoundOn = KSO[availableSubstages_list[currentSubstageIdx]]
    curr_coh =  select_evidence(trial_list[i], coh_to_chose_from)

    if trial_list[i] == 0:
        valvetime = ValveLtime
        rewardValve = (OutputChannel.Valve, 1)
        pokechange = {EventName.Port1In:'Reward', EventName.Port3In:'Punish',EventName.Tup:'WaitResponse'}
        resp = {EventName.Port1In:'Reward', EventName.Port3In:'Punish', EventName.Tup:'Invalid'}
        rewardLED = LEDL
    elif trial_list[i] == 1:
        valvetime = ValveRtime
        rewardValve = (OutputChannel.Valve, 3)
        pokechange = {EventName.Port3In:'Reward', EventName.Port1In:'Punish', EventName.Tup:'WaitResponse'}
        resp = {EventName.Port3In:'Reward', EventName.Port1In:'Punish', EventName.Tup:'Invalid'}
        rewardLED = LEDR
    # time this,
    SL,SR, EL, ER = envelope(curr_coh, soundVec, 1, 20, samplingR=XonarSamplingR, variance=0.015, randomized=False, paired=False, LAmp=LAmp, RAmp=RAmp)
    soundStream.load(SL, SR)
    my_bpod.register_value('KeepSoundOn', KeepSoundOn)
    my_bpod.register_value('coherence01', curr_coh)
    my_bpod.register_value('left_envelope', EL.tolist())
    my_bpod.register_value('right_envelope', ER.tolist())

    sma = StateMachine(my_bpod)
    # sma.set_global_timer_legacy(timer_id=1, timer_duration=remainingTime())
    
    sma.add_state(#WaitCPoke
        state_name='WaitCPoke',
        state_timer=remainingTime(),
        state_change_conditions={Bpod.Events.Port2In:'Fixation', EventName.Tup: 'exit'},
        output_actions=[LEDC]) #,(Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(#Fixation
        state_name='Fixation',
        state_timer=0.3,
        state_change_conditions={Bpod.Events.Port2Out:'Invalid', EventName.Tup:'StartSound'},
        output_actions=[LEDC])
    sma.add_state(#StartSound
        state_name='StartSound',
        state_timer=0.1,
        state_change_conditions={EventName.Port2Out:'KeepSoundOn'},
        output_actions=[(OutputChannel.SoftCode, 68)]) #,(Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(
        state_name='KeepSoundOn', # = time that it takes to shut the sound of since the animal pokes out of the central port
        state_timer=round((KeepSoundOn/1000)+0.001, 3),
        state_change_conditions=pokechange,
        output_actions=[])
    sma.add_state(
        state_name='WaitResponse',
        state_timer=round(8-(KeepSoundOn/1000)-0.001, 3),
        state_change_conditions=resp,
        output_actions=[(OutputChannel.SoftCode, 66)])
    sma.add_state(#Reward
        state_name='Reward',
        state_timer=valvetime,
        state_change_conditions={EventName.Tup: 'keep-led-on'},
        output_actions=[(OutputChannel.SoftCode, 66), rewardValve, rewardLED])
    sma.add_state(#keep-led-on
        state_name='keep-led-on',
        state_timer=0.3-valvetime,
        state_change_conditions= {EventName.Tup: 'exit'},
        output_actions=[rewardLED])
    sma.add_state(#Punish
        state_name='Punish',
        state_timer=2,
        state_change_conditions= {EventName.Tup: 'exit'},
        output_actions=[(OutputChannel.SoftCode, 66)])
    sma.add_state(
        state_name='Invalid',
        state_timer=0.001,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[])

    my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device

    print("Waiting for poke. Reward: ", 'left' if trial_list[i] == 0 else 'right')

    my_bpod.run_state_machine(sma)  # Run state machine

    trialstring = ("Current trial info: {0}".format(my_bpod.session.current_trial))
    if "'Punish': [(nan, nan)]" in trialstring and "'Reward': [(nan, nan)]" in trialstring: # invalid trial. They are not taken into account to calc current accwindow.
        invalidcounter += 1
    else:
        validcounter += 1
        if "'Punish': [(nan, nan)]" in trialstring:# Punish state was not triggered = rat poked in the correct port. This one is enough since it is binary, modify when introdicing invalid trials
            accwindow = accwindow[1:]+[1]
            if trial_list[i]==0:
                left_accwindow = left_accwindow[1:]+[1]
            else:
                right_accwindow = right_accwindow[1:]+[1]
        elif "'Reward': [(nan, nan)]" in trialstring:
            accwindow = accwindow[1:]+[0]
            if trial_list[i]==0:
                left_accwindow = left_accwindow[1:]+[0]
            else:
                right_accwindow = right_accwindow[1:]+[0]

    if validcounter%5==0: # too high
        if sum(accwindow)/len(accwindow)>0.78: # increase diff
            if availableSubstages_list[currentSubstageIdx]==9: # already top dif
                if len(availableSubstages_list)>1:
                    availableSubstages_list = availableSubstages_list[1:]
                    # need to decr index because list is shorter and we already reached last item
                    currentSubstageIdx -=1
            else:
                
                if len(availableSubstages_list)>5:
                    availableSubstages_list = availableSubstages_list[1:] # as we shorten the list by left side, there's no need to increase the index
                else:
                    currentSubstageIdx += 1
        elif sum(accwindow)/len(accwindow)<0.72: # decrease diff,
            if currentSubstageIdx>0:
                currentSubstageIdx -= 1
            # no need to do else, because if idx=0 we cannot decrease the diff more
    # check wheter apply antibias or not
    if validcounter%25==0 and i>45:
        side_acc_dif = sum(left_accwindow)/len(left_accwindow) - sum(right_accwindow)/len(right_accwindow)
        if side_acc_dif > 0:
            tobump = 1
        else:
            tobump = 0

        if abs(side_acc_dif)>0.24:
            trial_list=antibias(tobump, 25, trial_list, 0.45, i)
        elif abs(side_acc_dif)>0.16:
            trial_list=antibias(tobump, 25, trial_list, 0.30, i)
        elif abs(side_acc_dif)>0.08:
            trial_list=antibias(tobump, 25, trial_list, 0.15, i)
        # if alterning trial_list, aknowledge again so plugin keeps representing actual trials
        if abs(side_acc_dif)>0.08:
            my_bpod.register_value('REWARD_SIDE', trial_list)

    #sav section
    if int(conf.VAR_DATA)==1:
        tt = my_bpod.session.current_trial
        mydata.add(tt.export())

    if time.time()>sessionTimeEnding:
        break
## execute before closing bpod // update var required
if availableSubstages_list[currentSubstageIdx]>=2:
    if len(availableSubstages_list)>2:
        if currentSubstageIdx>1:
            conf.VAR_SUBSTAGE = str(availableSubstages_list[currentSubstageIdx]-2)
        else:
            conf.VAR_SUBSTAGE = str(availableSubstages_list[currentSubstageIdx])
    else:
        conf.VAR_SUBSTAGE = str(availableSubstages_list[currentSubstageIdx])
else:
    conf.VAR_SUBSTAGE = str(availableSubstages_list[currentSubstageIdx]) # next session rat will start with this one

my_bpod.register_value('TOTAL_TRIALS', i)
my_bpod.register_value('VALID_TRIALS', validcounter)
my_bpod.register_value('INVALID_TRIALS', invalidcounter)
my_bpod.close()  # Disconnect Bpod and perform post-run actions

cam.stop()
if int(conf.VAR_DATA) == 1:
    mydata.save()

print('EXECUTION TIME', timeit.default_timer() - START_APP)
import glob, sys
sys.path.append(os.path.expanduser('~/pybpod/'))
from daily_reports import report
report.main(glob.glob('*.csv')[0])
