# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
P4_ONLY_LIGHT TASK. A clone of the p4 task without sound (only light stimulus) and only alternating blocks.

Required vars (all strings, transformed whithin the task script)= [
    VAR_CAMIDX: int, camera index; 
    VAR_BOX: str, box name (paired with bpod), eg 'BPOD01'; 
    VAR_REC: int, 0=do not record; 1=record; 
    VAR_BLEN: int (even), block len;
    VAR_DATA: int, 0= do not save using rach util; 1= save data using R util,
    VAR_BNUM: int, block number]
    VAR_SSIDE: 'r'-random, '0'-left, '1'-right
    VAR_VARIATION: 'standard', 'no_stimulus', 'high_volatility', 'low_volatility_alt', 'low_volatility_rep' just logging purposes
Update_status var = []

# known issues:
    - keep-led-on will malfunction if valvetime > 0.3 s
    - todo: set seed if random trials. 

"""
task_version = '0.1' # First revision of the p4_u task.
task_name = 'p4_only_light'
stage_number = 4
import user_settings as conf # to use vars: conf.VAR_0
from toolsR import VideoR
from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import timeit, numpy as np, time, datetime
import os, csv
from scipy.signal import firwin, lfilter
import subprocess
from toolsJ import *
from modular_report.main import main

def water_calib(board, ports):
    """
    Obtain the water calibration values saved in the log. ONLY FOR BOXES > 6, OTHERWISE USE 'getWaterCalib' INSIDE TOOLSJ.
    If it's the first time using a box, a calibration has to be performed first.
    """
    # Location of the CSV calibration datafile that contains past calibration results:
    log_name = os.path.expanduser("~/pluginsr-for-pybpod/water-calibration-plugin/DATA/water_calibration.csv")
    with open(log_name, 'r') as log:
        calibration_data = csv.DictReader(log, delimiter=';')
        latest_row = None
        for row in calibration_data:
            if row['board'] == board:
                latest_row = dict(row)
        if latest_row is None:
            raise Exception("Water calibration data not found.")
        else:
            results = latest_row['pulse_duration'].split("-")
            return [float(results[i-1]) for i in ports]

sessionDir = os.getcwd()

def get_git_revision_hash(dirpath):
    os.chdir(os.path.expanduser(dirpath))
    return subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()

try:
    tasks_commit = get_git_revision_hash('~/projects/tasks/')
except:
    tasks_commit = 'coud not find git commit'

#cam
camcheck = os.popen("ls /dev/video*").read().split('\n')
if ('/dev/video'+conf.VAR_CAMIDX) not in camcheck:
    raise ValueError('wrong cam idx')
del camcheck

### GET WORKING PORTS and new var-aliases:
if sum(conf.BPOD_BEHAVIOR_PORTS_ENABLED)!=3:
    raise ValueError("this protocol requires 3 and only 3 active ports, activate them manually")
behavport_idx = (np.where(np.array(conf.BPOD_BEHAVIOR_PORTS_ENABLED)==True)[0]+1).tolist() # 1-based indexing, because of bpod | use it right away for valves

# here it comes Albert's suggestion to definetely wipe any remaining readability
LEDL, LEDC, LEDR = [(getattr(OutputChannel, f"PWM{num}"), 8) for num in behavport_idx]

# same but with photogates
PGLin, PGCin, PGRin = [getattr(EventName, f"Port{num}In") for num in behavport_idx]
PGLout, PGCout, PGRout = [getattr(EventName, f"Port{num}Out") for num in behavport_idx]

# no more forgetting logs.
if 'VAR_VARIATION' in conf.PYBPOD_VARSNAMES:
    from tkinter import Tk, simpledialog
    targetdic = ['standard', 'no_stimulus', 'high_volatility', 'low_volatility_alt', 'low_volatility_rep', 'other...']
    targetdefault = "standard"
    targettitle = "Chose VARIATION value"
    targetlabel = "variation"

    conf.VAR_VARIATION = dropdownmenu(targettitle, targetlabel,targetdic,targetdefault)
    # assign blen and bnum + sblock according to variation
    if conf.VAR_VARIATION in ['standard', 'no_stimulus']:
        conf.VAR_BLEN, conf.VAR_BNUM, conf.VAR_SBLOCK='80','26','r'
    elif conf.VAR_VARIATION=='high_volatility':
        conf.VAR_BLEN, conf.VAR_BNUM, conf.VAR_SBLOCK='20','100','r'
    elif conf.VAR_VARIATION.startswith('low_volatility'):
        conf.VAR_BLEN, conf.VAR_BNUM='2000','2'
        if conf.VAR_VARIATION.endswith('rep'):
            conf.VAR_SBLOCK='1'
        elif conf.VAR_VARIATION.endswith('alt'):
            conf.VAR_SBLOCK='0'

    # this will load vars from gui but log what user desires 
    if conf.VAR_VARIATION == 'other...':
        suppl = Tk()
        suppl.title(targettitle)
        conf.VAR_VARIATION = simpledialog.askstring('type in variation', suppl)
        suppl.destroy()

if 'VAR_POSTOP' in conf.PYBPOD_VARSNAMES:
    from tkinter import Tk, simpledialog
    targetdic = ['none', 'no_injection', 'saline_ppc', 'saline_mpfc', 'muscimol_ppc', 'muscimol_mpfc', 'electro', 'opto', 'lesion_mpfc', 'lesion_dms', 'other...']
    targetdefault = "none"
    targettitle = "Chose POSTOP value"
    targetlabel = "postop"

    conf.VAR_POSTOP = dropdownmenu(targettitle, targetlabel,targetdic,targetdefault)
    if conf.VAR_POSTOP == 'other...':
        suppl = Tk()
        suppl.title(targettitle)
        conf.VAR_POSTOP = simpledialog.askstring('type in postop', suppl)
        suppl.destroy()

# ----------- REWARD SIDE CREATION -----------------------------------------
if conf.VAR_SSIDE == 'r':
    startingSide = int(np.random.choice([0, 1])) # which side to start
else:
    startingSide = int(conf.VAR_SSIDE) # needs to be either 0 (left) or 1(right)

if conf.VAR_SBLOCK == 'r':
    startingBlock = bool(np.random.choice([True, False])) # type of block
else:
    startingBlock = bool(int(conf.VAR_SBLOCK))

def reward_side(tm, ss='None', sblock='None', bnum=26, blen=80, block_seq='None', block_lens='None'):
    '''if any block_seq and are provided, this function should ignore bnum and/or blen, huehue future to do
    tm: transition matrix 2d array; rows being block type, and cols p(repeat when L, repeat when R), complementary p(alt) will be 1-p(rep) since its 2afc
    ss: starting side (0=L, 1=R)
    sblock= starting block (row index of corresponding probs in tm)
    bnum = total blocks
    blen = trial length of each block
    block_seq= list of blocktypes
    block_lens= list of lengths (1 item = 1 block)
    '''  
    # since pybpod is just getting strs ok we'll just use str Nones
    if ss == 'None':
        ss=np.random.choice([0,1])
    if sblock == 'None': # this shouldnt happen inside the function because it's desirable to log first Block type
        sblock = np.random.choice(np.arange(tm.shape[0]))
    if block_seq == 'None':
        block_seq = np.tile(np.roll(np.arange(tm.shape[0]),-sblock), int(bnum/tm.shape[0])+tm.shape[0])[:bnum]
    if block_lens == 'None':
        block_lens = np.repeat(blen, bnum)
    out = [ss]
    prob_repeat = [] # len = len(out)-1 !! # agree about how to stardardize this, leaving it as it was
    # first block having 1st arbitrary side
    for btype, curr_blen in zip(block_seq, block_lens): # ensure both zipped lists/arrays have the same length, else it can lead to unexpected behavior will happen
        for j in range(curr_blen):
            c_r_prob = tm[btype, out[-1]]
            out += [np.random.choice([out[-1],(out[-1]-1)**2], p=[c_r_prob, 1-c_r_prob])]
            prob_repeat += [c_r_prob]
     
    return out, prob_repeat

# Block definition and creation:
block_order = {'random': 0, 'alternating': 1, 'repeating': 2}
block_prob = {
                'random': {'random': 0, 'alternating': 0.0, 'repeating': 0.0},
                'alternating': {'random': 0.0, 'alternating': 1, 'repeating': 0.0},
                'repeating': {'random': 0.0, 'alternating': 0.0, 'repeating': 0}
             }
block_types = list(block_prob.keys())
starting_block_dict = {'u': 'random', 'r': 'None', 'alt': 'alternating', 'rep': 'repeating'}
# First block, either specified or random:
try:
    starting_block = 'alternating'
except KeyError:
    raise Exception("Starting block variable VAR_SBLOCK is not configured properly. See the task header.")

if starting_block != 'None':
    block_seq = [starting_block]
else:
    block_seq = [np.random.choice(block_types)]
for jj in range(int(conf.VAR_BNUM)-1):
    probs = [element[1] for element in block_prob[block_seq[jj]].items()]
    block_seq.append(np.random.choice(block_types, p=probs))
block_seq = np.vectorize(block_order.get)(block_seq)

t_matrix = np.array([[.5, .5], [.2, .2], [.8, .8]])
trial_list, prob_repeat = reward_side(t_matrix, ss=startingSide, sblock='None', block_seq=block_seq, bnum=int(conf.VAR_BNUM), blen=int(conf.VAR_BLEN))
trial_list = list(trial_list)

sessionTimeLength = 300 # in seconds; 5 minutes
sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart+sessionTimeLength

########################## pre-start, load stuff #########################################

### Calibrations
try:
    ValveLtime, ValveCtime, ValveRtime = water_calib(conf.VAR_BOX, [1,2,3]) # retrieve water calibratino
except:
    raise NameError('Could not find water calibration of '+str(conf.VAR_BOX)+'. Aborting.')

### Video
#currSessionVid = os.path.expanduser('~/VIDEO_pybpod/'+conf.VAR_BOX+'/') # old way
currSessionVid = os.path.expanduser('~/VIDEO_pybpod/'+(conf.PYBPOD_SUBJECTS[0]).split("'")[1]+'/') #  new way, ensure pybpod_subjects is a string (probably a list)
#currSessionName = 'test_'+datetime.datetime.now().strftime('%Y%m%d_%H%M') # not used anymore
if not os.path.exists(os.path.expanduser(currSessionVid)):
    os.makedirs(os.path.expanduser(currSessionVid))
cam = VideoR(indx_or_path=int(conf.VAR_CAMIDX), name_video=conf.PYBPOD_SESSION+'.avi', path=currSessionVid, title=conf.VAR_BOX, fps=30, codec_cam='X264', codec_video='X264') # 120fps MJPG
camOK = False                      
cam.play()
if int(conf.VAR_REC)>0:
        cam.record()
        
def remainingTime():
    return sessionTimeEnding-time.time()

invalidcounter = 0
validcounter = 0
######################### BPOD and States ##################################
START_APP = timeit.default_timer()
my_bpod = Bpod()

my_bpod.register_value('TASK', [task_name, task_version, tasks_commit])
my_bpod.register_value('STAGE_NUMBER', stage_number)

### WE NEED TO KEEP THIS, since it ccan be cchanged on the go, it first log wont be accurate (probably previous session one)
try:
    my_bpod.register_value('Variation', conf.VAR_VARIATION)
except:
    my_bpod.register_value('Variation', 'no variation of the task stated')
try:
    my_bpod.register_value('Postop', conf.VAR_POSTOP) # dunno what will happen if it gets a None type
except:
    my_bpod.register_value('Postop', 'no postop stated')

my_bpod.register_value('REWARD_SIDE', trial_list) # required for trend plugin [0-left, 1-right]
my_bpod.register_value('left_valve', ValveLtime)
my_bpod.register_value('right_valve', ValveRtime)

for i in range(0, len(trial_list),1):  # Main loop
    timeleft = remainingTime()
    
    if trial_list[i] == 0:
        valvetime = ValveLtime
        rewardValve = (OutputChannel.Valve, behavport_idx[0])
        resp = {PGLin:'Reward', PGRin:'Punish', EventName.Tup:'Invalid'}
        rewardLED = LEDL
    elif trial_list[i] == 1:
        valvetime = ValveRtime
        rewardValve = (OutputChannel.Valve, behavport_idx[2])
        resp = {PGRin:'Reward', PGLin:'Punish', EventName.Tup:'Invalid'}
        rewardLED = LEDR
    # time this,

    my_bpod.register_value('prob_repeat', prob_repeat[i])

    sma = StateMachine(my_bpod)
    sma.set_global_timer_legacy(timer_id=1, timer_duration=timeleft)
    
    sma.add_state(#WaitCPoke
        state_name='WaitCPoke',
        state_timer=timeleft,
        state_change_conditions={PGCin:'Fixation', Bpod.Events.GlobalTimer1_End: 'exit', EventName.Tup: 'exit'},
        output_actions=[LEDC, (Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(#Fixation
        state_name='Fixation',
        state_timer=0.3,
        state_change_conditions={PGCout:'WaitCPoke', EventName.Tup:'StartStimulus'},
        output_actions=[LEDC])
    sma.add_state(#StartStimulus
        state_name='StartStimulus',
        state_timer=1,
        state_change_conditions={PGCout:'WaitResponse', EventName.Tup:'WaitResponse'}, # because it seems to get stuck here sometimes
        output_actions=[(rewardLED)])#,(Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(
        state_name='WaitResponse',
        state_timer=8,
        state_change_conditions=resp,
        output_actions=[(rewardLED)])
    sma.add_state(#Reward
        state_name='Reward',
        state_timer=valvetime,
        state_change_conditions={EventName.Tup: 'sleep'},
        output_actions=[(rewardValve)])
    sma.add_state(#keep-led-on
        state_name='sleep',
        state_timer=0.3-valvetime,
        state_change_conditions= {EventName.Tup: 'exit'},
        output_actions=[])
    sma.add_state(#Punish
        state_name='Punish',
        state_timer=2,
        state_change_conditions= {EventName.Tup: 'exit'},
        output_actions=[])
    sma.add_state(
        state_name='Invalid',
        state_timer=0.001,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[])

    my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device
    print("Waiting for poke. Reward: ", 'left' if trial_list[i] == 0 else 'right')
    my_bpod.run_state_machine(sma)  # Run state machine

    trialstring = ("Current trial info: {0}".format(my_bpod.session.current_trial))# remove all
    if "'Punish': [(nan, nan)]" in trialstring and "'Reward': [(nan, nan)]" in trialstring: # invalid trial. They are not taken into account to calc current accwindow.
        invalidcounter += 1
    else:
        validcounter += 1
    #remove 
    if int(conf.VAR_DATA)==1:
        tt = my_bpod.session.current_trial
        mydata.add(tt.export())

    if time.time()>sessionTimeEnding:
        break


# Since daily reports are working smoothly i'll remove this soon as well 
my_bpod.register_value('TOTAL_TRIALS', i) # remove
my_bpod.register_value('VALID_TRIALS', validcounter) #remove
my_bpod.register_value('INVALID_TRIALS', invalidcounter) # remove
my_bpod.close()  # Disconnect Bpod and perform post-run actions

cam.stop()
if int(conf.VAR_DATA) == 1:#remove
    mydata.save()

print('EXECUTION TIME', timeit.default_timer() - START_APP)

os.chdir(sessionDir)
#new reports:
main(os.path.join(conf.PYBPOD_SESSION_PATH, conf.PYBPOD_SESSION + ".csv"))
