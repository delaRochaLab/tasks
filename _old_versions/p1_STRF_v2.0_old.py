# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
required vars = [
    VAR_CAMIDX: int, camera index;
    VAR_XONAR: int, soundcard index;
    VAR_BOX: str, box name (paired with bpod), eg 'BPOD01';
    VAR_REC: int, 0=do not record; 1=record;
    VAR_BLEN: int (even), block len; 150
    VAR_DATA: int, 0= do not save using rach util; 1= save data using R util,
    VAR_BNUM: int, block number]

Update_status var = []

# known issues:
    - keep-led-on will malfunction if valvetime > 0.3 s

@07/03/2019: Adapted by DDD to allow for characterization of neural responses by
presenting TORCs (temporally orthogonal ripple sound) and for construction of
STRFs (spectro-temporal receptive fields)
- Lines 55-61 (defines variables for TORC passive presentation)
- Lines 98-115 (creates random trial_list, consisting in 5 repetitions of 30 TORCs)
- Lines 156-184 (change loading of sounds in main loop and saves TORC name)
@12/03/2019: Modified after testing with Yerko to get rid of calibration and Video problems.
@18/03/2019: line 235 value changed from 1 --> 3 to make sound lasts for 3 sec

"""
task_version = '0.2'
stage_number = 1
task_name = 'p1_STRFs'
from toolsR import SoundR
from toolsR import VideoR
import user_settings as conf # to use vars: conf.VAR_0
from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import timeit, numpy as np, time, datetime
import os
from scipy.signal import firwin, lfilter
import sounddevice as sd
import soundfile as sf
from toolsJ import *
from modular_report.main import main
import csv

class soundRep:
    def __init__(self, fs):
	
	    self.fs = fs
	    self.Rstim = None
	    self.Lstim = None

    def load_sound(self, Lstim, Rstim):
	
	    self.Rstim = Rstim	
	    self.Lstim = Lstim

    def play_sound(self):

	    sd.play(self.Rstim, self.fs)
	    sd.play(self.Lstim, self.fs)
	

sessionDir= os.getcwd()

def get_git_revision_hash(dirpath):
    os.chdir(os.path.expanduser(dirpath))
    return subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()

try:
    tasks_commit = get_git_revision_hash('~/projects/tasks/')
except:
    tasks_commit = 'coud not find git commit'

########################### relevant variables for the task ####################
# Edited by DDD @07/03/2019
# session-trials (there are normally 30 TORCs, we should play them 5 times at least)
nTrials = 150
# TORCs length are around 3 s, so we should have at least 6 seconds length trials
# 1 s pre-silence + 3 s sound + 1 s post-silence (total 750 s, 12,5 min)
sessionTimeLength = 1000 # secs
sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart+sessionTimeLength

counter_idle_stop_sound = 0

########################## pre-start, load stuff ###############################
# Get the calibration for the valves:
def water_calib(board, ports):
    log_name = os.path.expanduser("~/pluginsr-for-pybpod/water-calibration-plugin/DATA/water_calibration.csv")
    with open(log_name, 'r') as log:
        calibration_data = csv.DictReader(log, delimiter=';')
        latest_row = None
        for row in calibration_data:
            if row['board'] == board:
                latest_row = dict(row)
        if latest_row is None:
            raise Exception("Water calibration data not found.")
        else:
            results = latest_row['pulse_duration'].split("-")
            return [float(results[i-1]) for i in ports]

### Calibrations (lines 75 & 80 commented by @YF)
#try:
#    print(conf.VAR_BOX)
#    ValveLtime, ValveCtime, ValveRtime = water_calib(conf.VAR_BOX, [1,2,3]) # retrieve water calibration
#except:
#    raise NameError('Could not find water calibration of '+str(conf.VAR_BOX)+'. Aborting.')
try:
    LAmp, RAmp = getBroadbandCalib(int(conf.VAR_XONAR)) # retrieve broadband calibration
except:
    #raise NameError('Could not find broadband calibration of sounddevice '+str(conf.VAR_XONAR)+'. Aborting.')
    LAmp,RAmp=1,1

### OLD Video (changed by AF lines)
#currSessionVid = os.path.expanduser('~/VIDEO_pybpod/'+conf.VAR_BOX+'/')
#currSessionName = 'test_'+datetime.datetime.now().strftime('%Y%m%d_%H%M')
#if not os.path.exists(os.path.expanduser(currSessionVid)):
#    os.makedirs(os.path.expanduser(currSessionVid))
#cam = VideoR(int(conf.VAR_CAMIDX), path= currSessionVid, title=conf.VAR_BOX) #width='default', height='default',#  fps=60)#, fourcc_mjpg='default', showWindows=True)
#camOK = False
#cam.play()
#if int(conf.VAR_REC)>0:
#        cam.record()

### sacado de electro_p4 from AF
### Video
cam_sync = {68: ["StartSound", 0, (330, 30)],
	    66: ["WaitResponse", 1, (360, 30)],
	    10: ["WaitCPoke", 2, (300, 30)]}
#currSessionVid = os.path.expanduser('~/VIDEO_pybpod/'+conf.VAR_BOX+'/') # old way
currSessionVid = os.path.expanduser('~/VIDEO_pybpod/'+(conf.PYBPOD_SUBJECTS[0]).split("'")[1]+'/') #  new way, ensure pybpod_subjects is a string (probably a list)
#currSessionName = 'test_'+datetime.datetime.now().strftime('%Y%m%d_%H%M') # not used anymore
if not os.path.exists(os.path.expanduser(currSessionVid)):
    os.makedirs(os.path.expanduser(currSessionVid))
cam = VideoR(indx_or_path=int(conf.VAR_CAMIDX),
    name_video=conf.PYBPOD_SESSION+'.avi',
    path=currSessionVid,
    title=conf.VAR_BOX,
    fps=30,
    codec_cam='X264',
    codec_video='X264')

camOK = False
cam.play()
if int(conf.VAR_REC)>0:
        cam.record()

###########################
# Edited by DDD @07/03/2019
# Load folder for stimuli
soundDir= "/home/delarocha7/Downloads/p1_STRFs/Sounds"

# Create list of 150 sounds to play (trial_list)
# It creates 5 iterations of 30 TORCs
trial_list = []
for iterations in range(5):
    torc_list = np.random.choice(list(range(1,31)), 30, replace=False)
    for sounds in torc_list:
        soundname = str(sounds)
        # if sound length is 1, add a zero at the begining
        if len(soundname) ==1:
            soundname = soundname.zfill(2)
        filepath = os.path.join(soundDir, "TORC_424_"+soundname+"_u501.wav")
        # filepath = os.path.join(soundDir, "TORC_448_"+soundname+"_z501.wav")
        trial_list += [filepath]

print(trial_list)

### misc (NO LED!!! Set intensity to zero)
LEDINT = 0 # from 0 to 255
LEDL, LEDC, LEDR = (OutputChannel.PWM1,LEDINT), (OutputChannel.PWM2,LEDINT), (OutputChannel.PWM3,LEDINT)

# we will define a function so it gets evaluated in each trial. will it? :)
# To set the timer to exit the trial when session timer ends
def remainingTime():
    return sessionTimeEnding-time.time()

# Create the sound server (change device depending on the sound card)
#soundStream = soundRep(80000)
soundStream = SoundR(sampleRate=88200, deviceOut=int(conf.VAR_XONAR), channelsOut=2)

def my_softcode_handler(data):
    global start, timeit, soundStream
    print(data)
    if data == 68:
        print("Play")
        soundStream.playSound()
        start = timeit.default_timer()
    elif data == 66:
        soundStream.stopSound()
        print(':::::::::Time to stop:::::::::', timeit.default_timer() - start)

WaitForPoke_timer=29.5
list_trial=[0]*150

######################## BPOD and States #########################
START_APP = timeit.default_timer()
my_bpod = Bpod()
my_bpod.register_value('STAGE_NUMBER', stage_number)
my_bpod.register_value('TASK', [task_name, task_version, tasks_commit])
my_bpod.register_value('REWARD_SIDE', list_trial) # required for trend plugin [0-left, 1-right]
my_bpod.register_value('left_amp', LAmp)
my_bpod.register_value('right_amp', RAmp)
#my_bpod.register_value('left_valve', ValveLtime)
#my_bpod.register_value('right_valve', ValveRtime)
my_bpod.softcode_handler_function = my_softcode_handler

###########################
# Edited by DDD @07/03/2019
# Main loop
for i in range(len(trial_list)):
    # go idle or not
    if counter_idle_stop_sound < 5:
        goIdle = 'Punish'
    else:
        goIdle = 'Idle'

    #valvetime = ValveLtime
    rewardValve = (OutputChannel.Valve, 1)

    # Edited by DDD @07/03/2019
    # Same sound in both speakers (eliminate the rewardValve info)
    ############
    # For tests:
    # i = 0; RAmp = 1; LAmp=0.9
    soundVec, fs = sf.read(trial_list[i], dtype='float32')
    print(f'fs: {fs}')

    # with calibrations (LAmp, RAmp), get stim with envelopes of each side
    RStim = soundVec*RAmp
    LStim = soundVec*LAmp

    #################################
    # Tests if sounds work (they do!)
    # import sounddevice as sd
    # sd.play(RStim, fs)
    # sd.play(LStim, fs)

    # SoundStream.load loads first the left speaker and then the right one
    soundStream.load(LStim, RStim)
    pokechange = {EventName.Port1In:'Reward', EventName.Tup:  goIdle}
    rewardLED = LEDC
    # Save TORC name
    my_bpod.register_value('TORC', os.path.split(trial_list[i])[-1])

    sma = StateMachine(my_bpod)
    sma.set_global_timer_legacy(timer_id=1, timer_duration=remainingTime())

    sma.add_state(
        state_name='StartSound',
        state_timer=3,
        state_change_conditions={Bpod.Events.Tup: 'stopsound'},
        output_actions=[(OutputChannel.SoftCode, 68), (OutputChannel.BNC1, 3)])
    sma.add_state(
        state_name='stopsound',
        state_timer=2,
        state_change_conditions={Bpod.Events.Tup: 'exit'},
        output_actions=[(OutputChannel.BNC1, 0)])

    my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device
    print("Waiting for poke. Reward: ", 'left' if trial_list[i] == 0 else 'right')
    my_bpod.run_state_machine(sma)  # Run state machine
    
    if time.time()>sessionTimeEnding:
        break

my_bpod.close()  # Disconnect Bpod and perform post-run actions

cam.stop()

print('EXECUTION TIME', timeit.default_timer() - START_APP)
os.chdir(sessionDir)
#new reports:
main(os.path.join(conf.PYBPOD_SESSION_PATH, conf.PYBPOD_SESSION + ".csv"))
