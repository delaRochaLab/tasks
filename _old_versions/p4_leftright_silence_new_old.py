# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
required vars (all strings, transformed whithin the task script)= [
    VAR_CAMIDX: int, camera index; 
    VAR_XONAR: int, soundcard index; 
    VAR_BOX: str, box name (paired with bpod), eg 'BPOD01'; 
    VAR_REC: int, 0=do not record; 1=record; 
    VAR_BLEN: int (even), block len;
    VAR_DATA: int, 0= do not save using rach util; 1= save data using R util,
    VAR_BNUM: int, block number]
    VAR_SSIDE: 'r'-random, '0'-left, '1'-right
    VAR_SBLOCK: 'r'-random, True - repetitive, 'False'- alternating
    VAR_VARIATION: 'standard', 'no_stimulus', 'high_volatility', 'low_volatility_alt', 'low_volatility_rep' just logging purposes
Update_status var = []

# known issues:
    - keep-led-on will malfunction if valvetime > 0.3 s
    - todo: set seed if random trials. 

# edited by DDD 29/01/2019 to add silence trials (modified in evidences (-2) and coherences)

"""
task_version = '0.5' # changelog. new stuff: dropdown menu. REQUIRES update-variables checked
task_name = 'p4'
stage_number = 4
import user_settings as conf # to use vars: conf.VAR_0
from toolsR import VideoR
from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import timeit, numpy as np, time, datetime
from toolsR import SoundR
import os, csv
from scipy.signal import firwin, lfilter
import sounddevice as sd # is this shiet being used? -del-
import subprocess
from toolsJ import *
from modular_report.main import main

def water_calib(board, ports):
    """
    Obtain the water calibration values saved in the log. ONLY FOR BOXES > 6, OTHERWISE USE 'getWaterCalib' INSIDE TOOLSJ.
    If it's the first time using a box, a calibration has to be performed first.
    """
    # Location of the CSV calibration datafile that contains past calibration results:
    log_name = os.path.expanduser("~/pluginsr-for-pybpod/water-calibration-plugin/DATA/water_calibration.csv")
    with open(log_name, 'r') as log:
        calibration_data = csv.DictReader(log, delimiter=';')
        latest_row = None
        for row in calibration_data:
            if row['board'] == board:
                latest_row = dict(row)
        if latest_row is None:
            raise Exception("Water calibration data not found.")
        else:
            results = latest_row['pulse_duration'].split("-")
            return [float(results[i-1]) for i in ports]

sessionDir = os.getcwd()

def get_git_revision_hash(dirpath):
    os.chdir(os.path.expanduser(dirpath))
    return subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()

try:
    tasks_commit = get_git_revision_hash('~/projects/tasks/')
except:
    tasks_commit = 'coud not find git commit'

## check whether xonar idx is among possible ones. (particularly usefull for the one that's usually switching positions)
scheck = [x.lstrip().split(' Xonar DX')[0] for x in str(SoundR.getDevices()).split('\n') if 'Multichannel' in x]
if conf.VAR_XONAR not in scheck:
    raise ValueError('wrong VAR_XONAR, could not find respective soundcard')
del scheck

#cam
camcheck = os.popen("ls /dev/video*").read().split('\n')
if ('/dev/video'+conf.VAR_CAMIDX) not in camcheck:
    raise ValueError('wrong cam idx')
del camcheck

### GET WORKING PORTS and new var-aliases:
if sum(conf.BPOD_BEHAVIOR_PORTS_ENABLED)!=3:
    raise ValueError("this protocol requires 3 and only 3 active ports, activate them manually")
behavport_idx = (np.where(np.array(conf.BPOD_BEHAVIOR_PORTS_ENABLED)==True)[0]+1).tolist() # 1-based indexing, because of bpod | use it right away for valves

# here it comes Albert's suggestion to definetely wipe any remaining readability
LEDL, LEDC, LEDR = [(getattr(OutputChannel, f"PWM{num}"), 8) for num in behavport_idx]

# same but with photogates
PGLin, PGCin, PGRin = [getattr(EventName, f"Port{num}In") for num in behavport_idx]
PGLout, PGCout, PGRout = [getattr(EventName, f"Port{num}Out") for num in behavport_idx]

# no more forgetting logs.
if 'VAR_VARIATION' in conf.PYBPOD_VARSNAMES:
    from tkinter import Tk, simpledialog
    targetdic = ['standard', 'leftright', 'no_stimulus', 'high_volatility', 'low_volatility_alt', 'low_volatility_rep', 'other...']
    targetdefault = "standard"
    targettitle = "Chose VARIATION value"
    targetlabel = "variation"

    conf.VAR_VARIATION = dropdownmenu(targettitle, targetlabel,targetdic,targetdefault)
    # assign blen and bnum + sblock according to variation
    if conf.VAR_VARIATION in ['standard', 'no_stimulus']:
        conf.VAR_BLEN, conf.VAR_BNUM, conf.VAR_SBLOCK='80','26','r'
    elif conf.VAR_VARIATION=='high_volatility':
        conf.VAR_BLEN, conf.VAR_BNUM, conf.VAR_SBLOCK='20','100','r'
    elif conf.VAR_VARIATION.startswith('low_volatility'):
        conf.VAR_BLEN, conf.VAR_BNUM='2000','2'
        if conf.VAR_VARIATION.endswith('rep'):
            conf.VAR_SBLOCK='1'
        elif conf.VAR_VARIATION.endswith('alt'):
            conf.VAR_SBLOCK='0'



    # this will load vars from gui but log what user desires 
    if conf.VAR_VARIATION == 'other...':
        suppl = Tk()
        suppl.title(targettitle)
        conf.VAR_VARIATION = simpledialog.askstring('type in variation', suppl)
        suppl.destroy()

if 'VAR_POSTOP' in conf.PYBPOD_VARSNAMES:
    from tkinter import Tk, simpledialog
    targetdic = ['none', 'no_injection', 'saline_ppc', 'saline_mpfc', 'muscimol_ppc', 'muscimol_mpfc', 'electro', 'opto', 'lesion_mpfc', 'lesion_dms', 'other...']
    targetdefault = "none"
    targettitle = "Chose POSTOP value"
    targetlabel = "postop"

    conf.VAR_POSTOP = dropdownmenu(targettitle, targetlabel,targetdic,targetdefault)
    if conf.VAR_POSTOP == 'other...':
        suppl = Tk()
        suppl.title(targettitle)
        conf.VAR_POSTOP = simpledialog.askstring('type in postop', suppl)
        suppl.destroy()

########################### relevant variables for the task ##############################

### complete Trial list
if conf.VAR_SSIDE == 'r':
    startingSide = int(np.random.choice([0, 1])) # which side to start
else:
    startingSide = int(conf.VAR_SSIDE) # needs to be either 0 (left) or 1(right)

if conf.VAR_SBLOCK == 'r':
    startingBlock = bool(np.random.choice([True, False])) # type of block
else:
    startingBlock = bool(int(conf.VAR_SBLOCK))

trial_list, prob_repeat = block_side_enhanced(startingSide, startingBlock, bsize=int(conf.VAR_BLEN), bnum=int(conf.VAR_BNUM))
trial_list = list(trial_list)

sessionTimeLength = 4500 # secs
sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart+sessionTimeLength

evidences = {
    'f1':(1, 0.5, 0.25, 0, -0.25, -0.5, -1)
}

# stimuli (white-noise)
duration = 1 # stim len (secs). Does not imply playing whole stim each time
band_fs = [2000, 20000] # boundaries for the white noise (Hz)
XonarSamplingR = 192000 # soundcard sampling rate
filter_len = 10000 # filter len
# Next we assign sound vector. Returns np.array. How does this work? -> help(whiteNoiseGen)
soundVec = whiteNoiseGen(1.0, band_fs[0], band_fs[1], duration, FsOut=XonarSamplingR, Fn=filter_len) # using amplitude=1.0, adapt to calib values later.

########################## pre-start, load stuff #########################################

### Calibrations
try:
    ValveLtime, ValveCtime, ValveRtime = water_calib(conf.VAR_BOX, [1,2,3]) # retrieve water calibratino
except:
    raise NameError('Could not find water calibration of '+str(conf.VAR_BOX)+'. Aborting.')
try:
    LAmp, RAmp = getBroadbandCalib(int(conf.VAR_XONAR)) # retrieve broadband calibration
except:
    raise NameError('Could not find broadband calibration of sounddevice '+str(conf.VAR_XONAR)+'. Aborting.')

### Video
#currSessionVid = os.path.expanduser('~/VIDEO_pybpod/'+conf.VAR_BOX+'/') # old way
currSessionVid = os.path.expanduser('~/VIDEO_pybpod/'+(conf.PYBPOD_SUBJECTS[0]).split("'")[1]+'/') #  new way, ensure pybpod_subjects is a string (probably a list)
#currSessionName = 'test_'+datetime.datetime.now().strftime('%Y%m%d_%H%M') # not used anymore
if not os.path.exists(os.path.expanduser(currSessionVid)):
    os.makedirs(os.path.expanduser(currSessionVid))
cam = VideoR(indx_or_path=int(conf.VAR_CAMIDX), name_video=conf.PYBPOD_SESSION+'.avi', path=currSessionVid, title=conf.VAR_BOX, fps=30, codec_cam='X264', codec_video='X264') # 120fps MJPG
camOK = False                      
cam.play()
if int(conf.VAR_REC)>0:
        cam.record()

### Data, deprecated (should remove it in next couple of commits)
'''
if int(conf.VAR_DATA) == 1:
    from toolsR import DataR
    currSessionPath = os.path.expanduser('~/DATA_pybpod/'+conf.VAR_BOX+'/') # VAR_BOX should look like 'BPOD01'
    if not os.path.exists(os.path.expanduser(currSessionPath)):
        os.makedirs(os.path.expanduser(currSessionPath))
    mydata = DataR(path=currSessionPath, name=currSessionName)
'''

# Create the sound server 
soundStream = SoundR(sampleRate=XonarSamplingR, deviceOut=int(conf.VAR_XONAR), channelsOut=2) #change device depending on the sound card

def my_softcode_handler(data):
    global start, timeit, soundStream, cam
    #print(data)
    if data == 68:
        #print("Play")
        soundStream.playSound()
        #start = timeit.default_timer()
    elif data == 66:
        soundStream.stopSound()
        #print(':::::::::Time to stop:::::::::', timeit.default_timer() - start)
        
def remainingTime():
    return sessionTimeEnding-time.time()

invalidcounter = 0
validcounter = 0
######################### BPOD and States ##################################
START_APP = timeit.default_timer()
my_bpod = Bpod()

my_bpod.register_value('TASK', [task_name, task_version, tasks_commit])
my_bpod.register_value('STAGE_NUMBER', stage_number)

### WE NEED TO KEEP THIS, since it ccan be cchanged on the go, it first log wont be accurate (probably previous session one)
try:
    my_bpod.register_value('Variation', conf.VAR_VARIATION)
except:
    my_bpod.register_value('Variation', 'no variation of the task stated')
try:
    my_bpod.register_value('Postop', conf.VAR_POSTOP) # dunno what will happen if it gets a None type
except:
    my_bpod.register_value('Postop', 'no postop stated')

my_bpod.register_value('REWARD_SIDE', trial_list) # required for trend plugin [0-left, 1-right]
my_bpod.register_value('left_amp', LAmp)
my_bpod.register_value('right_amp', RAmp)
my_bpod.register_value('left_valve', ValveLtime)
my_bpod.register_value('right_valve', ValveRtime)

my_bpod.softcode_handler_function = my_softcode_handler

for i in range(0,len(trial_list),1):  # Main loop
    timeleft = remainingTime()
    coh_to_chose_from = np.array(evidences['f1'])
    curr_coh = select_evidence(trial_list[i], coh_to_chose_from)
    
    if trial_list[i] == 0:
        valvetime = ValveLtime
        rewardValve = (OutputChannel.Valve, behavport_idx[0])
        resp = {PGLin:'Reward', PGRin:'Punish', EventName.Tup:'Invalid'}
        rewardLED = LEDL
    elif trial_list[i] == 1:
        valvetime = ValveRtime
        rewardValve = (OutputChannel.Valve, behavport_idx[2])
        resp = {PGRin:'Reward', PGLin:'Punish', EventName.Tup:'Invalid'}
        rewardLED = LEDR
    # time this,
    SL,SR, EL, ER = envelope(curr_coh, soundVec, 1, 20, samplingR=XonarSamplingR, variance=0.015, randomized=False, paired=False, LAmp=LAmp, RAmp=RAmp)

    # edited by DDD to add for silence trial (10% probability)
    sil_prob = np.random.random_integers(1, 10, 1)
    if sil_prob == 1:
        soundStream.load(SL*0, SR*0)
        my_bpod.register_value('coherence01', 0.5)
        my_bpod.register_value('silence_trial', 1)
        my_bpod.register_value('left_envelope', EL.tolist())
        my_bpod.register_value('right_envelope', ER.tolist())
    else:
        soundStream.load(SL, SR)
        my_bpod.register_value('coherence01', curr_coh)
        my_bpod.register_value('left_envelope', EL.tolist())
        my_bpod.register_value('right_envelope', ER.tolist())
    my_bpod.register_value('prob_repeat', prob_repeat[i])

    sma = StateMachine(my_bpod)
    sma.set_global_timer_legacy(timer_id=1, timer_duration=timeleft)
    
    sma.add_state(#WaitCPoke
        state_name='WaitCPoke',
        state_timer=timeleft,
        state_change_conditions={PGCin:'Fixation', Bpod.Events.GlobalTimer1_End: 'exit', EventName.Tup: 'exit'},
        output_actions=[LEDC,(Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(#Fixation
        state_name='Fixation',
        state_timer=0.3,
        state_change_conditions={PGCout:'WaitCPoke', EventName.Tup:'StartSound'},
        output_actions=[LEDC])
    sma.add_state(#StartSound
        state_name='StartSound',
        state_timer=1,
        state_change_conditions={PGCout:'WaitResponse', EventName.Tup:'WaitResponse'}, # because it seems to get stuck here sometimes
        output_actions=[(OutputChannel.SoftCode, 68)])#,(Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(
        state_name='WaitResponse',
        state_timer=8,
        state_change_conditions=resp,
        output_actions=[(OutputChannel.SoftCode, 66)])
    sma.add_state(#Reward
        state_name='Reward',
        state_timer=valvetime,
        state_change_conditions={EventName.Tup: 'keep-led-on'},
        output_actions=[rewardValve, rewardLED])
    sma.add_state(#keep-led-on
        state_name='keep-led-on',
        state_timer=0.3-valvetime,
        state_change_conditions= {EventName.Tup: 'exit'},
        output_actions=[rewardLED])
    sma.add_state(#Punish
        state_name='Punish',
        state_timer=2,
        state_change_conditions= {EventName.Tup: 'exit'},
        output_actions=[])
    sma.add_state(
        state_name='Invalid',
        state_timer=0.001,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[])

    try:
        my_bpod.send_state_machine(sma)
        my_bpod.run_state_machine(sma)
    except Exception as e:
        number_of_errors += 1
        if isinstance(e, IndexError) or number_of_errors >= 3:
            break
        time.sleep(10)
        my_bpod.register_value('CRASH_trial', i)
        my_bpod.register_value('CRASH_error', e)

    #my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device
    #print("Waiting for poke. Reward: ", 'left' if trial_list[i] == 0 else 'right')
    #my_bpod.run_state_machine(sma)  # Run state machine

    trialstring = ("Current trial info: {0}".format(my_bpod.session.current_trial))# remove all
    if "'Punish': [(nan, nan)]" in trialstring and "'Reward': [(nan, nan)]" in trialstring: # invalid trial. They are not taken into account to calc current accwindow.
        invalidcounter += 1
    else:
        validcounter += 1
    #remove 
    if int(conf.VAR_DATA)==1:
        tt = my_bpod.session.current_trial
        mydata.add(tt.export())

    if time.time()>sessionTimeEnding:
        break


# Since daily reports are working smoothly i'll remove this soon as well 
my_bpod.register_value('TOTAL_TRIALS', i) # remove
my_bpod.register_value('VALID_TRIALS', validcounter) #remove
my_bpod.register_value('INVALID_TRIALS', invalidcounter) # remove
my_bpod.close()  # Disconnect Bpod and perform post-run actions

cam.stop()
if int(conf.VAR_DATA) == 1:#remove
    mydata.save()

print('EXECUTION TIME', timeit.default_timer() - START_APP)

os.chdir(sessionDir)
#new reports:
main(os.path.join(conf.PYBPOD_SESSION_PATH, conf.PYBPOD_SESSION + ".csv"))
