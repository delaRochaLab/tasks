# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
required vars = [
    VAR_CAMIDX: int, camera index; 
    VAR_XONAR: int, soundcard index; 
    VAR_BOX: str, box name (paired with bpod), eg 'BPOD01'; 
    VAR_REC: int, 0=do not record; 1=record; 
    VAR_BLEN: int (even), block len;
    VAR_DATA: int, 0= do not save using rach util; 1= save data using R util,
    VAR_BNUM: int, block number]

Update_status var = []

# known issues:
    - keep-led-on will malfunction if valvetime > 0.3 s
    - set seed if random trials. 

# Check:
    - is trend plugin getting 2nd vector choice (when altering predefined trial_list because accwindow>85)
"""
task_version = '0.2'
task_name = 'p2'
stage_number = 2

from toolsR import VideoR
from toolsR import SoundR2
import user_settings as conf # to use vars: conf.VAR_0
from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import timeit, numpy as np, time, datetime
import os, csv
from scipy.signal import firwin, lfilter
import sounddevice as sd
from toolsJ import *
from modular_report.main import main

def water_calib(board, ports):
    """
    Obtain the water calibration values saved in the log. ONLY FOR BOXES > 6, OTHERWISE USE 'getWaterCalib' INSIDE TOOLSJ.
    If it's the first time using a box, a calibration has to be performed first.
    """
    # Location of the CSV calibration datafile that contains past calibration results:
    log_name = os.path.expanduser("~/pluginsr-for-pybpod/water-calibration-plugin/DATA/water_calibration.csv")
    with open(log_name, 'r') as log:
        calibration_data = csv.DictReader(log, delimiter=';')
        latest_row = None
        for row in calibration_data:
            if row['board'] == board:
                latest_row = dict(row)
        if latest_row is None:
            raise Exception("Water calibration data not found.")
        else:
            results = latest_row['pulse_duration'].split("-")
            return [float(results[i-1]) for i in ports]

sessionDir=os.getcwd()
def get_git_revision_hash(dirpath):
    os.chdir(os.path.expanduser(dirpath))
    return subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()

try:
    tasks_commit = get_git_revision_hash('~/projects/tasks/')
except:
    tasks_commit = 'coud not find git commit'

## check whether xonar idx is among possible ones. (particularly usefull for the one that's usually switching positions)
scheck = [x.lstrip().split(' Xonar DX')[0] for x in str(SoundR2.getDevices()).split('\n') if 'Multichannel' in x]
if conf.VAR_XONAR not in scheck:
    raise ValueError('wrong VAR_XONAR, could not find respective soundcard')
del scheck

#cam
camcheck = os.popen("ls /dev/video*").read().split('\n')
if ('/dev/video'+conf.VAR_CAMIDX) not in camcheck:
    raise ValueError('wrong cam idx')
del camcheck

########################### relevant variables for the task ##############################

# session-trials
nTrials = 2000
sessionTimeLength = 3000 # secs

switchedToRandom = False
accwindow = [0]*75 # len = 75 perhaps too much
invalidcounter = 0
validcounter = 0

sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart+sessionTimeLength

evidences = {
    'easy':(1, 0.866, -0.866, -1)
}
# stimuli (white-noise)
duration = 1 # stim len (secs). Does not imply playing whole stim each time
band_fs = [2000, 20000] # boundaries for the white noise (Hz)
XonarSamplingR = 192000 # soundcard sampling rate
filter_len = 10000 # filter len
# Next we assign sound vector. Returns np.array. How does this work? -> help(whiteNoiseGen)
soundVec = whiteNoiseGen(1.0, band_fs[0], band_fs[1], duration, FsOut=XonarSamplingR, Fn=filter_len) # using amplitude=1.0, adapt to calib values later.

########################## pre-start, load stuff #########################################

### Calibrations
try:
    ValveLtime, ValveCtime, ValveRtime = water_calib(conf.VAR_BOX, [1,2,3]) # retrieve water calibration
except:
    raise NameError('Could not find water calibration of '+str(conf.VAR_BOX)+'. Aborting.')
try:
    LAmp, RAmp = getBroadbandCalib(int(conf.VAR_XONAR)) # retrieve broadband calibration
except:
    raise NameError('Could not find broadband calibration of sounddevice '+str(conf.VAR_XONAR)+'. Aborting.')

### Video
currSessionVid = os.path.expanduser('~/VIDEO_pybpod/'+conf.VAR_BOX+'/')
currSessionName = 'test_'+datetime.datetime.now().strftime('%Y%m%d_%H%M')
if not os.path.exists(os.path.expanduser(currSessionVid)):
    os.makedirs(os.path.expanduser(currSessionVid))
cam = VideoR(indx_or_path=int(conf.VAR_CAMIDX), name_video=conf.PYBPOD_SESSION+'.avi', path=currSessionVid, title=conf.VAR_BOX, fps=30, codec_cam='X264', codec_video='X264') #width='default', height='default',#  fps=60)#, fourcc_mjpg='default', showWindows=True)
camOK = False                      
cam.play()
if int(conf.VAR_REC)>0:
        cam.record()

### Data
if int(conf.VAR_DATA) == 1:
    from toolsR import DataR
    currSessionPath = os.path.expanduser('~/DATA_pybpod/'+conf.VAR_BOX+'/') # VAR_BOX should look like 'BPOD01'
    if not os.path.exists(os.path.expanduser(currSessionPath)):
        os.makedirs(os.path.expanduser(currSessionPath))
    mydata = DataR(path=currSessionPath, name=currSessionName)

### misc
LEDINT = 2 # from 0 to 255
LEDL, LEDC, LEDR = (OutputChannel.PWM1,LEDINT), (OutputChannel.PWM2,LEDINT), (OutputChannel.PWM3,LEDINT)
LEDLerror, LEDCerror, LEDRerror = (OutputChannel.PWM1,250), (OutputChannel.PWM2,250), (OutputChannel.PWM3,250)

##### trial list #######  trialTypes = [0, 1] 0=(rewarded left) or  1=(rewarded right)
coin = str(np.random.choice(['heads', 'tails'])) # from which side should be the first block


## like previous stage, just that if it goes above 0.85 accuracy it switches to random
trial_list = []
starting_block_length = int(conf.VAR_BLEN) # 50 i guess is a good value to start with
for i in range(int(conf.VAR_BLEN)):
    blocklen= int(starting_block_length/2**i)
    if blocklen<10:
        break
    if coin == 'heads':
        trial_list += [0]*blocklen+[1]*blocklen
    else:
        trial_list += [1]*blocklen+[0]*blocklen

if coin=='heads':
    shortblock = [0]*10+[1]*10
else:
    shortblock = [1]*10+[0]*10

amount_missing = nTrials-len(trial_list)
pending_trials = shortblock*int(amount_missing/len(shortblock))
trial_list = trial_list + pending_trials


# Create the sound server 
soundStream = SoundR2(sampleRate=XonarSamplingR, deviceOut=int(conf.VAR_XONAR), channelsOut=2) #change device depending on the sound card

# Code to trig the sound
def my_softcode_handler(data):
    global start, timeit, soundStream
    print(data)
    if data == 68:
        print("Play")
        soundStream.playSound()
        start = timeit.default_timer()
    elif data == 66:
        soundStream.stopSound()
        print(':::::::::Time to stop:::::::::', timeit.default_timer() - start)
        
# we will define a function so it gets evaluated in each trial. will it? :)
def remainingTime():
    return sessionTimeEnding-time.time()

######################### BPOD and States ##################################
START_APP = timeit.default_timer()
my_bpod = Bpod()
my_bpod.register_value('STAGE_NUMBER', stage_number)
my_bpod.register_value('TASK', [task_name, task_version, tasks_commit])
my_bpod.register_value('REWARD_SIDE', trial_list) # required for trend plugin [0-left, 1-right]
my_bpod.register_value('left_amp', LAmp)
my_bpod.register_value('right_amp', RAmp)
my_bpod.register_value('left_valve', ValveLtime)
my_bpod.register_value('right_valve', ValveRtime)
my_bpod.softcode_handler_function = my_softcode_handler

for i in range(len(trial_list)):  # Main loop
    coh_to_chose_from = np.array(evidences['easy'])
    curr_coh =  select_evidence(trial_list[i], coh_to_chose_from)

    timeleft = remainingTime()

    if trial_list[i] == 0:
        valvetime = ValveLtime
        rewardValve = (OutputChannel.Valve, 1)
        pokechange = {EventName.Port1In:'Reward', EventName.Port3In:'Punish', EventName.Tup: 'Invalid'}
        rewardLED = LEDL
        punishLED = LEDRerror
    elif trial_list[i] == 1:
        valvetime = ValveRtime
        rewardValve = (OutputChannel.Valve, 3)
        pokechange = {EventName.Port3In:'Reward', EventName.Port1In:'Punish', EventName.Tup: 'Invalid'}
        rewardLED = LEDR
        punishLED = LEDLerror

    SL,SR, EL, ER = envelope(curr_coh, soundVec, 1, 20, samplingR=XonarSamplingR, variance=0.015, randomized=False, paired=False, LAmp=LAmp, RAmp=RAmp)
    
    soundStream.load(SL, SR)
    my_bpod.register_value('coherence01', curr_coh)
    my_bpod.register_value('left_envelope', EL.tolist())
    my_bpod.register_value('right_envelope', ER.tolist())

    sma = StateMachine(my_bpod)
    sma.set_global_timer_legacy(timer_id=1, timer_duration=timeleft)
    
    sma.add_state(#WaitCPoke
        state_name='WaitCPoke',
        state_timer=timeleft,
        state_change_conditions={Bpod.Events.Port2In:'StartSound', EventName.Tup: 'exit'},
        output_actions=[LEDC,(Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(#StartSound
        state_name='StartSound',
        state_timer=8,
        state_change_conditions=pokechange,
        output_actions=[(OutputChannel.SoftCode, 68)])
    sma.add_state(
        state_name='Reward',
        state_timer=valvetime,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[(OutputChannel.SoftCode, 66), rewardValve])
    sma.add_state(
        state_name='Punish',
        state_timer=0.3,
        state_change_conditions= {EventName.Tup: 'exit'},
        output_actions=[(OutputChannel.SoftCode, 66), punishLED])
    sma.add_state(
        state_name='Invalid',
        state_timer=0.001,
        state_change_conditions={EventName.Tup:'exit'},
        output_actions=[])

    my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device
    print("Waiting for poke. Reward: ", 'left' if trial_list[i] == 0 else 'right')
    my_bpod.run_state_machine(sma)  # Run state machine

    trialstring = ("Current trial info: {0}".format(my_bpod.session.current_trial))
    if "'Punish': [(nan, nan)]" in trialstring and "'Reward': [(nan, nan)]" in trialstring: # invalid trial. In this particular stage it could be if StartSound==[(nan, nan)]. Not in the next though
        invalidcounter += 1
    else:
        validcounter +=1
        if "'Punish': [(nan, nan)]" in trialstring:# Punish state was not triggered = rat poked in the correct port. This one is enough since it is binary
            accwindow = accwindow[1:]+[1]
        elif "'Reward': [(nan, nan)]" in trialstring:
            accwindow = accwindow[1:]+[0]
        
    if i>150 and sum(accwindow)/len(accwindow) > 0.85:
        if switchedToRandom == False: # havent reassigned trial_list yet
            prev_trials = trial_list[:i+1]
            pending_trials = np.random.choice([0,1], nTrials - len(prev_trials)).tolist()
            trial_list = prev_trials + pending_trials
            my_bpod.register_value('REWARD_SIDE', trial_list) # update trend list, will it work?
            switchedToRandom = True # so this piece of code do not run again
            # conf.VAR_BNUM = 0
    
    if int(conf.VAR_DATA)==1:
        tt = my_bpod.session.current_trial
        mydata.add(tt.export())
    if time.time()>sessionTimeEnding:
        break
my_bpod.close()  # Disconnect Bpod and perform post-run actions


cam.stop()
if int(conf.VAR_DATA) == 1:
    mydata.save()
print('EXECUTION TIME', timeit.default_timer() - START_APP)
os.chdir(sessionDir)
main(os.path.join(conf.PYBPOD_SESSION_PATH, conf.PYBPOD_SESSION + ".csv"))
