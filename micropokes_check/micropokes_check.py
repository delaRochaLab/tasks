"""
Check portin portout
"""

from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import user_settings as conf
from toolsR import UtilsR
import numpy as np

# TASK
task_version = '1.0'
task_name = 'micropokes_check'

# PORTS
try:
    behavport_idx = (np.where(np.array(conf.BPOD_BEHAVIOR_PORTS_ENABLED) == True)[0] + 1).tolist()
    LEDL, LEDC, LEDR = [getattr(OutputChannel, f"PWM{num}") for num in behavport_idx]
    WATERL, WATERC, WATERR = [(getattr(OutputChannel, "Valve"), num) for num in behavport_idx]
    PGLin, PGCin, PGRin = [getattr(EventName, f"Port{num}In") for num in behavport_idx]
    PGLout, PGCout, PGRout = [getattr(EventName, f"Port{num}Out") for num in behavport_idx]
except:
    UtilsR.stop('THIS PROTOCOL REQUIRES 3 AND ONLY 3 ACTIVE PORTS, ACTIVATE THEM MANUALLY')

led_intensity = 255  # from 0 to 255

# INIT BPOD
my_bpod = Bpod()


# CREATING STATE MACHINE, ADDING STATES, SENDING AND RUNNING
sma = StateMachine(my_bpod)
sma.set_global_timer_legacy(timer_id=1, timer_duration=200)
sma.set_global_timer_legacy(timer_id=2, timer_duration=5)

sma.add_state(
    state_name='start',
    state_timer=10,
    state_change_conditions={PGCin: 'center'},
    output_actions=[(LEDC, led_intensity)])

sma.add_state(
    state_name='center',
    state_timer=1,
    state_change_conditions={PGCout: 'start'},
    output_actions=[])



# sma.add_state(
#     state_name='WaitCPoke',
#     state_timer=1,
#     state_change_conditions={PGCin: 'TurnTimerOnOff', Bpod.Events.GlobalTimer1_End: 'exit'},
#     output_actions=[(LEDL, 100), (Bpod.OutputChannels.GlobalTimerTrig, 1)])
# sma.add_state(
#     state_name='TurnTimerOnOff',
#     state_timer=0.001,
#     state_change_conditions={EventName.Tup: 'Fixation'},
#     output_actions=[(LEDL, 100), (Bpod.OutputChannels.GlobalTimerTrig, 2)])
# sma.add_state(
#     state_name='Fixation',
#     state_timer=1,
#     state_change_conditions={PGCout: 'OutOff', Bpod.Events.GlobalTimer2_End: 'StartSound'},
#     output_actions=[(LEDC, 100)])
# sma.add_state(
#     state_name='OutOff',
#     state_timer=0.5,
#     state_change_conditions={PGCin: 'Fixation', EventName.Tup: 'WaitCPoke',
#                              Bpod.Events.GlobalTimer2_End: 'WaitCPoke'},
#     output_actions=[(LEDC, 100)])
# sma.add_state(
#     state_name='StartSound',
#     state_timer=1,
#     state_change_conditions={PGCout: 'WaitCPoke'},
#     output_actions=[(LEDR, 100)])


# (OutputChannel.GlobalTimerCancel, 2)


my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device
my_bpod.run_state_machine(sma)  # Run state machine

my_bpod.close()  # Disconnect Bpod and perform post-run actions
