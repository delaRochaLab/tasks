"""
required vars = [
    VAR_BOX: str, box name (paired with bpod), eg 'BPOD01';
    VAR_REC: int, 0=do not record; 1=record;
    VAR_DATA: int, 0= do not save using rach util; 1= save data using R util,

Update_status var = [
    #if setup==animal hack,
    update VAR_KSO at the end of the session  (+40 ms?)
]

# known issues:
    - todo: set seed if random trials.

 - Cube matrix pending, is it really worth?
"""

from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import user_settings as conf
from toolsR import SoundR, VideoR, UtilsR
import numpy as np
import os
import timeit
import time
import datetime
import csv
from scipy.signal import firwin, lfilter

# TASK
task_version = 'v0.3'
task_name = 'p3'
stage_number = 3
try:
    tasks_commit, md5sum = UtilsR.get_git_revision_hash('~/projects/tasks/', conf.PYBPOD_PROTOCOL)
except:
    tasks_commit, md5sum = ['coud not find git commit or md5checksum']*2
    

# PORTS
try:
    behavport_idx = (np.where(np.array(conf.BPOD_BEHAVIOR_PORTS_ENABLED) == True)[0] + 1).tolist()
    LEDL, LEDC, LEDR = [getattr(OutputChannel, f"PWM{num}") for num in behavport_idx]
    WATERL, WATERC, WATERR = [(getattr(OutputChannel, "Valve"), num) for num in behavport_idx]
    PGLin, PGCin, PGRin = [getattr(EventName, f"Port{num}In") for num in behavport_idx]
    PGLout, PGCout, PGRout = [getattr(EventName, f"Port{num}Out") for num in behavport_idx]
except:
    UtilsR.stop('THIS PROTOCOL REQUIRES 3 AND ONLY 3 ACTIVE PORTS, ACTIVATE THEM MANUALLY')


# GET WATER CALIBRATION VALUES
try:
    ValveLtime, ValveCtime, ValveRtime = UtilsR.getWaterCalib(conf.VAR_BOX, behavport_idx)
except:
    UtilsR.stop('COULD NOT FIND WATER CALIBRATION OF BOX: ' + str(conf.VAR_BOX))
if ValveLtime >= 0.3 or (ValveLtime / ValveRtime) > 2 or ValveRtime >= 0.3 or (ValveRtime / ValveLtime) > 2:
    continueTask = UtilsR.dropdownmenu('WATER CALIBRATION ERROR. L: ' + str(ValveLtime) + ', R: ' + str(ValveRtime),
                                       'Continue with these values?', ['Yes', 'No'], 'No')
    if continueTask == 'No':
        UtilsR.stop('')


# GET SOUND CALIBRATION VALUES (BROADBAND)
try:
    LAmp, RAmp = UtilsR.getBroadbandCalib(conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT FIND BROADBAND CALIBRATION OF SOUNDDEVICE: ' + str(conf.VAR_BOX))


# INITIALIZE SOUND STREAM
try:
    sampleRate = 192000
    soundStream = SoundR(sampleRate=sampleRate, deviceOut=conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT INITIALIZE SOUND STREAM, CHECK SOUNDCARD INDEX')


# INITIALIZE VIDEO, PLAY AND RECORD
try:
    currSessionVid = os.path.expanduser('~/VIDEO_pybpod/' + (conf.PYBPOD_SUBJECTS[0]).split("'")[1] + '/')
    if not os.path.exists(os.path.expanduser(currSessionVid)):
        os.makedirs(os.path.expanduser(currSessionVid))
    cam = VideoR(indx_or_path=conf.VAR_BOX, name_video=conf.PYBPOD_SESSION + '.avi', path=currSessionVid,
                 title=conf.VAR_BOX, fps=120, codec_cam='X264', codec_video='X264')
    cam.play()
    if int(conf.VAR_REC) > 0:
        cam.record()
except:
    UtilsR.stop('COULD NOT INITIALIZE VIDEO, CHECK VIDEO INDEX')



########################### relevant variables for the task ##############################

# session-trials
nTrials = 2000
sessionTimeLength = 3000  # secs
nSubstages = 10
# substageWindow = 5 # not used anymore

sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart + sessionTimeLength
trial_list = np.random.choice([0, 1], nTrials).tolist()  # random reward side

accwindow = [0] * 25
invalidcounter = 0
validcounter = 0

evidences = {}
KSO = {}
ez, mid, hard = np.array([1] * nSubstages), np.around(np.linspace(0.866, 0.5, num=nSubstages), 3), np.around(
    np.linspace(0.732, 0.25, num=nSubstages), 3)
for i in range(nSubstages):
    evidences[i] = (ez[i], mid[i], hard[i], -1 * hard[i], -1 * mid[i], -1 * ez[i])
    KSO[i] = np.around(np.linspace(400, 0, num=10), 1)[i]


# this become almost useless because now we are using integers as keys for the dicts
# then with a single int (substage = availableSubstages[currentSubstage]) we can call values for both dics (evidences['e'+str(availableSubstages[currentSubstage])]) and same with KSO but KSO['KSO'+...]
# keep things easy. by now use linearly distributed evidences and KSO
def decr_steps(start, stop, numbers, factor):
    step = np.linspace(0, start - stop, num=numbers)
    shift = np.linspace(factor, 1, num=numbers)
    return np.repeat(start, numbers) - step * shift


# changed strategy to match with evidences / dict
# KSO_list = list(np.linspace(350, 0, num=10))
### think about this a bit if <70 decr diff; if >75: incr diff (from 70 to 75 keep diff)
'''
e_thr = { # probably remove this:
    'e0': (0, .95),
    'e1': (.85, .90),
    'e2': (.80, .85),
    'e3': (.75, .80),
    'e4': (.70, .75)
}
'''
# stage3sessionNumb = 0 # conf.VAR_SESSION
startingSubstage = int(conf.VAR_SUBSTAGE)  ## cannot go below this (should be an int in the range [0,9])
availableSubstages_num = np.where(np.linspace(9, 0, num=10) == startingSubstage)[0][
    0]  # +1 not anymore # +1 because 0-indexed
availableSubstages_list = np.arange(startingSubstage, nSubstages, step=1).tolist()
# availableSubstages_list = ev_list[len(ev_list)-availableSubstages_num:]
currentSubstageIdx = 0  # start with 0 ## !!!! update vars or this wont work propperly

# by now ignore this stratgy
# we shift the available evidences according to accuracy and session number
# threshold_pos = 0 # means e0 = substage[0]
# threshold_pos = 5 means e0 = substage[5] 6,7,8, 9 # hardest
# todo: if reaachest ceiling, shift thresholds (substages). There's no way back

# KeepSoundOn = int(conf.VAR_KSO) #### wont use this anymore


# stimuli (white-noise)
duration = 1  # stim len (secs). Does not imply playing whole stim each time
band_fs = [2000, 20000]  # boundaries for the white noise (Hz)
filter_len = 10000  # filter len
# Next we assign sound vector. Returns np.array. How does this work? -> help(whiteNoiseGen)
soundVec = UtilsR.whiteNoiseGen(1.0, band_fs[0], band_fs[1], duration, FsOut=sampleRate,
                                Fn=filter_len)  # using amplitude=1.0, adapt to calib values later.


def my_softcode_handler(data):
    global start, timeit, soundStream, cam
    # print(data)
    if data == 68:
        # print("Play")
        soundStream.playSound()
        # start = timeit.default_timer()
    elif data == 66:
        soundStream.stopSound()
        # print(':::::::::Time to stop:::::::::', timeit.default_timer() - start)


def remainingTime():
    return sessionTimeEnding - time.time()


######################### BPOD and States ##################################
START_APP = timeit.default_timer()
my_bpod = Bpod()
my_bpod.register_value('TASK', [task_name, task_version, tasks_commit, md5sum])
my_bpod.register_value('STAGE_NUMBER', stage_number)
my_bpod.register_value('REWARD_SIDE', trial_list)  # required for trend plugin [0-left, 1-right]
my_bpod.register_value('left_amp', LAmp)
my_bpod.register_value('right_amp', RAmp)
my_bpod.register_value('left_valve', ValveLtime)
my_bpod.register_value('right_valve', ValveRtime)

my_bpod.softcode_handler_function = my_softcode_handler
# testing
ValveLtime = round(ValveLtime, 3)
ValveRtime = round(ValveRtime, 3)

for i in range(len(trial_list)):  # Main loop
    coh_to_chose_from = np.array(evidences[availableSubstages_list[currentSubstageIdx]])  # fixed evidences
    KeepSoundOn = KSO[availableSubstages_list[currentSubstageIdx]]
    curr_coh = UtilsR.select_evidence(trial_list[i], coh_to_chose_from)

    if trial_list[i] == 0:
        valvetime = ValveLtime
        rewardValve = WATERL
        pokechange = {PGLin: 'Reward', PGRin: 'Punish', EventName.Tup: 'WaitResponse'}
        resp = {PGLin: 'Reward', PGRin: 'Punish', EventName.Tup: 'Invalid'}
        rewardLED = (LEDL, 8)
        centralLED = (LEDC, 8)
    elif trial_list[i] == 1:
        valvetime = ValveRtime
        rewardValve = WATERR
        pokechange = {PGRin: 'Reward', PGLin: 'Punish', EventName.Tup: 'WaitResponse'}
        resp = {PGRin: 'Reward', PGLin: 'Punish', EventName.Tup: 'Invalid'}
        rewardLED = (LEDR, 8)
        centralLED = (LEDC, 8)
    # time this,
    SL, SR, EL, ER = UtilsR.envelope(curr_coh, soundVec, 1, 20, samplingR=sampleRate, variance=0.015, randomized=False,
                                     paired=False, LAmp=LAmp, RAmp=RAmp)
    soundStream.load(SL, SR)
    my_bpod.register_value('KeepSoundOn', KeepSoundOn)
    my_bpod.register_value('coherence01', curr_coh)
    my_bpod.register_value('left_envelope', EL.tolist())
    my_bpod.register_value('right_envelope', ER.tolist())

    sma = StateMachine(my_bpod)
    # sma.set_global_timer_legacy(timer_id=1, timer_duration=remainingTime())

    sma.add_state(  # WaitCPoke
        state_name='WaitCPoke',
        state_timer=remainingTime(),
        state_change_conditions={PGCin: 'Fixation', EventName.Tup: 'exit'},
        output_actions=[centralLED])  # ,(Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(  # Fixation
        state_name='Fixation',
        state_timer=0.3,
        state_change_conditions={PGCout: 'Invalid', EventName.Tup: 'StartSound'},
        output_actions=[centralLED])
    sma.add_state(  # StartSound
        state_name='StartSound',
        state_timer=0.1,
        state_change_conditions={PGCout: 'KeepSoundOn'},
        output_actions=[(OutputChannel.SoftCode, 68), (OutputChannel.BNC1, 3)])  # ,(Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(
        state_name='KeepSoundOn',
        # = time that it takes to shut the sound of since the animal pokes out of the central port
        state_timer=round((KeepSoundOn / 1000) + 0.001, 3),
        state_change_conditions=pokechange,
        output_actions=[])
    sma.add_state(
        state_name='WaitResponse',
        state_timer=round(8 - (KeepSoundOn / 1000) - 0.001, 3),
        state_change_conditions=resp,
        output_actions=[(OutputChannel.SoftCode, 66)])
    sma.add_state(  # Reward
        state_name='Reward',
        state_timer=valvetime,
        state_change_conditions={EventName.Tup: 'keep-led-on'},
        output_actions=[(OutputChannel.SoftCode, 66), (OutputChannel.BNC1, 0), rewardValve, rewardLED])
    sma.add_state(  # keep-led-on
        state_name='keep-led-on',
        state_timer=0.3 - valvetime,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[rewardLED])
    sma.add_state(  # Punish
        state_name='Punish',
        state_timer=2,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[(OutputChannel.SoftCode, 66)])
    sma.add_state(
        state_name='Invalid',
        state_timer=0.001,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[])

    my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device
    my_bpod.run_state_machine(sma)  # Run state machine

    trialstring = ("Current trial info: {0}".format(my_bpod.session.current_trial))
    if "'Punish': [(nan, nan)]" in trialstring and "'Reward': [(nan, nan)]" in trialstring:  # invalid trial. They are not taken into account to calc current accwindow.
        invalidcounter += 1
    else:
        validcounter += 1
        if "'Punish': [(nan, nan)]" in trialstring:  # Punish state was not triggered = rat poked in the correct port. This one is enough since it is binary, modify when introdicing invalid trials
            accwindow = accwindow[1:] + [1]
        elif "'Reward': [(nan, nan)]" in trialstring:
            accwindow = accwindow[1:] + [0]

    if validcounter % 5 == 0:  # too high
        if sum(accwindow) / len(accwindow) > 0.78:  # increase diff
            if availableSubstages_list[currentSubstageIdx] == 9:  # already top dif
                if len(availableSubstages_list) > 1:
                    availableSubstages_list = availableSubstages_list[1:]
                    # need to decr index because list is shorter and we already reached last item
                    currentSubstageIdx -= 1
            else:

                if len(availableSubstages_list) > 5:
                    availableSubstages_list = availableSubstages_list[
                                              1:]  # as we shorten the list by left side, there's no need to increase the index
                else:
                    currentSubstageIdx += 1
        elif sum(accwindow) / len(accwindow) < 0.72:  # decrease diff,
            if currentSubstageIdx > 0:
                currentSubstageIdx -= 1
            # no need to do else, because if idx=0 we cannot decrease the diff more

    if time.time() > sessionTimeEnding:
        break

## execute before closing bpod // update var required
if availableSubstages_list[currentSubstageIdx] >= 2:
    if len(availableSubstages_list) > 2:
        if currentSubstageIdx > 1:
            conf.VAR_SUBSTAGE = str(availableSubstages_list[currentSubstageIdx] - 2)
        else:
            conf.VAR_SUBSTAGE = str(availableSubstages_list[currentSubstageIdx])
    else:
        conf.VAR_SUBSTAGE = str(availableSubstages_list[currentSubstageIdx])
else:
    conf.VAR_SUBSTAGE = str(availableSubstages_list[currentSubstageIdx])  # next session rat will start with this one

my_bpod.register_value('TOTAL_TRIALS', i)
my_bpod.register_value('VALID_TRIALS', validcounter)
my_bpod.register_value('INVALID_TRIALS', invalidcounter)

my_bpod.close()  # Disconnect Bpod and perform post-run actions
cam.stop()

print('EXECUTION TIME', timeit.default_timer() - START_APP)

# new reports:
os.system('datahandler -l ' + conf.PYBPOD_SESSION[:-7])
