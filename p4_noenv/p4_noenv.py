"""
required vars (all strings, transformed whithin the task script)= [
    VAR_BOX: str, box name (paired with bpod), eg 'BPOD01';
    VAR_REC: int, 0=do not record; 1=record;
    VAR_BLEN: int (even), block len;
    VAR_DATA: int, 0= do not save using rach util; 1= save data using R util,
    VAR_BNUM: int, block number]
    VAR_SSIDE: 'r'-random, '0'-left, '1'-right
    VAR_SBLOCK: 'r'-random, True - repetitive, 'False'- alternating
    VAR_VARIATION: 'standard', 'no_stimulus', 'high_volatility', 'low_volatility_alt', 'low_volatility_rep' just logging purposes
Update_status var = []
"""

from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel

import user_settings as conf
from toolsR import VideoR, UtilsR
import numpy as np
import os
import timeit
import time
import datetime
import csv
from scipy.signal import firwin, lfilter
from datahandler.custom_jordi import report_noenv


# generate a seed according to date in format YYYYmmdd + box
seed = int(conf.VAR_BOX[-2:])+int(datetime.datetime.now().strftime("%Y%m%d"))
# get a randomgenerator from that seed
rg = np.random.RandomState(seed=seed)

# TASK
task_version = '0.7'  # default electro = 1 if in delarocha7
task_name = 'p4_noenv'
stage_number = 4
try:
    tasks_commit, md5sum = UtilsR.get_git_revision_hash('~/projects/tasks/', conf.PYBPOD_PROTOCOL)
except:
    tasks_commit, md5sum = ['coud not find git commit or md5checksum']*2

# PORTS
try:
    behavport_idx = (np.where(np.array(conf.BPOD_BEHAVIOR_PORTS_ENABLED) == True)[0] + 1).tolist()
    LEDL, LEDC, LEDR = [getattr(OutputChannel, f"PWM{num}") for num in behavport_idx]
    WATERL, WATERC, WATERR = [(getattr(OutputChannel, "Valve"), num) for num in behavport_idx]
    PGLin, PGCin, PGRin = [getattr(EventName, f"Port{num}In") for num in behavport_idx]
    PGLout, PGCout, PGRout = [getattr(EventName, f"Port{num}Out") for num in behavport_idx]
except:
    UtilsR.stop('THIS PROTOCOL REQUIRES 3 AND ONLY 3 ACTIVE PORTS, ACTIVATE THEM MANUALLY')

# here add 2nd check: both BNC enabled
if conf.BPOD_BNC_PORTS_ENABLED.count(True)!=2: # should work with other bpod versions, right?
    UtilsR.stop('THIS PROTOCOL REQUIRES 2 BNC PORTS, ACTIVATE THEM MANUALLY')

# GET WATER CALIBRATION VALUES
try:
    ValveLtime, ValveCtime, ValveRtime = UtilsR.getWaterCalib(conf.VAR_BOX, behavport_idx)
except:
    UtilsR.stop('COULD NOT FIND WATER CALIBRATION OF BOX: ' + str(conf.VAR_BOX))
if ValveLtime >= 0.3 or (ValveLtime / ValveRtime) > 2 or ValveRtime >= 0.3 or (ValveRtime / ValveLtime) > 2:
    continueTask = UtilsR.dropdownmenu('WATER CALIBRATION ERROR. L: ' + str(ValveLtime) + ', R: ' + str(ValveRtime),
                                       'Continue with these values?', ['Yes', 'No'], 'No')
    if continueTask == 'No':
        UtilsR.stop('')

# GET SOUND CALIBRATION VALUES (BROADBAND)
try:
    LAmp, RAmp = UtilsR.getBroadbandCalib(conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT FIND BROADBAND CALIBRATION OF SOUNDDEVICE: ' + str(conf.VAR_BOX))


default_electro = 0
if os.path.expanduser('~')=="/home/delarocha7":
    default_electro = 1

# ask few optional stuff
opts_dict = UtilsR.checkbuttonmenu(
    {'enhance_com':0, # upon certain Zt it increases prob to show unexpected stim
    'electro':default_electro, # bncs for electro task
    'late_stim': 0, # 10% sound is played proportional to time inside central port but with somedelay centered at 65ms
    'silent_catch':default_electro, # 5% silent catch trials
    'oldSoundR':default_electro, # old sound server (higher latency)
    'rep_block7_3': 1, # use prep .7 in repeating block instead of .8
    'alt_block9_1': 0
    }, 
    label='choose task options')

if opts_dict['oldSoundR']:
    from toolsR import SoundROld as SoundR
    print('using old sound server')
else:
    from toolsR import SoundR
    print('using new sound server')

# INITIALIZE SOUND STREAM
try:
    sampleRate = 192000
    soundStream = SoundR(sampleRate=sampleRate, deviceOut=conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT INITIALIZE SOUND STREAM, CHECK SOUNDCARD INDEX')



BNC1_ON, BNC1_OFF, BNC2_ON, BNC2_OFF = [], [], [], []
if opts_dict['electro']:
    BNC1_OFF = [(OutputChannel.BNC1, 0)]
    BNC1_ON = [(OutputChannel.BNC1, 3)]
    BNC2_OFF = [(OutputChannel.BNC2, 0)]
    BNC2_ON = [(OutputChannel.BNC2, 3)]

# INITIALIZE VIDEO, PLAY AND RECORD
try:
    currSessionVid = os.path.expanduser('~/VIDEO_pybpod/' + (conf.PYBPOD_SUBJECTS[0]).split("'")[1] + '/')
    if not os.path.exists(os.path.expanduser(currSessionVid)):
        os.makedirs(os.path.expanduser(currSessionVid))
    cam = VideoR(indx_or_path=conf.VAR_BOX, name_video=conf.PYBPOD_SESSION + '.avi', path=currSessionVid,
                 title=conf.VAR_BOX, fps=120, codec_cam='X264', codec_video='X264')
    cam.play()
    if int(conf.VAR_REC) > 0:
        cam.record()
except:
    UtilsR.stop('COULD NOT INITIALIZE VIDEO, CHECK VIDEO INDEX')

########################### relevant variables for the task ##############################

#### Commenting this out because vars in gui are rarely used and this
#### sort of options are way more likely to introduce unintended changes
#### Remove at some point
# ### complete Trial list
# if conf.VAR_SSIDE == 'r':
#     startingSide = int(rg.choice([0, 1]))  # which side to start
# else:
#     startingSide = int(conf.VAR_SSIDE)  # needs to be either 0 (left) or 1(right)

# if conf.VAR_SBLOCK == 'r':
#     startingBlock = bool(rg.choice([True, False]))  # type of block
# else:
#     startingBlock = bool(int(conf.VAR_SBLOCK))

# trial_list, prob_repeat = UtilsR.block(startingSide, repetitive=startingBlock, bsize=int(conf.VAR_BLEN),
#                                        bnum=int(conf.VAR_BNUM), randgen=rg)
# trial_list = list(trial_list)
startingSide = int(rg.choice([0, 1]))  # which side to start
startingBlock = bool(rg.choice([True, False]))  # type of block

# transition matrix:
if opts_dict['rep_block7_3']:
    rep_bl_prob = [0.7,0.7]
else:
    rep_bl_prob = [0.8,0.8]

if opts_dict['alt_block9_1']:
    alt_bl_prob = [0.1,0.1]
else:
    alt_bl_prob = [0.2,0.2]

tm = [rep_bl_prob, alt_bl_prob]

trial_list, prob_repeat = UtilsR.newBlock(tm, ss=startingSide, sblock=startingBlock*1, bnum=26, blen=80, randgen=rg)
trial_list = list(trial_list) # idk, just in case
curr_coh=np.array([0])[0]

# if opts_dict['enhance_com']:  ## prepare vars / functions
#     # this requires to keep track about trials
#     #hist = [] # history, being 1 hit, 0 miss, nan invalid
#     # why is list better than simply streak?
#     # init vars
#     coh = 0
#     hitstreak = 0
#     #UtilsR.stop(message='enhance com is not implemented yet')
#     def decision_function_hitstreak(curr_rng=None, hitstreak=0, rewside=trial_list, current_index=0):
#         if curr_rng is None:
#             curr_rng=np.random

#         prev_rewards = trial_list[current_index-hitstreak:current_index] # current trial (i) not included
#         transitions = np.diff(prev_rewards)**2 # 0 = rep, 1, = alt
#         # find (inverted) sequence of same transition
#         targ_transition = transitions[-1]
#         non_targ = np.where(transitions[::-1]==(targ_transition-1)**2)[0]
#         if not non_targ.size: # all prev transitions in hitstreak are the same
#             trans_streak = transitions.size
#         else:
#             trans_streak = non_targ[0][0]
#         # add some extra jitter in onset # intrinsic to probabilistic aproach / so it is not a constant
#         p_switch = max(
#             min(1, 0.02 * 2**trans_streak + curr_rng.normal(scale=0.01*(trans_streak+0.5))),
#             0)
#         booltoswitch = bool(curr_rng.choice(
#             [0,1],
#             p=[1-p_switch, p_switch]
#         ))
#         if not booltoswitch:
#             return False, 0.5 # should do nothing ~ do not alter rewside_sequence
#         else:
#             next_transition = (targ_transition-1)**2 # 1 alt, 0 rep # changes prev transition to increase p(com)
#             if next_transition: # alt
#                 trial_list[current_index] = (trial_list[current_index-1]-1)**2
#             else: # repeat
#                 trial_list[current_index] = trial_list[current_index-1]

#             next_trial_coh = curr_rng.choice([0.5,1.])
#             if not trial_list[current_index]:
#                 next_trial_coh *= -1 # left trial, neg coh
#             # return coh in 0~1 space
#             return booltoswitch, (next_trial_coh+1)/2

if opts_dict['enhance_com']:  ## prepare vars / functions
    # rewrite whole function now assuming that get_zt is working (we get zt from recent history)
    def decision_function_zt(): # try avoiding hard thresholds
        global trial_list, i, zt_repalt, zt_LR, rg, my_bpod # there's no need to init vars as long as they exist when function is called
        # fixed huge bug:
        # now using abs zt to get p to switch,
        # now using corresponding zt (LR) to decide whether to alter trial or not
        # not modifying next trial coh if there's no side switch    
        p_switch = min(max(
            abs(zt_repalt)-0.65,
            0
        ), 1)
        booltoswitch = rg.choice(
            [False, True],
            p = [1-p_switch, p_switch]
        )
        if not booltoswitch:
            return False, 0.5
        else: #~ need to ensure next/current trial breaks transition pattern
            if zt_LR<0: # expecting left
                if trial_list[i]==0: # current trial is left, we need to change it
                    trial_list[i]=1
                    # register here!
                    my_bpod.register_value('enhance_com_switch', i) 
                    next_trial_coh = rg.choice(np.array([0.7,1.])) # coherence in 0-1space
                    return True, next_trial_coh
                else:
                    return False, 0.5
            else: # expecting right
                if trial_list[i]:
                    trial_list[i]=0
                    my_bpod.register_value('enhance_com_switch', i) 
                    next_trial_coh = rg.choice(np.array([0., 0.3]))
                    return True, next_trial_coh
                else:
                    return False, 0.5
else:
    switchnext = False



### old strategy
#     fixationchange = {PGCout: 'WaitCPoke', EventName.Tup: 'Delay'}
# else:
#     fixationchange = {PGCout: 'WaitCPoke', EventName.Tup: 'StartSound'}
if opts_dict['electro']:
    sessionTimeLength = 3600
else:
    sessionTimeLength = 3600 
sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart + sessionTimeLength

evidences = {
    'f1': (1, 0.4, 0.2, 0, -0.2, -0.4, -1)
}

# stimuli (white-noise)
duration = 1  # stim len (secs). Does not imply playing whole stim each time
band_fs = [2000, 20000]  # boundaries for the white noise (Hz)
filter_len = 10000  # filter len

# Next we assign sound vector. Returns np.array. How does this work? -> help(whiteNoiseGen)
soundVec = UtilsR.whiteNoiseGen(1.0, band_fs[0], band_fs[1], duration, FsOut=sampleRate,
                                Fn=filter_len, randgen=rg)  # using amplitude=1.0, adapt to calib values later.

# adding some ramp
ramp = -np.sin(2 * np.pi * 20 * np.arange(0,1/80, step=1/192000)+np.pi)
soundVec[:2400] = soundVec[:2400] * ramp

def my_softcode_handler(data):
    global soundStream
    if data == 68:
        soundStream.playSound()
    elif data == 66:
        soundStream.stopSound()

if opts_dict['late_stim']:
    delay_len = 0.050 # placeholder
    def delayed_softcode_handler(data):
        global soundStream, delay_len
        time.sleep(delay_len) # not precise
        # timerends = datetime.datetime.now() + datetime.timedelta(seconds=delay_len)
        # while timerends > datetime.datetime.now(): # hopefully it will block just 1 parallel thread
        #     pass
        if data == 68:
            soundStream.playSound()
        elif data == 66:
            soundStream.stopSound()

def remainingTime():
    return sessionTimeEnding - time.time()

######################### BPOD and States ##################################
START_APP = timeit.default_timer()
my_bpod = Bpod()

# my_bpod.register_value('TASK', [task_name, task_version, tasks_commit])
my_bpod.register_value('TASK', [task_name, task_version, tasks_commit, md5sum])
my_bpod.register_value('STAGE_NUMBER', stage_number)
my_bpod.register_value('SEED', seed)

#opts
log_opts = []
for k in opts_dict.keys():
    if opts_dict[k]:
        log_opts += [k]

my_bpod.register_value('opts_dict', sorted(log_opts))

try:
    if opts_dict['late_stim']:
        my_bpod.register_value('Variation', 'late_stim')
    else:
        my_bpod.register_value('Variation', 'none')
except:
    my_bpod.register_value('Variation', 'no variation of the task stated')
try:
    if opts_dict['electro']:
        my_bpod.register_value('Postop', 'electro')
    else:
        my_bpod.register_value('Postop', 'none') 
except:
    my_bpod.register_value('Postop', 'no postop stated')

my_bpod.register_value('REWARD_SIDE', trial_list)  # required for trend plugin [0-left, 1-right]
my_bpod.register_value('left_amp', LAmp)
my_bpod.register_value('right_amp', RAmp)
my_bpod.register_value('left_valve', ValveLtime)
my_bpod.register_value('right_valve', ValveRtime)

# my_bpod.softcode_handler_function = my_softcode_handler

# init other vars
zt_repalt = 0
hit = 0


for i in range(len(trial_list)):  # Main loop
    timeleft = remainingTime()
    if rg.choice([True, False], p=[.05, .95]) & opts_dict['silent_catch']: # bool silent catch * bool outcomw p(silent)
        my_bpod.register_value('silence_trial', i)
        SL = np.zeros(soundVec.shape)
        SR = np.zeros(soundVec.shape)
        my_bpod.softcode_handler_function = my_softcode_handler
        late_stim = False
        switchnext = False

    else:
        if opts_dict['enhance_com']: 
            #if hithistory[-1]:
            if hit:
                # zt = UtilsR.get_zt(hithistory, rewside=trial_list, current_index=i, gating_ae=0.15)
                # switchnext, nextcoh = decision_function_hitstreak(curr_rng=rg, hitstreak=hitstreak, rewside=trial_list, current_index=i)
                switchnext, nextcoh = decision_function_zt()
            else:
                switchnext=False

        if opts_dict['late_stim']:
            # if switchnext: # if also late_stim, we could increase its chance to 33%
            #     late_stim = bool(rg.choice([0,1], p=[.9, .1]))   
            late_stim = rg.choice([False,True], p=[.9, .1])
        else:
            my_bpod.softcode_handler_function = my_softcode_handler
            late_stim = False

        if switchnext:
            curr_coh = nextcoh
        else:
            coh_to_chose_from = np.array(evidences['f1'])
            curr_coh = UtilsR.select_evidence(trial_list[i], coh_to_chose_from, randgen=rg)

        SL = soundVec * (1-curr_coh) * LAmp
        SR = soundVec * curr_coh * RAmp

    if trial_list[i] == 0:
        valvetime = ValveLtime
        rewardValve = WATERL
        resp = {PGLin: 'Reward', PGRin: 'Punish', EventName.Tup: 'Invalid'}
        rewardLED = (LEDL, 8)
        centralLED = (LEDC, 8)
    elif trial_list[i] == 1:
        valvetime = ValveRtime
        rewardValve = WATERR
        resp = {PGRin: 'Reward', PGLin: 'Punish', EventName.Tup: 'Invalid'}
        rewardLED = (LEDR, 8)
        centralLED = (LEDC, 8)


    

    soundStream.load(SL, SR)
    my_bpod.register_value('coherence01', curr_coh)
    my_bpod.register_value('zt_repalt', zt_repalt)
    #my_bpod.register_value('left_envelope', EL.tolist())
    #my_bpod.register_value('right_envelope', ER.tolist())
    
    # registering somewhere else: just when trial_list is edited!
    # if switchnext:
    #     my_bpod.register_value('enhance_com_switch', i) # 0 based index

    # Jaime's delay sound strategy implementation
    # regular_outputs = [
    #     [(OutputChannel.SoftCode, 68)], # triggers sound
    #     [(OutputChannel.SoftCode, 66)], # stops sound
    #     [], # empty addition
    # ]    
    # if late_stim: # this would be 1 strategy (Jaime)
    #     my_bpod.register_value('delay_trial', i) # 0 based index # following dani's nomenclature
        # basically delay sound onset 1 state # remember this crap afterwards for the analysis will wreck pipe in lots of sections
        # alter regular outputs, so they are shifted 1 state
        # regular_outputs = [
        #     [], # empty
        #     [(OutputChannel.SoftCode, 68)], # triggers sound
        #     [(OutputChannel.SoftCode, 66)] # stops sound
        # ]
    # my proposal, dani-like: add a constant state which lasts either 1ms or random centered at 65ms
    if late_stim:
        # my_bpod.register_value('delay_trial', 1) # legacy / Dani
        delay_len = round(rg.normal(loc=0.065, scale=0.005),4) #loc=0.065, scale=0.005), 4)
        my_bpod.softcode_handler_function = delayed_softcode_handler
        my_bpod.register_value('delayed_trial', i) # iterating var
        my_bpod.register_value('expected_delay', delay_len)
    else:
        #delay_len = 0.001
        my_bpod.softcode_handler_function = my_softcode_handler
    
    my_bpod.register_value('prob_repeat', prob_repeat[i])

    sma = StateMachine(my_bpod)
    sma.set_global_timer_legacy(timer_id=1, timer_duration=timeleft)

    sma.add_state(  # WaitCPoke
        state_name='WaitCPoke',
        state_timer=timeleft,
        state_change_conditions={PGCin: 'Fixation_fb', Bpod.Events.GlobalTimer1_End: 'exit', EventName.Tup: 'exit'},
        output_actions=[centralLED, (Bpod.OutputChannels.GlobalTimerTrig, 1)] + BNC1_OFF + BNC2_OFF)
    sma.add_state(
        state_name='Fixation_fb',
        state_timer=0.08,
        state_change_conditions={PGCout: 'WaitCPoke', EventName.Tup: 'Fixation'},
        output_actions=BNC1_ON)
    sma.add_state(  # Fixation
        state_name='Fixation',
        state_timer=0.22,
        state_change_conditions={PGCout: 'WaitCPoke', EventName.Tup: 'StartSound'}, # change: Tup: 'Delay' # fixationchange, #
        output_actions=[centralLED]+BNC1_OFF)
    # if opts_dict['late_stim']:
    #     sma.add_state(
    #         state_name='Delay',
    #         state_timer=delay_len,
    #         state_change_conditions={PGCout: 'WaitResponse' ,EventName.Tup: 'StartSound'}, # what if pokes out?
    #         output_actions=[])
    sma.add_state(  # StartSound
        state_name='StartSound',
        state_timer=1,
        state_change_conditions={PGCout: 'WaitResponse', EventName.Tup: 'WaitResponse'},
        # because it seems to get stuck here sometimes
        output_actions=[(OutputChannel.SoftCode, 68)] + BNC2_ON)  # ,(Bpod.OutputChannels.GlobalTimerTrig, 1)]) # regular_outputs[0]
    sma.add_state(
        state_name='WaitResponse',
        state_timer=8,
        state_change_conditions=resp,
        output_actions= [(OutputChannel.SoftCode, 66)] + [(LEDL, 8), (LEDR, 8)]+ BNC2_OFF)  # regular_outputs[1]
    sma.add_state(  # Reward
        state_name='Reward',
        state_timer=valvetime,
        state_change_conditions={EventName.Tup: 'keep-led-on'},
        output_actions=[rewardValve, rewardLED]+ BNC1_ON + BNC2_ON) # regular_outputs[2]
    sma.add_state(  # keep-led-on
        state_name='keep-led-on',
        state_timer=0.3 - valvetime,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[rewardLED]+ BNC1_ON + BNC2_ON)
    sma.add_state(  # Punish
        state_name='Punish',
        state_timer=2,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[]+ BNC1_ON + BNC2_ON) # regular_outputs[2]
    sma.add_state(
        state_name='Invalid',
        state_timer=0.100,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[]+ BNC1_ON + BNC2_ON) # regular_outputs[2]

    my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device
    my_bpod.run_state_machine(sma)  # Run state machine

    if time.time() > sessionTimeEnding:
        my_bpod.register_value('clean_exit', 1)
        break
    #if opts_dict['enhance_com']: # track it regardless ehance_com active or not
    # there is no reversal here so there is no issue
    trialstring = ("Current trial info: {0}".format(my_bpod.session.current_trial))  # remove all
    hit_1 = hit
    if "'Reward': [(nan, nan)]" not in trialstring: # # (and or ORRRRRRR) ("'invReward': [(nan, nan)]" not in trialstring): # not in this task
        #hithistory = hithistory[1:]+[1]
        hit = 1
    else:
        # hithistory = hithistory[1:]+[0]
        hit = 0
    if i:
        zt_repalt, zt_LR = UtilsR.get_zt_light(zt_repalt, hit, hit_1, trial_list[i], trial_list[i-1])
    #

my_bpod.close()  # Disconnect Bpod and perform post-run actions
cam.stop()

# new reports:
# os.system('datahandler -l ' + conf.PYBPOD_SESSION[:-7])

# independently from other reports, try to send new reports through slack
npypath = currSessionVid+conf.PYBPOD_SESSION+'.npy'

if not os.path.exists(npypath): # assumes it is electrobox or something went wrong with videotimestamps, then call the function without video info
    npypath=None # need to adapt report_noenv to handle nones fine

pdfpath = report_noenv(
    os.path.join(conf.PYBPOD_SESSION_PATH, conf.PYBPOD_SESSION)+'.csv', 
    npypath, 
    os.path.expanduser('~/daily_reports')+'/'+conf.PYBPOD_SUBJECTS[0].split("'")[1]+f'/c{conf.PYBPOD_SESSION}.pdf')

if 'VAR_SLACK' in conf.PYBPOD_VARSNAMES:
    UtilsR.slack_spam(f'{conf.PYBPOD_SESSION} report', pdfpath, conf.VAR_SLACK)
