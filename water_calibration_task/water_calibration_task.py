# !/usr/bin/python3
# -*- coding: utf-8 -*-

"""
A protocol to calibrate the water system. In addition, to contro the lights.
"""

import user_settings as conf
from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import timeit

# Exacution time
start = 0
START_APP = timeit.default_timer()

"""
The plugin loads these variables:
* VAR_ITER contains the number of cycles.
* VAR_INTERVAL contains the interval between one trial and the next one.
* VAR_DUR contains the pulse duration for each of the valves.
"""

# Check which ports are active:
active_ports = conf.BPOD_BEHAVIOR_PORTS_ENABLED # i.e [True, False, True, False...] up to eight values
active_ports_idx = [idx + 1 for idx, port in enumerate(active_ports) if port] # indices of the enabled ports , starting at 1
number_active_ports = sum(active_ports)

pulse_duration = [float(t) for t in conf.VAR_DUR.split('-')]
iterations = int(conf.VAR_ITER)
interval = float(conf.VAR_INTERVAL)

# ----> Start the task
my_bpod = Bpod()
for i in range(iterations):  # Main loop
    print('Trial: ', i + 1)

    sma = StateMachine(my_bpod)
    changeTostate = ''

    # Prepare the state machine. We will make as many states as active ports,
    # with a final 'End' state to exit the task.
    for (jj, port) in enumerate(active_ports_idx, 1):
        if jj != number_active_ports:
            changeTostate = 'GetWater_P' + str(active_ports_idx[jj])
        else:
            changeTostate = 'End'

        sma.add_state(
            state_name  ='GetWater_P' + str(port),
            state_timer = pulse_duration[port-1],
            state_change_conditions = {EventName.Tup: changeTostate},
            output_actions = [(OutputChannel.Valve, port), (OutputChannel.LED, port)])

    sma.add_state(
        state_name = 'End',
        state_timer = float(interval),
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[])

    my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device
    my_bpod.run_state_machine(sma)  # Run state machine

my_bpod.close()
print('EXECUTION TIME: ', timeit.default_timer() - START_APP)


