"""
Example adapted from Josh Sanders' original version on Sanworks Bpod repository
"""
from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import user_settings as conf
from toolsR import SoundR, VideoR, UtilsR
import numpy as np
import os
import timeit
import time
import datetime
import csv
from scipy.signal import firwin, lfilter


# PORTS
try:
    behavport_idx = (np.where(np.array(conf.BPOD_BEHAVIOR_PORTS_ENABLED) == True)[0] + 1).tolist()
    LEDL, LEDC, LEDR = [getattr(OutputChannel, f"PWM{num}") for num in behavport_idx]
    WATERL, WATERC, WATERR = [(getattr(OutputChannel, "Valve"), num) for num in behavport_idx]
    PGLin, PGCin, PGRin = [getattr(EventName, f"Port{num}In") for num in behavport_idx]
    PGLout, PGCout, PGRout = [getattr(EventName, f"Port{num}Out") for num in behavport_idx]
except:
    UtilsR.stop('THIS PROTOCOL REQUIRES 3 AND ONLY 3 ACTIVE PORTS, ACTIVATE THEM MANUALLY')


LEDL, LEDC, LEDR = (LEDL, 25), (LEDC, 25), (LEDR, 25)
waitingPoke_change_cond = {PGLin: 'Flush1', PGCin: 'Flush2', PGRin: 'Flush3', EventName.Tup: 'exit'}

timeToFlush = 60

my_bpod = Bpod()
sma = StateMachine(my_bpod)

sma.add_state(
    state_name='waitingPoke',
    state_timer=30,
    state_change_conditions=waitingPoke_change_cond,
    output_actions=[LEDL, LEDC, LEDR])
sma.add_state(
    state_name='Flush1',
    state_timer=timeToFlush,
    state_change_conditions={PGLout: 'waitingPoke', EventName.Tup: 'exit'},
    output_actions= [LEDL, WATERL])
sma.add_state(
    state_name='Flush2',
    state_timer=timeToFlush,
    state_change_conditions={PGCout: 'waitingPoke', EventName.Tup: 'exit'},
    output_actions= [LEDC, WATERC])
sma.add_state(
    state_name='Flush3',
    state_timer=timeToFlush,
    state_change_conditions={PGRout: 'waitingPoke', EventName.Tup: 'exit'},
    output_actions= [LEDR, WATERR])

my_bpod.send_state_machine(sma)
my_bpod.run_state_machine(sma)

print("Current trial info: {0}".format(my_bpod.session.current_trial))
my_bpod.close()