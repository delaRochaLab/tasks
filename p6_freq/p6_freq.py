from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import user_settings as conf
from toolsR import SoundR, VideoR, UtilsR
import numpy as np
import os
import time


# TASK
task_version = '0.1'
stage_number = 1
task_name = 'p6_freq'
try:
    tasks_commit, md5sum = UtilsR.get_git_revision_hash('~/projects/tasks/', conf.PYBPOD_PROTOCOL)
except:
    tasks_commit, md5sum = ['coud not find git commit or md5checksum']*2


# PORTS
try:
    behavport_idx = (np.where(np.array(conf.BPOD_BEHAVIOR_PORTS_ENABLED) == True)[0] + 1).tolist()
    LEDL, LEDC, LEDR = [getattr(OutputChannel, f"PWM{num}") for num in behavport_idx]
    WATERL, WATERC, WATERR = [(getattr(OutputChannel, "Valve"), num) for num in behavport_idx]
    PGLin, PGCin, PGRin = [getattr(EventName, f"Port{num}In") for num in behavport_idx]
    PGLout, PGCout, PGRout = [getattr(EventName, f"Port{num}Out") for num in behavport_idx]
except:
    UtilsR.stop('THIS PROTOCOL REQUIRES 3 AND ONLY 3 ACTIVE PORTS, ACTIVATE THEM MANUALLY')


# GET WATER CALIBRATION VALUES
try:
    ValveLtime, ValveCtime, ValveRtime = UtilsR.getWaterCalib(conf.VAR_BOX, behavport_idx)
except:
    UtilsR.stop('COULD NOT FIND WATER CALIBRATION OF BOX: ' + str(conf.VAR_BOX))
if ValveLtime >= 0.3 or (ValveLtime / ValveRtime) > 2 or ValveRtime >= 0.3 or (ValveRtime / ValveLtime) > 2:
    continueTask = UtilsR.dropdownmenu('WATER CALIBRATION ERROR. L: ' + str(ValveLtime) + ', R: ' + str(ValveRtime),
                                       'Continue with these values?', ['Yes', 'No'], 'No')
    if continueTask == 'No':
        UtilsR.stop('')


# GET SOUND CALIBRATION VALUES
try:
    sound1_freq = 6500
    sound2_freq = 31000
    sound1_left_amp, sound1_right_amp, sound2_left_amp, sound2_right_amp = UtilsR.getFrequencyCalib(conf.VAR_BOX, sound1_freq, sound2_freq)
except:
    UtilsR.stop('COULD NOT FIND BROADBAND CALIBRATION OF SOUNDDEVICE: ' + str(conf.VAR_BOX))


# INITIALIZE SOUND STREAM
try:
    sampleRate = 192000
    soundStream = SoundR(sampleRate=sampleRate, deviceOut=conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT INITIALIZE SOUND STREAM, CHECK SOUNDCARD INDEX')


# INITIALIZE VIDEO, PLAY AND RECORD
try:
    currSessionVid = os.path.expanduser('~/VIDEO_pybpod/' + (conf.PYBPOD_SUBJECTS[0]).split("'")[1] + '/')
    if not os.path.exists(os.path.expanduser(currSessionVid)):
        os.makedirs(os.path.expanduser(currSessionVid))
    cam = VideoR(indx_or_path=conf.VAR_BOX, name_video=conf.PYBPOD_SESSION + '.avi', path=currSessionVid,
                 title=conf.VAR_BOX, fps=120, codec_cam='X264', codec_video='X264')
    cam.play()
    if int(conf.VAR_REC) > 0:
        cam.record()
except:
    UtilsR.stop('COULD NOT INITIALIZE VIDEO, CHECK VIDEO INDEX')


startingSide = int(np.random.choice([0, 1]))  # which side to start
startingBlock = int(np.random.choice([0, 1]))  # type of block


sessionTimeLength = int(conf.VAR_DURATION_TASK) # in seconds
sessionTimeStart = time.time()
sessionTimeEnd = sessionTimeStart + sessionTimeLength


coherences = [-1, -0.75, -0.45, 0, 0.45, 0.75, 1]


prob_repeat_in_alt = float(conf.VAR_P_REPEAT_ALT)
prob_repeat_in_rep = float(conf.VAR_P_REPEAT_REP)

tm = [
  [prob_repeat_in_alt, prob_repeat_in_alt],  # blocktype0 = Alt block
  [prob_repeat_in_rep, prob_repeat_in_rep]   # blocktype1 = Rep block
]

alt_block_length = int(conf.VAR_BLEN_ALT)
rep_block_length = int(conf.VAR_BLEN_REP)

# Define the different block lengths (larger for Alt: blocktype1)
unbalanced_len = list([alt_block_length, rep_block_length])

# Use UtilsR.newBlock instead of UtilsR.block (starting block = 1, Alt)
trial_list, prob_repeat = UtilsR.newBlock(tm, sblock=startingBlock, block_seq=[0, 1] * 17, block_lens=unbalanced_len*17)
# [20,100]*17 = around 2000 trials





trial_list = list(trial_list)

sessionTimeLength = 3000  # secs
sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart + sessionTimeLength






# BPOD AND SOFTCODE HANDLER
my_bpod = Bpod()

def my_softcode_handler(data):
    global soundStream
    if data == 68:
        soundStream.playSound()
    elif data == 66:
        soundStream.stopSound()

my_bpod.softcode_handler_function = my_softcode_handler


# REGISTER VALUES
my_bpod.register_value('STAGE_NUMBER', stage_number)
my_bpod.register_value('TASK', [task_name, task_version, tasks_commit, md5sum])
my_bpod.register_value('REWARD_SIDE', trial_list)  # required for trend plugin [0-left, 1-right]
my_bpod.register_value('sound1_freq', sound1_freq)
my_bpod.register_value('sound2_freq', sound2_freq)
my_bpod.register_value('sound1_left_amp', sound1_left_amp)
my_bpod.register_value('sound1_right_amp', sound1_right_amp)
my_bpod.register_value('sound2_left_amp', sound2_left_amp)
my_bpod.register_value('sound2_right_amp', sound2_right_amp)
my_bpod.register_value('left_valve', ValveLtime)
my_bpod.register_value('right_valve', ValveRtime)


# MAIN LOOP
for i in range(len(trial_list)):

    timeleft = sessionTimeEnd - time.time()

    if timeleft <= 0:
        break

    coh_to_chose_from = np.array(coherences)
    curr_coh = UtilsR.select_evidence(trial_list[i], coh_to_chose_from)

    print(curr_coh)
    SL, SR, EL, ER = UtilsR.generate_sound_freq(stim_evidence=curr_coh, sound_duration=1,
                                                modulator_freq=20, variance=0.015,
                                                sound1_freq=sound1_freq, sound2_freq=sound2_freq,
                                                sound1_left_amp=sound1_left_amp, sound1_right_amp=sound1_right_amp,
                                                sound2_left_amp=sound2_left_amp, sound2_right_amp=sound2_right_amp,
                                                out_freq=sampleRate)

    soundStream.load(SL, SR)

    if trial_list[i] == 0:
        valvetime = ValveLtime
        rewardValve = WATERL
        resp = {PGLin: 'Reward', PGRin: 'Punish', EventName.Tup: 'Invalid'}
        rewardLED = (LEDL, 8)
        centralLED = (LEDC, 8)
    elif trial_list[i] == 1:
        valvetime = ValveRtime
        rewardValve = WATERR
        resp = {PGRin: 'Reward', PGLin: 'Punish', EventName.Tup: 'Invalid'}
        rewardLED = (LEDR, 8)
        centralLED = (LEDC, 8)

    my_bpod.register_value('coherence01', curr_coh)
    my_bpod.register_value('left_envelope', EL.tolist())
    my_bpod.register_value('right_envelope', ER.tolist())
    my_bpod.register_value('prob_repeat', prob_repeat[i])

    sma = StateMachine(my_bpod)
    sma.set_global_timer_legacy(timer_id=1, timer_duration=timeleft)

    sma.add_state(
        state_name='WaitCPoke',
        state_timer=timeleft,
        state_change_conditions={PGCin: 'Fixation', Bpod.Events.GlobalTimer1_End: 'exit', EventName.Tup: 'exit'},
        output_actions=[centralLED, (Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(
        state_name='Fixation',
        state_timer=0.3,
        state_change_conditions={PGCout: 'WaitCPoke', EventName.Tup: 'StartSound'},
        output_actions=[centralLED])
    sma.add_state(
        state_name='StartSound',
        state_timer=1,
        state_change_conditions={PGCout: 'WaitResponse', EventName.Tup: 'WaitResponse'},
        output_actions=[(OutputChannel.SoftCode, 68)])
    sma.add_state(
        state_name='WaitResponse',
        state_timer=8,
        state_change_conditions=resp,
        output_actions=[(OutputChannel.SoftCode, 66)])
    sma.add_state(
        state_name='Reward',
        state_timer=valvetime,
        state_change_conditions={EventName.Tup: 'keep-led-on'},
        output_actions=[(OutputChannel.SoftCode, 66), rewardValve, rewardLED])
    sma.add_state(
        state_name='keep-led-on',
        state_timer=0.3 - valvetime,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[rewardLED])
    sma.add_state(
        state_name='Punish',
        state_timer=2,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[(OutputChannel.SoftCode, 66)])
    sma.add_state(
        state_name='Invalid',
        state_timer=0.001,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[])

    my_bpod.send_state_machine(sma)
    my_bpod.run_state_machine(sma)

# CLOSING AND REPORTS
my_bpod.close()
cam.stop()
print('EXECUTION TIME', time.time() - sessionTimeStart)

time.sleep(5)
os.system('datahandler -l ' + conf.PYBPOD_SESSION[:-7])
pdfpath = os.path.expanduser('~/daily_reports')+'/'+conf.PYBPOD_SUBJECTS[0].split("'")[1]+f'/{conf.PYBPOD_SESSION}.pdf'
if 'VAR_SLACK' in conf.PYBPOD_VARSNAMES:
    UtilsR.slack_spam(f'{conf.PYBPOD_SESSION} report', pdfpath, conf.VAR_SLACK)
