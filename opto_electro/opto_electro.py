from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import user_settings as conf
from toolsR import SoundR, VideoR, UtilsR
import numpy as np
import os
import time
from toolsR.utils.pulse_pal import PulsePal
import random


# TASK
task_version = '1.0'
stage_number = 1
task_name = 'opto_electro'
try:
    tasks_commit, md5sum = UtilsR.get_git_revision_hash('~/projects/tasks/', conf.PYBPOD_PROTOCOL)
except:
    tasks_commit, md5sum = ['coud not find git commit or md5checksum']*2


# PORTS
try:
    behavport_idx = (np.where(np.array(conf.BPOD_BEHAVIOR_PORTS_ENABLED) == True)[0] + 1).tolist()
    LEDL, LEDC, LEDR = [getattr(OutputChannel, f"PWM{num}") for num in behavport_idx]
    WATERL, WATERC, WATERR = [(getattr(OutputChannel, "Valve"), num) for num in behavport_idx]
    PGLin, PGCin, PGRin = [getattr(EventName, f"Port{num}In") for num in behavport_idx]
    PGLout, PGCout, PGRout = [getattr(EventName, f"Port{num}Out") for num in behavport_idx]
except:
    UtilsR.stop('THIS PROTOCOL REQUIRES 3 AND ONLY 3 ACTIVE PORTS, ACTIVATE THEM MANUALLY')


# GET WATER CALIBRATION VALUES
try:
    ValveLtime, ValveCtime, ValveRtime = UtilsR.getWaterCalib(conf.VAR_BOX, behavport_idx)
except:
    UtilsR.stop('COULD NOT FIND WATER CALIBRATION OF BOX: ' + str(conf.VAR_BOX))
if ValveLtime >= 0.3 or (ValveLtime / ValveRtime) > 2 or ValveRtime >= 0.3 or (ValveRtime / ValveLtime) > 2:
    continueTask = UtilsR.dropdownmenu('WATER CALIBRATION ERROR. L: ' + str(ValveLtime) + ', R: ' + str(ValveRtime),
                                       'Continue with these values?', ['Yes', 'No'], 'No')
    if continueTask == 'No':
        UtilsR.stop('')


# GET SOUND CALIBRATION VALUES
try:
    LAmp, RAmp = UtilsR.getBroadbandCalib(conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT FIND BROADBAND CALIBRATION OF SOUNDDEVICE: ' + str(conf.VAR_BOX))


# INITIALIZE SOUND STREAM
try:
    sampleRate = 192000
    soundStream = SoundR(sampleRate=sampleRate, deviceOut=conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT INITIALIZE SOUND STREAM, CHECK SOUNDCARD INDEX')


# INITIALIZE VIDEO, PLAY AND RECORD
try:
    currSessionVid = os.path.expanduser('~/VIDEO_pybpod/' + (conf.PYBPOD_SUBJECTS[0]).split("'")[1] + '/')
    if not os.path.exists(os.path.expanduser(currSessionVid)):
        os.makedirs(os.path.expanduser(currSessionVid))

    cam_sync = {"Light": (600, 30)}

    cam = VideoR(indx_or_path=conf.VAR_BOX, name_video=conf.PYBPOD_SESSION + '.avi', path=currSessionVid,
                 title=conf.VAR_BOX, fps=120, codec_cam='X264', codec_video='X264',
                 cam_sync=cam_sync, states_on_sync=True)
    cam.play()
    if int(conf.VAR_REC) > 0:
        cam.record()
except:
    UtilsR.stop('COULD NOT INITIALIZE VIDEO, CHECK VIDEO INDEX')


# EVIDENCES
if conf.VAR_EVIDENCES == "easy":
    evidences = [1, 0.5, 0.25, 0, -0.25, -0.5, -1]
elif conf.VAR_EVIDENCES == "medium":
    evidences = [1, 0.5, 0.25, 0.25, 0.001, -0.001, -0.25, -0.25, -0.5, -1]
elif conf.VAR_EVIDENCES == "hard":
    evidences = [1, 0.5, 0.25, 0.25, 0.25, 0.25, 0.001, 0.001, -0.001, -0.001,
                 -0.25, -0.25, -0.25, -0.25, -0.5, -1]
elif conf.VAR_EVIDENCES == "super-hard":
    evidences = [1, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.001, 0.001, 0.001, -0.001, -0.001, -0.001,
                 -0.25, -0.25, -0.25, -0.25, -0.25, -0.25, -0.5, -1]
else:
    UtilsR.stop('VAR_EVIDENCES SHOULD BE easy, medium, hard or super-hard')
evidences = np.array(evidences)


# ZT AND LIGHT
zt_threshold = 0.5
first_trials = 20  # first trials with light always off

zts = []  # to store the zts
hits = []  # to store the hits(reward is not nan)
lights = []  # to store the light-on trials
lights_off = []  # to store the light-off trials (trials that meet the conditions to be light but are not light)


# LASER SIDE
targetdic = ["unilateral", "disconnected"]
targetdefault = "unilateral"
targettitle = "Laser side"
targetlabel = "laser_side"

laser_side = UtilsR.dropdownmenu(targettitle, targetlabel, targetdic, targetdefault)

laser = False
my_pulse_pal = None
states_with_light = ""

if laser_side != "disconnected":

    laser = True

    # PROPORTION LIGHT
    targetdic = ['10', '20', '30', '40', '50']
    targetdefault = "50"
    targettitle = "proportion_light"
    targetlabel = "proportion_light"

    proportion_light = float(UtilsR.dropdownmenu(targettitle, targetlabel, targetdic, targetdefault)) / 100

    states_with_light = 'iti_electro'
    max_duration_light = 1.0
    duration_ramp = 0.1


    try:
        my_pulse_pal = PulsePal(address='/dev/pulsepal')
        pulse1 = my_pulse_pal.create_square_pulse(max_duration_light, 0, duration_ramp, 5)
        pulse2 = my_pulse_pal.create_square_pulse(duration_ramp, 0.0, duration_ramp, 5)
        my_pulse_pal.assign_pulse(pulse1, 1)
        my_pulse_pal.assign_pulse(pulse2, 2)
    except:
        UtilsR.stop('PULSE PAL NOT FOUND')


# BLOCKS
startingSide = int(np.random.choice([0, 1]))  # which side to start
startingBlock = int(np.random.choice([0, 1]))  # type of block


# DURATION TASK
sessionTimeLength = int(conf.VAR_DURATION_TASK)  # in seconds
sessionTimeStart = time.time()
sessionTimeEnd = sessionTimeStart + sessionTimeLength

prob_repeat_in_alt = float(conf.VAR_P_REPEAT_ALT)
prob_repeat_in_rep = float(conf.VAR_P_REPEAT_REP)

tm = [
    [prob_repeat_in_alt, prob_repeat_in_alt],
    [prob_repeat_in_rep, prob_repeat_in_rep]
]

alt_block_length = int(conf.VAR_BLEN_ALT)
rep_block_length = int(conf.VAR_BLEN_REP)

if startingBlock == 0:
    unbalanced_len = list([alt_block_length, rep_block_length])
else:
    unbalanced_len = list([rep_block_length, alt_block_length])

trial_list, prob_repeat = UtilsR.newBlock(tm, sblock=startingBlock, bnum=24, block_lens=unbalanced_len*12)

trial_list = list(trial_list)


# STIMULI
duration = 5
band_fs = [2000, 20000]
filter_len = 10000
soundVec = UtilsR.whiteNoiseGen(1.0, band_fs[0], band_fs[1], duration, FsOut=sampleRate, Fn=filter_len)


def my_softcode_handler(data):
    global soundStream, cam, my_pulse_pal, states_with_light
    if data == 1:
        if laser:
            cam.put_state('Light')
            my_pulse_pal.trigger_pulse(1)
    elif data == 2:
        if laser:
            my_pulse_pal.trigger_pulse(2)
            cam.put_state('')
    elif data == 3:
        if laser:
            cam.put_state('')
    elif data == 68:
        soundStream.playSound()
    elif data == 66:
        soundStream.stopSound()


my_bpod = Bpod()

my_bpod.softcode_handler_function = my_softcode_handler

my_bpod.register_value('TASK', [task_name, task_version, tasks_commit, md5sum])
my_bpod.register_value('STAGE_NUMBER', stage_number)
my_bpod.register_value('REWARD_SIDE', trial_list)  # required for trend plugin [0-left, 1-right]

my_bpod.register_value('left_amp', LAmp)
my_bpod.register_value('right_amp', RAmp)

my_bpod.register_value('left_valve', ValveLtime)
my_bpod.register_value('right_valve', ValveRtime)
my_bpod.register_value('prob_repeat_in_alt', prob_repeat_in_alt)
my_bpod.register_value('prob_repeat_in_rep', prob_repeat_in_rep)
my_bpod.register_value('alt_block_length', alt_block_length)
my_bpod.register_value('rep_block_length', rep_block_length)

my_bpod.register_value('first_trials', first_trials)
my_bpod.register_value('zt_threshold', zt_threshold)
my_bpod.register_value('laser_side', laser_side)

if laser:
    my_bpod.register_value('states_with_light', states_with_light)
    my_bpod.register_value('proportion_light', proportion_light)
    my_bpod.register_value('max_duration_light', max_duration_light)
    my_bpod.register_value('duration_ramp', duration_ramp)

afterError = False


for i in range(len(trial_list)):

    timeleft = sessionTimeEnd - time.time()

    if timeleft <= 0:
        break

    curr_coh = UtilsR.select_evidence(trial_list[i], evidences)  # curr_coh in space (0, 1) evidences in space(-1, 1)

    SL, SR, EL, ER = UtilsR.envelope(curr_coh, soundVec, 1, 20, samplingR=sampleRate, variance=0.015, randomized=False,
                                     paired=False, LAmp=LAmp, RAmp=RAmp)

    soundStream.load(SL, SR)

    lights.append(False)
    lights_off.append(False)

    if laser and i > first_trials:
        if random.randint(0, 99) < proportion_light * 100 and not afterError:
            lights[i] = True
        else:
            lights_off[i] = True

    if trial_list[i] == 0:
        valvetime = ValveLtime
        rewardValve = WATERL
        resp = {PGLin: 'Reward', PGRin: 'Punish', EventName.Tup: 'Invalid'}
        rewardLED = (LEDL, 8)
        centralLED = (LEDC, 8)
    elif trial_list[i] == 1:
        valvetime = ValveRtime
        rewardValve = WATERR
        resp = {PGRin: 'Reward', PGLin: 'Punish', EventName.Tup: 'Invalid'}

        rewardLED = (LEDR, 8)
        centralLED = (LEDC, 8)

    my_bpod.register_value('coherence01', curr_coh)
    my_bpod.register_value('left_envelope', EL.tolist())
    my_bpod.register_value('right_envelope', ER.tolist())
    my_bpod.register_value('prob_repeat', prob_repeat[i])

    try:
        my_bpod.register_value('zt', zts[i-1])
    except:
        my_bpod.register_value('zt', 0)

    my_bpod.register_value('light_on_trial', lights[i])
    my_bpod.register_value('light_off_trial', lights_off[i])

    sma = StateMachine(my_bpod)
    sma.set_global_timer_legacy(timer_id=1, timer_duration=timeleft)
    sma.set_global_timer_legacy(timer_id=2, timer_duration=0.3)

    if states_with_light == 'iti_electro' and lights[i]:

        sma.add_state(
            state_name='WaitAnyPoke',
            state_timer=timeleft,
            state_change_conditions={PGCin: 'Fixation', PGLout: 'TurnLightOn', PGRout: 'TurnLightOn',
                                     Bpod.Events.GlobalTimer1_End: 'exit', EventName.Tup: 'exit'},
            output_actions=[centralLED, (Bpod.OutputChannels.GlobalTimerTrig, 1)])
        sma.add_state(
            state_name='TurnLightOn',
            state_timer=0.001,
            state_change_conditions={EventName.Tup: 'WaitCPokeLight'},
            output_actions=[centralLED, (OutputChannel.SoftCode, 1)])
        sma.add_state(
            state_name='WaitCPokeLight',
            state_timer=max_duration_light - 0.1,
            state_change_conditions={PGCin: 'TurnLightOff', Bpod.Events.GlobalTimer1_End: 'exit',
                                     EventName.Tup: 'TurnLightOff2'},
            output_actions=[centralLED])
        sma.add_state(
            state_name='TurnLightOff',
            state_timer=0.001,
            state_change_conditions={EventName.Tup: 'TurnTimerOnOff'},
            output_actions=[centralLED, (OutputChannel.SoftCode, 2)])
        sma.add_state(
            state_name='TurnLightOff2',
            state_timer=0.001,
            state_change_conditions={EventName.Tup: 'WaitCPoke'},
            output_actions=[centralLED, (OutputChannel.SoftCode, 2), (OutputChannel.BNC1, 0)])

    sma.add_state(
        state_name='WaitCPoke',
        state_timer=timeleft,
        state_change_conditions={PGCin: 'TurnTimerOnOff', Bpod.Events.GlobalTimer1_End: 'exit', EventName.Tup: 'exit'},
        output_actions=[centralLED, (Bpod.OutputChannels.GlobalTimerTrig, 1), (OutputChannel.SoftCode, 3)])
    sma.add_state(
        state_name='TurnTimerOnOff',
        state_timer=0.001,
        state_change_conditions={EventName.Tup: 'Fixation'},
        output_actions=[centralLED, (Bpod.OutputChannels.GlobalTimerTrig, 2)])
    sma.add_state(
        state_name='Fixation',
        state_timer=0.3,
        state_change_conditions={PGCout: 'OutOff', Bpod.Events.GlobalTimer2_End: 'StartSound'},
        output_actions=[centralLED])
    sma.add_state(
        state_name='OutOff',
        state_timer=0.1,
        state_change_conditions={PGCin: 'Fixation', EventName.Tup: 'WaitCPoke',
                                 Bpod.Events.GlobalTimer2_End: 'WaitCPoke'},
        output_actions=[centralLED])
    sma.add_state(
        state_name='StartSound',
        state_timer=1,
        state_change_conditions={PGCout: 'WaitResponse', EventName.Tup: 'WaitResponse'},
        output_actions=[(OutputChannel.SoftCode, 68), (OutputChannel.BNC1, 3)])
    sma.add_state(
        state_name='WaitResponse',
        state_timer=8,
        state_change_conditions=resp,
        output_actions=[(OutputChannel.SoftCode, 66), (OutputChannel.BNC1, 0)])
    sma.add_state(
        state_name='Reward',
        state_timer=valvetime,
        state_change_conditions={EventName.Tup: 'keep-led-on'},
        output_actions=[rewardValve, rewardLED, (OutputChannel.BNC2, 3)])
    sma.add_state(
        state_name='keep-led-on',
        state_timer=0.3 - valvetime,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[rewardLED, (OutputChannel.BNC2, 0)])
    sma.add_state(
        state_name='Punish',
        state_timer=2,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[])
    sma.add_state(
        state_name='Invalid',
        state_timer=0.001,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[])


    my_bpod.send_state_machine(sma)
    my_bpod.run_state_machine(sma)

    trialstring = ("Current trial info: {0}".format(my_bpod.session.current_trial))

    if "'Reward': [(nan, nan)]" in trialstring:
        hits.append(0)
        afterError = True
    else:
        hits.append(1)
        afterError = False

    if i > 0:
        zt, _ = UtilsR.get_zt_light(zts[i-1], hits[i], hits[i-1], trial_list[i], trial_list[i-1])
        zts.append(zt)
    else:
        zts.append(0)


my_bpod.close()
cam.stop()

time.sleep(5)
os.system('datahandler -l ' + conf.PYBPOD_SESSION[:-7])
pdfpath = os.path.expanduser('~/daily_reports')+'/'+conf.PYBPOD_SUBJECTS[0].split("'")[1]+f'/{conf.PYBPOD_SESSION}.pdf'
if 'VAR_SLACK' in conf.PYBPOD_VARSNAMES:
    UtilsR.slack_spam(f'{conf.PYBPOD_SESSION} report', pdfpath, conf.VAR_SLACK)
