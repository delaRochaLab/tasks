from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import user_settings as conf
import random
from toolsR import SoundR, VideoR, UtilsR
import numpy as np
import os
import time


# TASK
task_version = '0.1'
stage_number = 1
task_name = 'p4_freq'
try:
    tasks_commit, md5sum = UtilsR.get_git_revision_hash('~/projects/tasks/', conf.PYBPOD_PROTOCOL)
except:
    tasks_commit, md5sum = ['coud not find git commit or md5checksum']*2


# PORTS
try:
    behavport_idx = (np.where(np.array(conf.BPOD_BEHAVIOR_PORTS_ENABLED) == True)[0] + 1).tolist()
    LEDL, LEDC, LEDR = [getattr(OutputChannel, f"PWM{num}") for num in behavport_idx]
    WATERL, WATERC, WATERR = [(getattr(OutputChannel, "Valve"), num) for num in behavport_idx]
    PGLin, PGCin, PGRin = [getattr(EventName, f"Port{num}In") for num in behavport_idx]
    PGLout, PGCout, PGRout = [getattr(EventName, f"Port{num}Out") for num in behavport_idx]
except:
    UtilsR.stop('THIS PROTOCOL REQUIRES 3 AND ONLY 3 ACTIVE PORTS, ACTIVATE THEM MANUALLY')


# GET WATER CALIBRATION VALUES
try:
    ValveLtime, ValveCtime, ValveRtime = UtilsR.getWaterCalib(conf.VAR_BOX, behavport_idx)
except:
    UtilsR.stop('COULD NOT FIND WATER CALIBRATION OF BOX: ' + str(conf.VAR_BOX))
if ValveLtime >= 0.3 or (ValveLtime / ValveRtime) > 2 or ValveRtime >= 0.3 or (ValveRtime / ValveLtime) > 2:
    continueTask = UtilsR.dropdownmenu('WATER CALIBRATION ERROR. L: ' + str(ValveLtime) + ', R: ' + str(ValveRtime),
                                       'Continue with these values?', ['Yes', 'No'], 'No')
    if continueTask == 'No':
        UtilsR.stop('')


# GET SOUND CALIBRATION VALUES
try:
    sound1_freq = 6500
    sound2_freq = 31000
    sound1_left_amp, sound1_right_amp, sound2_left_amp, sound2_right_amp = UtilsR.getFrequencyCalib(conf.VAR_BOX, sound1_freq, sound2_freq)
except:
    UtilsR.stop('COULD NOT FIND BROADBAND CALIBRATION OF SOUNDDEVICE: ' + str(conf.VAR_BOX))


# INITIALIZE SOUND STREAM
try:
    sampleRate = 192000
    soundStream = SoundR(sampleRate=sampleRate, deviceOut=conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT INITIALIZE SOUND STREAM, CHECK SOUNDCARD INDEX')


# INITIALIZE VIDEO, PLAY AND RECORD
try:
    currSessionVid = os.path.expanduser('~/VIDEO_pybpod/' + (conf.PYBPOD_SUBJECTS[0]).split("'")[1] + '/')
    if not os.path.exists(os.path.expanduser(currSessionVid)):
        os.makedirs(os.path.expanduser(currSessionVid))
    cam = VideoR(indx_or_path=conf.VAR_BOX, name_video=conf.PYBPOD_SESSION + '.avi', path=currSessionVid,
                 title=conf.VAR_BOX, fps=120, codec_cam='X264', codec_video='X264')
    cam.play()
    if int(conf.VAR_REC) > 0:
        cam.record()
except:
    UtilsR.stop('COULD NOT INITIALIZE VIDEO, CHECK VIDEO INDEX')


# RELEVANT VARIABLES
sessionTimeLength = 3000  # secs
sessionTimeStart = time.time()
sessionTimeEnd = sessionTimeStart + sessionTimeLength

valid_trials = 0


PCT_list = [400, 360, 320, 280, 240, 200, 160, 120, 80, 40, 0]


if int(conf.VAR_EASY) == 2:
    coherences_list = [[-1, 1],
                       [-1, 1],
                       [-1, 1],
                       [-1, 1],
                       [-1, 1],
                       [-1, 1],
                       [-1, 1],
                       [-1, 1],
                       [-1, 1],
                       [-1, 1],
                       [-1, 1]
                       ]
else:
    coherences_list = [[-1, -1, -0.9, 0.9, 1, 1],
                       [-1, -0.9, -0.85, 0.85, 0.9, 1],
                       [-1, -0.9, -0.75, 0.75, 0.9, 1],
                       [-1, -0.85, -0.65, 0.65, 0.85, 1],
                       [-1, -0.8, -0.55, 0.55, 0.8, 1],
                       [-1, -0.75, -0.45, 0.45, 0.75, 1]
                       ]

fixation_time = float(conf.VAR_FIXATION_TIME)
sound_time = max(float(conf.VAR_SOUND_TIME), 0.001)
left_probability = float(conf.VAR_LEFT_PROB)

try:
    substage = int(conf.VAR_SUBSTAGE)
    PCT = PCT_list[substage]
    coherences = coherences_list[substage]
    min_substage = substage
except:
    UtilsR.stop('SUBSTAGE MUST BE A VALUE FROM 0 TO 10: ' + str(conf.VAR_BOX))

trial_list = list(random.choices(population=[0, 1], weights=[left_probability, 1 - left_probability], k=2000))

accwindow_L = [0] * 50
accwindow_R = [0] * 50

# BPOD AND SOFTCODE HANDLER
my_bpod = Bpod()

def my_softcode_handler(data):
    global soundStream
    if data == 68:
        soundStream.playSound()
    elif data == 66:
        soundStream.stopSound()

my_bpod.softcode_handler_function = my_softcode_handler


# REGISTER VALUES
my_bpod.register_value('STAGE_NUMBER', stage_number)
my_bpod.register_value('SUBSTAGE', conf.VAR_SUBSTAGE)
my_bpod.register_value('TASK', [task_name, task_version, tasks_commit, md5sum])
my_bpod.register_value('REWARD_SIDE', trial_list)  # required for trend plugin [0-left, 1-right]
my_bpod.register_value('sound1_freq', sound1_freq)
my_bpod.register_value('sound2_freq', sound2_freq)
my_bpod.register_value('sound1_left_amp', sound1_left_amp)
my_bpod.register_value('sound1_right_amp', sound1_right_amp)
my_bpod.register_value('sound2_left_amp', sound2_left_amp)
my_bpod.register_value('sound2_right_amp', sound2_right_amp)
my_bpod.register_value('left_valve', ValveLtime)
my_bpod.register_value('right_valve', ValveRtime)


# MAIN LOOP
for i in range(len(trial_list)):

    timeleft = sessionTimeEnd - time.time()

    if timeleft <= 0:
        break

    coh_to_chose_from = np.array(coherences)
    curr_coh = UtilsR.select_evidence(trial_list[i], coh_to_chose_from)

    print(curr_coh)
    SL, SR, EL, ER = UtilsR.generate_sound_freq(stim_evidence=(curr_coh + 1) / 2, sound_duration=1,
                                                modulator_freq=20, variance=0.015,
                                                sound1_freq=sound1_freq, sound2_freq=sound2_freq,
                                                sound1_left_amp=sound1_left_amp, sound1_right_amp=sound1_right_amp,
                                                sound2_left_amp=sound2_left_amp, sound2_right_amp=sound2_right_amp,
                                                out_freq=sampleRate)

    soundStream.load(SL, SR)

    if trial_list[i] == 0:
        valvetime = ValveLtime
        rewardValve = WATERL
        resp = {PGLin: 'Reward', PGRin: 'Punish', EventName.Tup: 'WaitResponse'}
        resp2 = {PGLin: 'Reward', PGRin: 'Punish'}
        rewardLED = (LEDL, 8)
        centralLED = (LEDC, 8)
    elif trial_list[i] == 1:
        valvetime = ValveRtime
        rewardValve = WATERR
        resp = {PGRin: 'Reward', PGLin: 'Punish', EventName.Tup: 'WaitResponse'}
        resp2 = {PGRin: 'Reward', PGLin: 'Punish'}
        rewardLED = (LEDR, 8)
        centralLED = (LEDC, 8)

    my_bpod.register_value('coherence01', curr_coh)
    my_bpod.register_value('left_envelope', EL.tolist())
    my_bpod.register_value('right_envelope', ER.tolist())
    my_bpod.register_value('prob_repeat', 0.5)

    sma = StateMachine(my_bpod)
    sma.set_global_timer_legacy(timer_id=1, timer_duration=timeleft)

    sma.add_state(
        state_name='WaitCPoke',
        state_timer=timeleft,
        state_change_conditions={PGCin: 'Fixation', Bpod.Events.GlobalTimer1_End: 'exit', EventName.Tup: 'exit'},
        output_actions=[centralLED, (Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(
        state_name='Fixation',
        state_timer=fixation_time,
        state_change_conditions={PGCout: 'WaitCPoke', EventName.Tup: 'StartSound'},
        output_actions=[centralLED])

    sma.add_state(
        state_name='StartSound',
        state_timer=sound_time,
        state_change_conditions={PGCout: 'StopSound', EventName.Tup: 'StartSound2'},
        output_actions=[(OutputChannel.SoftCode, 68)])
    sma.add_state(
        state_name='StopSound',
        state_timer=0.001,
        state_change_conditions={EventName.Tup: 'Invalid'},
        output_actions=[(OutputChannel.SoftCode, 66)])
    sma.add_state(
        state_name='StartSound2',
        state_timer=0.001,
        state_change_conditions={PGCout: 'KeepSoundOn'},
        output_actions=[])

    #sma.add_state(
    #    state_name='StartSound',
    #    state_timer=0.001,
    #    state_change_conditions={PGCout: 'KeepSoundOn'},
    #    output_actions=[(OutputChannel.SoftCode, 68)])

    sma.add_state(
        state_name='KeepSoundOn',
        state_timer=PCT / 1000,
        state_change_conditions=resp,
        output_actions=[])
    sma.add_state(
        state_name='WaitResponse',
        state_timer=1,
        state_change_conditions=resp2,
        output_actions=[(OutputChannel.SoftCode, 66)])
    sma.add_state(
        state_name='Reward',
        state_timer=valvetime,
        state_change_conditions={EventName.Tup: 'keep-led-on'},
        output_actions=[(OutputChannel.SoftCode, 66), rewardValve, rewardLED])
    sma.add_state(
        state_name='keep-led-on',
        state_timer=0.3 - valvetime,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[rewardLED])
    sma.add_state(
        state_name='Punish',
        state_timer=2,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[(OutputChannel.SoftCode, 66)])
    sma.add_state(  # we never come here, it exists in order the trend plugin works
        state_name='Invalid',
        state_timer=0.001,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[])

    my_bpod.send_state_machine(sma)
    my_bpod.run_state_machine(sma)

    my_bpod.register_value('Post_central_time', PCT)
    my_bpod.register_value('Substage', substage)

    trialstring = ("Current trial info: {0}".format(my_bpod.session.current_trial))

    if "'Punish': [(nan, nan)]" in trialstring:  # Punish state was not triggered = rat poked in the correct port
        if trial_list[i] == 0:
            accwindow_L = accwindow_L[1:] + [1]
        else:
            accwindow_R = accwindow_R[1:] + [1]
        valid_trials += 1
    elif "'Reward': [(nan, nan)]" in trialstring:
        if trial_list[i] == 0:
            accwindow_L = accwindow_L[1:] + [0]
        else:
            accwindow_R = accwindow_R[1:] + [0]
        valid_trials += 1


    if valid_trials % 50 == 0:

        if (sum(accwindow_L) / len(accwindow_L)) > 0.80 and (sum(accwindow_R) / len(accwindow_R)) > 0.80 and substage < 3:
            substage += 1
        elif (sum(accwindow_L) / len(accwindow_L)) > 0.76 and (sum(accwindow_R) / len(accwindow_R)) > 0.76 and substage >= 3:
            if substage < min_substage + 2:
                substage += 1
        elif (sum(accwindow_L) / len(accwindow_L)) < 0.70 or (sum(accwindow_R) / len(accwindow_R)) < 0.70:
            if substage > min_substage - 1 and substage > 0:
                substage -= 1

    if int(conf.VAR_EASY) == 1 or int(conf.VAR_EASY) == 2:
        PCT = 0
    else:
        PCT = PCT_list[substage]

    coherences = coherences_list[substage]

# CLOSING AND REPORTS
conf.VAR_SUBSTAGE = str(max(substage - 2, 0))
my_bpod.close()
cam.stop()
print('EXECUTION TIME', time.time() - sessionTimeStart)
os.system('datahandler -l ' + conf.PYBPOD_SESSION[:-7])
