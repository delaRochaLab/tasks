"""
required vars = [
    VAR_BOX: str, box name (paired with bpod), eg 'BPOD01';
    VAR_REC: int, 0=do not record; 1=record;
    VAR_BLEN: int (even), block len; 150
    VAR_DATA: int, 0= do not save using rach util; 1= save data using R util,
    VAR_BNUM: int, block number]

Update_status var = []

# known issues:

@07/03/2019: Adapted by DDD to allow for characterization of neural responses by
presenting TORCs (temporally orthogonal ripple sound) and for construction of
STRFs (spectro-temporal receptive fields)
- Lines 55-61 (defines variables for TORC passive presentation)
- Lines 98-115 (creates random trial_list, consisting in 5 repetitions of 30 TORCs)
- Lines 156-184 (change loading of sounds in main loop and saves TORC name)
@12/03/2019: Modified after testing with Yerko to get rid of calibration and Video problems.
@18/03/2019: line 235 value changed from 1 --> 3 to make sound lasts for 3 sec

"""

from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import user_settings as conf
from toolsR import SoundR, VideoR, UtilsR
import numpy as np
import os
import timeit
import time
import datetime
import csv
from scipy.signal import firwin, lfilter
import soundfile as sf

# TASK
task_version = '0.2'
stage_number = 1
task_name = 'p1_STRFs'
try:
    tasks_commit, md5sum = UtilsR.get_git_revision_hash('~/projects/tasks/', conf.PYBPOD_PROTOCOL)
except:
    tasks_commit, md5sum = ['coud not find git commit or md5checksum']*2

# PORTS
try:
    behavport_idx = (np.where(np.array(conf.BPOD_BEHAVIOR_PORTS_ENABLED) == True)[0] + 1).tolist()
    LEDL, LEDC, LEDR = [getattr(OutputChannel, f"PWM{num}") for num in behavport_idx]
    WATERL, WATERC, WATERR = [(getattr(OutputChannel, "Valve"), num) for num in behavport_idx]
    PGLin, PGCin, PGRin = [getattr(EventName, f"Port{num}In") for num in behavport_idx]
    PGLout, PGCout, PGRout = [getattr(EventName, f"Port{num}Out") for num in behavport_idx]
except:
    UtilsR.stop('THIS PROTOCOL REQUIRES 3 AND ONLY 3 ACTIVE PORTS, ACTIVATE THEM MANUALLY')

# GET WATER CALIBRATION VALUES
#try:
#    ValveLtime, ValveCtime, ValveRtime = UtilsR.getWaterCalib(conf.VAR_BOX, behavport_idx)
#except:
#    UtilsR.stop('COULD NOT FIND WATER CALIBRATION OF BOX: ' + str(conf.VAR_BOX))
#if ValveLtime >= 0.3 or (ValveLtime / ValveRtime) > 2 or ValveRtime >= 0.3 or (ValveRtime / ValveLtime) > 2:
#    continueTask = UtilsR.dropdownmenu('WATER CALIBRATION ERROR. L: ' + str(ValveLtime) + ', R: ' + str(ValveRtime),
#                                       'Continue with these values?', ['Yes', 'No'], 'No')
#    if continueTask == 'No':
#        UtilsR.stop('')

# GET SOUND CALIBRATION VALUES (BROADBAND)
try:
    LAmp, RAmp = UtilsR.getBroadbandCalib(conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT FIND BROADBAND CALIBRATION OF SOUNDDEVICE: ' + str(conf.VAR_BOX))

# INITIALIZE SOUND STREAM
try:
    sampleRate = 192000
    soundStream = SoundR(sampleRate=sampleRate, deviceOut=conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT INITIALIZE SOUND STREAM, CHECK SOUNDCARD INDEX')

# INITIALIZE VIDEO, PLAY AND RECORD
try:
    currSessionVid = os.path.expanduser('~/VIDEO_pybpod/' + (conf.PYBPOD_SUBJECTS[0]).split("'")[1] + '/')
    if not os.path.exists(os.path.expanduser(currSessionVid)):
        os.makedirs(os.path.expanduser(currSessionVid))
    cam = VideoR(indx_or_path=conf.VAR_BOX, name_video=conf.PYBPOD_SESSION + '.avi', path=currSessionVid,
                 title=conf.VAR_BOX, fps=120, codec_cam='X264', codec_video='X264')
    cam.play()
    if int(conf.VAR_REC) > 0:
        cam.record()
except:
    UtilsR.stop('COULD NOT INITIALIZE VIDEO, CHECK VIDEO INDEX')



########################### relevant variables for the task ####################
# Edited by DDD @07/03/2019
# session-trials (there are normally 30 TORCs, we should play them 5 times at least)
nTrials = 150
# TORCs length are around 3 s, so we should have at least 5 seconds length trials
# 1 s pre-silence + 3 s sound + 1 s post-silence (total 750 s, 12,5 min)
sessionTimeLength = 1000 # secs
sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart+sessionTimeLength

counter_idle_stop_sound = 0


###########################
# Edited by DDD @07/03/2019
# Load folder for stimuli
soundDir= "/home/delarocha7/Downloads/p1_STRFs/Sounds2"
i = 0

# Create list of 150 sounds to play (trial_list)
# It creates 5 iterations of 30 TORCs
trial_list = []
for iterations in range(5):
    torc_list = np.random.choice(list(range(1,31)), 30, replace=False)
    for sounds in torc_list:
        soundname = str(sounds)
        # if sound length is 1, add a zero at the begining
        if len(soundname) == 1:
            soundname = soundname.zfill(2)
        filepath = os.path.join(soundDir, "TORC_424_"+soundname+"_u501.wav")
        trial_list += [filepath]

print(trial_list)

def my_softcode_handler(data):
    global start, timeit, soundStream, i
    if data == 68:
        print('PLAY-------', i)
        print(timeit.default_timer() - start)
        soundStream.playSound()
    elif data == 66:
        soundStream.stopSound()
        i += 1
        if i < 150:
            soundVec, fs = sf.read(trial_list[i], dtype='float32')
            RStim = soundVec*RAmp
            LStim = soundVec*LAmp
            soundStream.load(LStim, RStim)

WaitForPoke_timer=29.5
list_trial=[0]*150

######################## BPOD and States #########################
start = timeit.default_timer()
my_bpod = Bpod()
my_bpod.register_value('STAGE_NUMBER', stage_number)
my_bpod.register_value('TASK', [task_name, task_version, tasks_commit, md5sum])
my_bpod.register_value('REWARD_SIDE', list_trial) # required for trend plugin [0-left, 1-right]
my_bpod.register_value('left_amp', LAmp)
my_bpod.register_value('right_amp', RAmp)

my_bpod.softcode_handler_function = my_softcode_handler

###########################
# Edited by DDD @07/03/2019
# Main loop

print('FIRST LOAD-------')
print(timeit.default_timer() - start)
soundVec, fs = sf.read(trial_list[0], dtype='float32')
RStim = soundVec*RAmp
LStim = soundVec*LAmp
soundStream.load(LStim, RStim)

# Save TORC name
for idx in range(len(trial_list)):
    my_bpod.register_value('TORC', os.path.split(trial_list[idx])[-1])

sma = StateMachine(my_bpod)
sma.set_global_timer(timer_id=1, timer_duration=750)

sma.add_state(
    state_name='StartTimer',
    state_timer=0.001,
    state_change_conditions={Bpod.Events.Tup: 'StartSound'},
    output_actions=[(Bpod.OutputChannels.GlobalTimerTrig, 1)])
sma.add_state(
    state_name='StartSound',
    state_timer=3,
    state_change_conditions={Bpod.Events.Tup: 'stopsound', Bpod.Events.GlobalTimer1_End: 'exit'},
    output_actions=[(OutputChannel.SoftCode, 68), (OutputChannel.BNC1, 3)])
sma.add_state(
    state_name='stopsound',
    state_timer=2,
    state_change_conditions={Bpod.Events.Tup: 'StartSound', Bpod.Events.GlobalTimer1_End: 'exit'},
    output_actions=[(OutputChannel.SoftCode, 66), (OutputChannel.BNC1, 0)])

my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device
my_bpod.run_state_machine(sma)  # Run state machine

my_bpod.close()  # Disconnect Bpod and perform post-run actions
cam.stop()
