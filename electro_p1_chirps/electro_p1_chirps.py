"""
required vars = [
    VAR_BOX: str, box name (paired with bpod), eg 'BPOD01';
    VAR_REC: int, 0=do not record; 1=record;
    VAR_BLEN: int (even), block len; 50
    VAR_DATA: int, 0= do not save using rach util; 1= save data using R util,
    VAR_BNUM: int, block number]

Update_status var = []

# known issues:
"""

from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import user_settings as conf
from toolsR import SoundR, VideoR, UtilsR
import numpy as np
import os
import timeit
import time
import datetime
import csv
from scipy.signal import firwin, lfilter

# TASK
task_version = '0.2'
stage_number = 1
task_name = 'p1_chirps_2.02'
try:
    tasks_commit, md5sum = UtilsR.get_git_revision_hash('~/projects/tasks/', conf.PYBPOD_PROTOCOL)
except:
    tasks_commit, md5sum = ['coud not find git commit or md5checksum']*2

# PORTS
try:
    behavport_idx = (np.where(np.array(conf.BPOD_BEHAVIOR_PORTS_ENABLED) == True)[0] + 1).tolist()
    LEDL, LEDC, LEDR = [getattr(OutputChannel, f"PWM{num}") for num in behavport_idx]
    WATERL, WATERC, WATERR = [(getattr(OutputChannel, "Valve"), num) for num in behavport_idx]
    PGLin, PGCin, PGRin = [getattr(EventName, f"Port{num}In") for num in behavport_idx]
    PGLout, PGCout, PGRout = [getattr(EventName, f"Port{num}Out") for num in behavport_idx]
except:
    UtilsR.stop('THIS PROTOCOL REQUIRES 3 AND ONLY 3 ACTIVE PORTS, ACTIVATE THEM MANUALLY')

# GET WATER CALIBRATION VALUES
# try:
#    ValveLtime, ValveCtime, ValveRtime = UtilsR.getWaterCalib(conf.VAR_BOX, behavport_idx)
# except:
#    UtilsR.stop('COULD NOT FIND WATER CALIBRATION OF BOX: ' + str(conf.VAR_BOX))
# if ValveLtime >= 0.3 or (ValveLtime / ValveRtime) > 2 or ValveRtime >= 0.3 or (ValveRtime / ValveLtime) > 2:
#    continueTask = UtilsR.dropdownmenu('WATER CALIBRATION ERROR. L: ' + str(ValveLtime) + ', R: ' + str(ValveRtime),
#                                       'Continue with these values?', ['Yes', 'No'], 'No')
#    if continueTask == 'No':
#        UtilsR.stop('')

# GET SOUND CALIBRATION VALUES (BROADBAND)
try:
    LAmp, RAmp = UtilsR.getBroadbandCalib(conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT FIND BROADBAND CALIBRATION OF SOUNDDEVICE: ' + str(conf.VAR_BOX))

# INITIALIZE SOUND STREAM
try:
    sampleRate = 192000
    soundStream = SoundR(sampleRate=sampleRate, deviceOut=conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT INITIALIZE SOUND STREAM, CHECK SOUNDCARD INDEX')

# INITIALIZE VIDEO, PLAY AND RECORD
try:
    currSessionVid = os.path.expanduser('~/VIDEO_pybpod/' + (conf.PYBPOD_SUBJECTS[0]).split("'")[1] + '/')
    if not os.path.exists(os.path.expanduser(currSessionVid)):
        os.makedirs(os.path.expanduser(currSessionVid))
    cam = VideoR(indx_or_path=conf.VAR_BOX, name_video=conf.PYBPOD_SESSION + '.avi', path=currSessionVid,
                 title=conf.VAR_BOX, fps=120, codec_cam='X264', codec_video='X264')
    cam.play()
    if int(conf.VAR_REC) > 0:
        cam.record()
except:
    UtilsR.stop('COULD NOT INITIALIZE VIDEO, CHECK VIDEO INDEX')

########################### relevant variables for the task ##############################

# session-trials
nTrials = 150
sessionTimeLength = 14400  # secs
sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart + sessionTimeLength

# stimuli (white-noise)
duration = 1  # stim len (secs). Does not imply playing whole stim each time
band_fs = [2000, 20000]  # boundaries for the white noise (Hz)
filter_len = 10000  # filter len
# Next we assign sound vector. Returns np.array. How does this work? -> help(whiteNoiseGen)
soundVec = UtilsR.whiteNoiseGen(5.0, band_fs[0], band_fs[1], duration, FsOut=sampleRate,
                                Fn=filter_len)  # using amplitude=1.0, adapt to calib values later.
# soundVec = pureToneGen(1.0, 5000, duration, FsOut=XonarSamplingR,ramp=True) # using amplitude=1.0, adapt to calib values later.

counter_idle_stop_sound = 0

# with calibrations, get stim with envelopes of each side
L0, L1, L2, L3 = UtilsR.envelope(0.5, soundVec, 1, 20, LAmp=LAmp, RAmp=RAmp)

LStim = L0[:19200]  # 0.1 seconds
RStim = L1[:19200]  # 0.1 seconds

##### trial list #######  trialTypes = [0, 1] 0=(rewarded left) or  1=(rewarded right)
# Create an array of nTrials
trial_list = [0] * nTrials


def remainingTime():
    return sessionTimeEnding - time.time()


def my_softcode_handler(data):
    global start, timeit, soundStream, LStim, RStim
    print(data)
    if data == 68:
        print("Play")
        soundStream.playSound()
        start = timeit.default_timer()
    elif data == 66:
        print(':::::::::Time to stop:::::::::', timeit.default_timer() - start)
        soundStream.load(LStim, RStim)


WaitForPoke_timer = 29.5

######################## BPOD and States #########################
START_APP = timeit.default_timer()
my_bpod = Bpod()
my_bpod.register_value('STAGE_NUMBER', stage_number)
my_bpod.register_value('task_version', task_version)
my_bpod.register_value('REWARD_SIDE', trial_list)  # required for trend plugin [0-left, 1-right]
my_bpod.register_value('left_amp', LAmp)
my_bpod.register_value('right_amp', RAmp)
my_bpod.softcode_handler_function = my_softcode_handler

soundStream.load(LStim, RStim)

# Save TORC name
sma = StateMachine(my_bpod)
sma.set_global_timer(timer_id=1, timer_duration=75)

sma.add_state(
    state_name='StartTimer',
    state_timer=0.001,
    state_change_conditions={Bpod.Events.Tup: 'StartSound'},
    output_actions=[(Bpod.OutputChannels.GlobalTimerTrig, 1)])

sma.add_state(
    state_name='StartSound',
    state_timer=0.1,
    state_change_conditions={Bpod.Events.Tup: 'stopsound', Bpod.Events.GlobalTimer1_End: 'exit'},
    output_actions=[(OutputChannel.SoftCode, 68), (OutputChannel.BNC1, 3)])

sma.add_state(
    state_name='stopsound',
    state_timer=0.4,
    state_change_conditions={Bpod.Events.Tup: 'StartSound', Bpod.Events.GlobalTimer1_End: 'exit'},
    output_actions=[(OutputChannel.SoftCode, 66), (OutputChannel.BNC1, 0)])

my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device
my_bpod.run_state_machine(sma)  # Run state machine

my_bpod.close()  # Disconnect Bpod and perform post-run actions
cam.stop()
