"""
TODO: @Jordi, port this
this should be the same widelyused 2afc intensity+RT+dualbetagen envelope
but adding slight sensory feedback when poking inside ports (LEDs blinking)
# Fair (option in dropdown menu):
# try to reward most intense side according what the animal listened
# The issue here is that we can alter transition proba and also,
# this closed loop is exerted by softodes which may involve other issues


required vars (all strings, transformed whithin the task script)= [
    VAR_BOX: str, box name (paired with bpod), eg 'BPOD01';
    VAR_REC: int, 0=do not record; 1=record;
    VAR_BLEN: int (even), block len;
    VAR_DATA: int, 0= do not save using rach util; 1= save data using R util,
    VAR_BNUM: int, block number]
    VAR_SSIDE: 'r'-random, '0'-left, '1'-right
    VAR_SBLOCK: 'r'-random, True - repetitive, 'False'- alternating
    VAR_VARIATION: asked a posteriori, but var needs to exist in pybpodgui
"""
from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import user_settings as conf
from toolsR import SoundR, VideoR, UtilsR
from toolsR import SoftcodeConnection
import numpy as np
import os
import timeit
import time
import datetime
import csv
from scipy.signal import firwin, lfilter
from datahandler.custom_jordi import report_noenv


task_version = '0.5' # decision function fixed
task_name = 'p4_feedback'
stage_number = 4
try:
    tasks_commit, md5sum = UtilsR.get_git_revision_hash('~/projects/tasks/', conf.PYBPOD_PROTOCOL)
except:
    tasks_commit, md5sum = ['coud not find git commit or md5checksum']*2

sc = SoftcodeConnection(address=int(conf.PYBPOD_NET_PORT))

# PORTS
try:
    behavport_idx = (np.where(np.array(conf.BPOD_BEHAVIOR_PORTS_ENABLED) == True)[0] + 1).tolist()
    LEDL, LEDC, LEDR = [getattr(OutputChannel, f"PWM{num}") for num in behavport_idx]
    WATERL, WATERC, WATERR = [(getattr(OutputChannel, "Valve"), num) for num in behavport_idx]
    PGLin, PGCin, PGRin = [getattr(EventName, f"Port{num}In") for num in behavport_idx]
    PGLout, PGCout, PGRout = [getattr(EventName, f"Port{num}Out") for num in behavport_idx]
except:
    UtilsR.stop('THIS PROTOCOL REQUIRES 3 AND ONLY 3 ACTIVE PORTS, ACTIVATE THEM MANUALLY')

# here add 2nd check: both BNC enabled
if conf.BPOD_BNC_PORTS_ENABLED.count(True)!=2: # should work with other bpod versions, right?
    UtilsR.stop('THIS PROTOCOL REQUIRES 2 BNC PORTS, ACTIVATE THEM MANUALLY')

# GET WATER CALIBRATION VALUES
try:
    ValveLtime, ValveCtime, ValveRtime = UtilsR.getWaterCalib(conf.VAR_BOX, behavport_idx)
except:
    UtilsR.stop('COULD NOT FIND WATER CALIBRATION OF BOX: ' + str(conf.VAR_BOX))
if ValveLtime >= 0.3 or (ValveLtime / ValveRtime) > 2 or ValveRtime >= 0.3 or (ValveRtime / ValveLtime) > 2:
    continueTask = UtilsR.dropdownmenu('WATER CALIBRATION ERROR. L: ' + str(ValveLtime) + ', R: ' + str(ValveRtime),
                                       'Continue with these values?', ['Yes', 'No'], 'No')
    if continueTask == 'No':
        UtilsR.stop('')

# GET SOUND CALIBRATION VALUES (BROADBAND)
try:
    LAmp, RAmp = UtilsR.getBroadbandCalib(conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT FIND BROADBAND CALIBRATION OF SOUNDDEVICE: ' + str(conf.VAR_BOX))

# INITIALIZE SOUND STREAM
try:
    sampleRate = 192000
    soundStream = SoundR(sampleRate=sampleRate, deviceOut=conf.VAR_BOX)
except:
    UtilsR.stop('COULD NOT INITIALIZE SOUND STREAM, CHECK SOUNDCARD INDEX')

# ask few optional stuff
opts_dict = UtilsR.checkbuttonmenu({'fair':1, 'enhance_com':1, 'electro':0, 'late_stim':1}, label='choose task options')

if opts_dict['fair']:
    conf.VAR_VARIATION='fair'


BNC1_ON, BNC1_OFF, BNC2_ON, BNC2_OFF = [], [], [], []
if opts_dict['electro']:
    for i, bncvar in enumerate([BNC1_ON, BNC1_OFF, BNC2_ON, BNC2_OFF], 1):
        bncvar = [(getattr(OutputChannel, f"BNC{int((i-1)/2)+1}"), int((i%2)*3))] # ;) [1,1,2,2], [3,0,3,0]



# INITIALIZE VIDEO, PLAY AND RECORD
try:
    currSessionVid = os.path.expanduser('~/VIDEO_pybpod/' + (conf.PYBPOD_SUBJECTS[0]).split("'")[1] + '/')
    if not os.path.exists(os.path.expanduser(currSessionVid)):
        os.makedirs(os.path.expanduser(currSessionVid))
    cam = VideoR(indx_or_path=conf.VAR_BOX, name_video=conf.PYBPOD_SESSION + '.avi', path=currSessionVid,
                 title=conf.VAR_BOX, fps=120, codec_cam='X264', codec_video='X264')
    cam.play()
    if int(conf.VAR_REC) > 0:
        cam.record()
except:
    UtilsR.stop('COULD NOT INITIALIZE VIDEO, CHECK VIDEO INDEX')

########################### relevant variables for the task ##############################
# generate a seed according to date in format YYYYmmdd + box
seed = int(datetime.datetime.now().strftime("%Y%m%d")+conf.VAR_BOX[-2:])
# get a randomgenerator from that seed
rg = np.random.RandomState(seed=seed)

### complete Trial list
if conf.VAR_SSIDE == 'r':
    startingSide = int(rg.choice([0, 1]))  # which side to start
else:
    startingSide = int(conf.VAR_SSIDE)  # needs to be either 0 (left) or 1(right)

if conf.VAR_SBLOCK == 'r':
    startingBlock = bool(rg.choice([True, False]))  # type of block
else:
    startingBlock = bool(int(conf.VAR_SBLOCK))

trial_list, prob_repeat = UtilsR.block(startingSide, repetitive=startingBlock, bsize=int(conf.VAR_BLEN),
                                       bnum=int(conf.VAR_BNUM), randgen=rg)
trial_list = list(trial_list)

sessionTimeLength = 3600 #4500  # secs
sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart + sessionTimeLength

evidences = {
    'f1': (1, 0.5, 0.25, 0, -0.25, -0.5, -1)
}

# stimuli (white-noise)
duration = 0.5  # stim len (secs). Does not imply playing whole stim each time # duration
nenvs = 20

modwave = 0.5 * (np.sin(2 * np.pi * (nenvs/duration) * np.arange(0, duration, step=0.25/sampleRate) - np.pi/2)+1)
# this one has fewer steps because it will only be used with utilsR.is_stim_fair

band_fs = [2000, 20000]  # boundaries for the white noise (Hz)
filter_len = 10000  # filter len


# Next we assign sound vector. Returns np.array. How does this work? -> help(whiteNoiseGen)
soundVec = UtilsR.whiteNoiseGen(1.0, band_fs[0], band_fs[1], duration, FsOut=sampleRate,
                                Fn=filter_len, randgen=rg)  # using amplitude=1.0, adapt to calib values later.



            

def remainingTime():
    return sessionTimeEnding - time.time()

######################### BPOD and States ##################################
START_APP = timeit.default_timer()
my_bpod = Bpod()

if opts_dict['electro']:
    # here do some kind of marks with ttls
    pass

if opts_dict['enhance_com']:  ## prepare vars / functions
    coh=0
    # rewrite whole function now assuming that get_zt is working (we get zt from recent history)
    def decision_function_zt(): # try avoiding hard thresholds
        global trial_list, i, zt_repalt, zt_LR, rg, my_bpod # there's no need to init vars as long as they exist when function is called
        # fixed huge bug:
        # now using abs zt to get p to switch,
        # now using corresponding zt (LR) to decide whether to alter trial or not
        # not modifying next trial coh if there's no side switch    
        p_switch = min(max(
            abs(zt_repalt)-0.65,
            0
        ), 1)
        booltoswitch = rg.choice(
            [False, True],
            p = [1-p_switch, p_switch]
        )
        if not booltoswitch:
            return False, 0.5
        else: #~ need to ensure next/current trial breaks transition pattern
            if zt_LR<0: # expecting left
                if trial_list[i]==0: # current trial is left, we need to change it
                    trial_list[i]=1
                    # register here!
                    my_bpod.register_value('enhance_com_switch', i) 
                    next_trial_coh = rg.choice([0.75,1.]) # coherence in 0-1space
                    return True, next_trial_coh
                else:
                    return False, 0.5
            else: # expecting right
                if trial_list[i]:
                    trial_list[i]=0
                    my_bpod.register_value('enhance_com_switch', i) 
                    next_trial_coh = rg.choice([0., 0.25])
                    return True, next_trial_coh
                else:
                    return False, 0.5
else:
    switchnext = False

fairvec = np.zeros(1000) # initialize var just in case

def my_softcode_handler(data):
    global soundStream
    if data == 68:
        soundStream.playSound()
    elif data == 66:
        soundStream.stopSound()

def my_softcode_handler_fair(data):
    global soundStream, sc, fairvec, soundtimer, my_bpod, i
    if data == 68:
        soundtimer = datetime.datetime.now() #time.time() #
        soundStream.playSound()
    elif data == 66:
        soundStream.stopSound()
        soundtimer = int(round((datetime.datetime.now() - soundtimer).microseconds/1000)) # time.time()
        # here; transform timer in ms (int) compare it to the vector cumtrapz vector
        if soundtimer > 1000:
            pass
        else:
            try:
                if not fairvec[soundtimer]:
                    sc.send(1)
                    my_bpod.register_value('fair_sc_switch_rewside', i)
            except:
                my_bpod.register_value('fair', f'failed to index the boolean sound_vector with {soundtimer}')

def delayed_softcode_handler(data): # cannot be fair and delayed at once
    global soundStream, delay_len
    time.sleep(delay_len) # not precise
    if data == 68:
        soundStream.playSound()
    elif data == 66:
        soundStream.stopSound()

my_bpod.register_value('TASK', [task_name, task_version, tasks_commit, md5sum])
my_bpod.register_value('STAGE_NUMBER', stage_number)
my_bpod.register_value('SEED', seed)

#opts
log_opts = []
for k in opts_dict.keys():
    if opts_dict[k]:
        log_opts += [k]

my_bpod.register_value('opts_dict', sorted(log_opts))

### WE NEED TO KEEP THIS, since it can be changed on the go, it first log wont be accurate (probably previous session one)
try:
    my_bpod.register_value('Variation', conf.VAR_VARIATION)
except:
    my_bpod.register_value('Variation', 'no variation of the task stated')
try:
    if opts_dict['electro']:
        my_bpod.register_value('Postop', 'electro')
    else:
        my_bpod.register_value('Postop', 'none') 
except:
    my_bpod.register_value('Postop', 'no postop stated')

my_bpod.register_value('REWARD_SIDE', trial_list)  # required for trend plugin [0-left, 1-right]
my_bpod.register_value('left_amp', LAmp)
my_bpod.register_value('right_amp', RAmp)
my_bpod.register_value('left_valve', ValveLtime)
my_bpod.register_value('right_valve', ValveRtime)



centralLED = (LEDC, 8) # this is constant

zt_repalt = 0
hit = 0

for i in range(len(trial_list)):
    timeleft = remainingTime()

    if opts_dict['enhance_com']: 
        #if hithistory[-1]:
        if hit:
            # zt = UtilsR.get_zt(hithistory, rewside=trial_list, current_index=i, gating_ae=0.15)
            # switchnext, nextcoh = decision_function_hitstreak(curr_rng=rg, hitstreak=hitstreak, rewside=trial_list, current_index=i)
            switchnext, nextcoh = decision_function_zt()
        else:
            switchnext=False

    late_stim = False
    my_bpod.softcode_handler_function = my_softcode_handler
    if opts_dict['late_stim']:
        # if switchnext: # if also late_stim, we could increase its chance to 33%
        #     late_stim = bool(rg.choice([0,1], p=[.9, .1]))   
        late_stim = rg.choice([False,True], p=[.9, .1])
        if late_stim:
            delay_len = round(rg.normal(loc=0.065, scale=0.005), 4)
            my_bpod.softcode_handler_function = delayed_softcode_handler

    elif conf.VAR_VARIATION=='fair': # fairstim overrrided by late trials
        my_bpod.softcode_handler_function = my_softcode_handler_fair


    if switchnext:
        curr_coh = nextcoh
    else:
        coh_to_chose_from = np.array(evidences['f1'])
        curr_coh = UtilsR.select_evidence(trial_list[i], coh_to_chose_from, randgen=rg)
    

    
    if trial_list[i] == 0:
        valvetime, inv_valvetime = ValveLtime, ValveRtime
        rewardValve, inv_rewardValve = WATERL, WATERR
        if conf.VAR_VARIATION=='fair':
            resp = {'SoftCode1':'invertedWaitResponse',PGLin: 'Reward', PGRin: 'Punish', EventName.Tup: 'Invalid'}
            invresp = {PGRin: 'invReward', PGLin: 'Punish', EventName.Tup: 'Invalid'}
        else:
            resp = {PGLin: 'Reward', PGRin: 'Punish', EventName.Tup: 'Invalid'}
        rewardLED, inv_rewardLED = (LEDL, 8), (LEDR, 8)
    elif trial_list[i] == 1:
        valvetime, inv_valvetime = ValveRtime, ValveLtime
        rewardValve, inv_rewardValve = WATERR, WATERL
        if conf.VAR_VARIATION=='fair':
            resp = {'SoftCode1':'invertedWaitResponse', PGRin: 'Reward', PGLin: 'Punish', EventName.Tup: 'Invalid'}
            invresp = {PGLin: 'invReward', PGRin: 'Punish', EventName.Tup: 'Invalid'}
        else:
            resp = {PGRin: 'Reward', PGLin: 'Punish', EventName.Tup: 'Invalid'}
        rewardLED, inv_rewardLED = (LEDR, 8), (LEDL, 8)
        
    SL, SR, EL, ER = UtilsR.envelope(curr_coh, soundVec, duration, nenvs, samplingR=sampleRate, variance=0.015, randomized=False,
                                     paired=False, LAmp=LAmp, RAmp=RAmp, oldbug=False, randgen=rg)
    
    if conf.VAR_VARIATION=='fair':
        fair, fairvec = UtilsR.is_stim_fair(trial_list[i], EL, ER, freq=(nenvs/duration),modwave=modwave, stimdur=duration)
        
        if not fair:
            my_bpod.register_value('fair_note:', 'switched_envelope')
            SL, SR = SR, SL # switch stims
            EL, ER = ER*-1, EL*-1 # to preserve Left env as negative and Renv as positive
            # so other scripts do not crash
            fairvec = np.invert(fairvec) # after inverting all, fair bool array needs to be switched again


    soundStream.load(SL, SR)
    my_bpod.register_value('coherence01', curr_coh)
    my_bpod.register_value('zt_repalt', zt_repalt)
    my_bpod.register_value('left_envelope', EL.tolist())
    my_bpod.register_value('right_envelope', ER.tolist())
    my_bpod.register_value('prob_repeat', prob_repeat[i])
    if late_stim:
        my_bpod.register_value('delayed_trial', i) # iterating var
        my_bpod.register_value('expected_delay', delay_len)

    sma = StateMachine(my_bpod)
    sma.set_global_timer_legacy(timer_id=1, timer_duration=timeleft)

    sma.add_state(  # WaitCPoke
        state_name='WaitCPoke',
        state_timer=timeleft,
        state_change_conditions={PGCin: 'Fixation_fb', Bpod.Events.GlobalTimer1_End: 'exit', EventName.Tup: 'exit'},
        output_actions=[centralLED, (Bpod.OutputChannels.GlobalTimerTrig, 1)] + BNC1_OFF + BNC2_OFF)
    sma.add_state( #fixation feedback
        state_name='Fixation_fb', 
        state_timer=0.08,
        state_change_conditions={PGCout: 'WaitCPoke', EventName.Tup: 'Fixation'},
        output_actions=BNC1_ON) # LED OFF + BNC1_ON is a list which will be empty if not using electro opt.
    sma.add_state(  # Fixation
        state_name='Fixation',
        state_timer=0.22,
        state_change_conditions={PGCout: 'WaitCPoke', EventName.Tup: 'StartSound'},
        output_actions=[centralLED]+BNC1_OFF)
    sma.add_state(  # StartSound
        state_name='StartSound',
        state_timer=duration, # same var used to generate soundvector so this bug wont happen again
        state_change_conditions={PGCout: 'WaitResponse', EventName.Tup: 'WaitResponse'},
        # because it seems to get stuck here sometimes
        output_actions=[(OutputChannel.SoftCode, 68)] + BNC2_ON)  # ,(Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(
        state_name='WaitResponse',
        state_timer=8,
        state_change_conditions=resp,
        output_actions=[(OutputChannel.SoftCode, 66), (LEDL, 8), (LEDR, 8)] + BNC2_OFF)
    if conf.VAR_VARIATION=='fair':
        sma.add_state(
            state_name='invertedWaitResponse',
            state_timer=8,
            state_change_conditions=invresp,
            output_actions=[(LEDL, 8), (LEDR, 8)] + BNC2_OFF) # we could turn on lateral port leds
    sma.add_state(  # Reward
        state_name='Reward',
        state_timer=valvetime,
        state_change_conditions={EventName.Tup: 'keep-led-on'},
        output_actions=[rewardValve] + BNC1_ON + BNC2_ON)
    if conf.VAR_VARIATION=='fair':
        sma.add_state( # reward when inverted
            state_name='invReward',
            state_timer= inv_valvetime,
            state_change_conditions={EventName.Tup: 'inv_keep-led-on'},
            output_actions=[inv_rewardValve] + BNC1_ON + BNC2_ON)
    sma.add_state(  # keep-led-on
        state_name='keep-led-on',
        state_timer=0.2,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[rewardLED])
    if conf.VAR_VARIATION=='fair':
        sma.add_state(  # keep-led-on
            state_name='inv_keep-led-on',
            state_timer=0.2,
            state_change_conditions={EventName.Tup: 'exit'},
            output_actions=[inv_rewardLED] + BNC1_ON + BNC2_ON)
    sma.add_state(  # Punish
        state_name='Punish',
        state_timer=2,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[] + BNC1_ON + BNC2_ON)
    sma.add_state(
        state_name='Invalid',
        state_timer=0.100,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[] + BNC1_ON + BNC2_ON)

    my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device
    my_bpod.run_state_machine(sma)  # Run state machine

    if time.time() > sessionTimeEnding:
        my_bpod.register_value('clean_exit', 1)
        break

    trialstring = ("Current trial info: {0}".format(my_bpod.session.current_trial))  # remove all
    hit_1 = hit
    if ("'Reward': [(nan, nan)]" not in trialstring) or ("'invReward': [(nan, nan)]" not in trialstring):
        #hithistory = hithistory[1:]+[1]
        hit = 1
    else:
        # hithistory = hithistory[1:]+[0]
        hit = 0
    if i:
        zt_repalt, zt_LR = UtilsR.get_zt_light(zt_repalt, hit, hit_1, trial_list[i], trial_list[i-1])
    #

my_bpod.close()  # Disconnect Bpod and perform post-run actions
cam.stop()
sc.close()


# new reports:
#os.system('datahandler -l ' + conf.PYBPOD_SESSION[:-7])

pdfpath = report_noenv(
    os.path.join(conf.PYBPOD_SESSION_PATH, conf.PYBPOD_SESSION)+'.csv', 
    currSessionVid+conf.PYBPOD_SESSION+'.npy', 
    os.path.expanduser('~/daily_reports')+'/'+conf.PYBPOD_SUBJECTS[0].split("'")[1]+f'/c{conf.PYBPOD_SESSION}.pdf')
if 'VAR_SLACK' in conf.PYBPOD_VARSNAMES:
    UtilsR.slack_spam(f'{conf.PYBPOD_SESSION} report', pdfpath, conf.VAR_SLACK)